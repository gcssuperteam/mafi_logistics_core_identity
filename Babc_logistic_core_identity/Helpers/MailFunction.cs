﻿using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System;
using Babc_logistic_core.Database;
using Microsoft.Extensions.Configuration;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Database.GIS.DTO;

namespace Babc_logistic_core.Helpers
{
    public class MailFunction:ModelBase
    {

        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        IConfiguration _config;
        public MailFunction(IConfiguration config, ApplicationUser user) : base(config, user)
        {
            _config = config;
        }


        public bool CreateEmail(SendMailParam param)
        {
            bool result = false;

            try
            {            

                byte[] data = Convert.FromBase64String(_config.GetValue<string>("MailConfig") ?? "");  //Lösenordet lagras i config-filen som en Base64-sträng för att undvika att det står i klartext
                List<string> mailValues = Encoding.UTF8.GetString(data).Split(';').ToList();
                MailSettings ms = new MailSettings();
                ms.SmtpServer = mailValues[0] ?? "";
                ms.Port = GeneralFunctions.getIntFromStr(mailValues[1] ?? "587");
                ms.SmtpUser = mailValues[2] ?? "";
                ms.SmtpPassword = mailValues[3] ?? "";

                ms.FromName = _config.GetValue<string>("MailFromName") ?? "";
                ms.FromAddress = _config.GetValue<string>("MailReplyAddress") ?? "";
                ms.ToName = ms.FromName;
                ms.ToAddress = param.To[0].Email;
                ms.ReplyAddress = _config.GetValue<string>("MailReplyAddress") ?? "";
                ms.BccAddress = new List<string>() { ms.ReplyAddress };
                ms.Subject = param.Subject;
                ms.Body = param.MessageHtml;

                if (SendMail(ms, true))
                {
                    result = true;
                    //mLog.Debug("SendErrorLogByMail was sent to " + Addresses);
                }
                else
                {
                    result = false;
                    //mLog.Debug("SendErrorLogByMail wasn't sent to " + Addresses);
                }
            }
            catch (Exception e)
            {
            

            }

            return result;
        }


        public bool SendMail(MailSettings ms, bool useAttachment)
        {
            bool result = false;
            bool useSSL = true;

            try
            {
                try { useSSL = Convert.ToBoolean(_config.GetValue<string>("UseSSL") ?? "true"); } catch (Exception e) { }

                SmtpClient smtpClient = new SmtpClient(ms.SmtpServer);

                // set smtp-client with basicAuthentication
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Port = ms.Port;
                smtpClient.EnableSsl = useSSL; // true;
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ms.SmtpUser, ms.SmtpPassword);
                smtpClient.Credentials = basicAuthenticationInfo;

                try
                {
                    bool useO365 = Convert.ToBoolean(_config.GetValue<string>("Office365") ?? "false");

                    if (useO365)
                    {
                        // static settings for Office 365:
                        smtpClient.TargetName = "STARTTLS/smtp.office365.com";
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                        smtpClient.EnableSsl = true;
                    }
                }
                catch (Exception e)
                {
                   
                }

                // add from,to mailaddresses
                MailAddress from = new MailAddress(ms.FromAddress, ms.FromName);
                MailAddress to = new MailAddress(ms.ToAddress, ms.ToName);

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = from;
 
                    mail.To.Add(ms.ToAddress);
            
                   
                    // add ReplyTo
                    mail.ReplyToList.Add(ms.ReplyAddress);

                    // set subject and encoding
                    mail.Subject = ms.Subject;
                    mail.SubjectEncoding = System.Text.Encoding.UTF8;

                    // set body-message and encoding
                    mail.Body = ms.Body;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    // text or html
                    mail.IsBodyHtml = true;

                    if (useAttachment && !string.IsNullOrEmpty(ms.AttachmentPath))
                    {
                        Attachment attachment = new Attachment(ms.AttachmentPath);
                        mail.Attachments.Add(attachment);
                    }

                    smtpClient.Send(mail);
                    result = true;
                }

            }
            catch (Exception e)
            {
               
            }

            return result;
        }



        public class MailSettings
        {
            public string SmtpServer { get; set; }
            public int Port { get; set; }
            public string SmtpUser { get; set; }
            public string SmtpPassword { get; set; }
            public string FromName { get; set; }
            public string FromAddress { get; set; }
            public string ToName { get; set; }
            public string ToAddress { get; set; }
            public List<string> ToAddresses { get; set; }
            public string ReplyAddress { get; set; }
            public List<string> BccAddress { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public string AttachmentPath { get; set; }

        }
    }
}
