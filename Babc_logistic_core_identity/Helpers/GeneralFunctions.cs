﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Babc_logistic_core.Helpers
{
    public static class GeneralFunctions
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string mDecimalSeparator = "";

        public static decimal getDecimalFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (getCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDecimal(value.Replace(".", ","));
                else
                    return Convert.ToDecimal(value.Replace(",", "."));
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        public static string getStrFromDecimal(Nullable<decimal> value)
        {
            try
            {
                if (value.HasValue)
                    return Convert.ToString(value.Value).Replace(",", ".");
                else
                    return "0";
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting Decimal to String", e);
                return "0";
            }
        }

        public static string getStrFromDouble(Nullable<double> value)
        {
            try
            {
                if (value.HasValue)
                    return Convert.ToString(value.Value).Replace(",", ".");
                else
                    return "0";
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting Double to String ", e);
                return "0";
            }
        }

        public static int getIntFromStr(string value)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(value))
                    return Convert.ToInt32(value);
                else
                    return 0;
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting String to int (" + value + ")", e);
                return 0;
            }

        }

        public static string getStrFromInt(int value)
        {
            try
            {
                return Convert.ToString(value);
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting Int to String ", e);
                return "0";
            }
        }


        public static DateTime getDateFromStr(string date)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(date))
                {
                    return DateTime.Now;
                }

                return DateTime.ParseExact(date, "yyMMdd", CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting String (" + date + ")to Date", e);
                return DateTime.Now;
            }
        }

        public static string getStrFromDate(DateTime date)
        {
            try
            {
                return date.ToString("yyMMdd");
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting Date to string", e);
                return "";
            }
        }

        public static string getGarpDateFromStr(string date)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(date))
                {
                    return DateTime.Now.ToString("yyMMdd");
                }

                return DateTime.Parse(date).ToString("yyMMdd");
            }
            catch (Exception e)
            {
                mLog.Error("Error while converting String (" + date + ")to Date", e);
                return DateTime.Now.ToString("yyMMdd");
            }
        }

        public static string getStrFromDate(DateTime? date)
        {
            try
            {
                if (date.HasValue)
                    return date.Value.ToString("yyMMdd");
                else
                    return DateTime.Now.ToString("yyMMdd");

            }
            catch (Exception e)
            {
                mLog.Error("Error while converting Date to string", e);
                return "";
            }
        }


        public static string getCurrentDecimalSeparator()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mDecimalSeparator))
                {
                    System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    mDecimalSeparator = ci.NumberFormat.CurrencyDecimalSeparator;
                }

                return mDecimalSeparator;
            }
            catch (Exception e)
            {
                mLog.Error("Error getting CurrentDecimalSeparator", e);
                return mDecimalSeparator;
            }
        }

        public static byte[] getBytesFromStr(string str)
        {
            List<byte> bytes = new List<byte>();

            try
            {
                foreach (char c in str.ToCharArray())
                {
                    bytes.Add(Convert.ToByte(c));
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error converting string to byte[]", e);
            }

            return bytes.ToArray();
        }

        public static string getStrFromBytes(byte[] bytes)
        {
            try
            {
                //                char[] chars = new char[bytes.Length / sizeof(char)];
                //                System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
                //                return new string(chars);

                return Encoding.ASCII.GetString(bytes);
            }
            catch (Exception e)
            {
                mLog.Error("Error converting Bytes[] to String", e);
                return "";
            }
        }

        public static void fillBlankRight(string data, byte[] dest)
        {
            fillCharRight(data, dest, (char)' ');
        }

        public static void fillBlankLeft(string data, byte[] dest)
        {
            fillCharLeft(data, dest, (char)' ');
        }

        public static void fillCharRight(string data, byte[] dest, char token)
        {
            try
            {
                dest = GeneralFunctions.getBytesFromStr(data.PadRight(dest.Length, token));
            }
            catch (Exception e)
            {
                mLog.Error("Error in fillCharRight", e);
            }
        }

        public static void fillCharLeft(string data, byte[] dest, char token)
        {
            try
            {
                dest = GeneralFunctions.getBytesFromStr(data.PadLeft(dest.Length, token));
            }
            catch (Exception e)
            {
                mLog.Error("Error in fillCharRight", e);
            }
        }

        public static bool compareUid(string current, string uid)
        {
            if (string.IsNullOrEmpty(current) && string.IsNullOrEmpty(uid))
                return false;

            if (string.IsNullOrEmpty(current) || string.IsNullOrEmpty(uid))
                return false;

            if (current.Equals(uid))
                return true;
            else
                return false;
        }

    }
}
