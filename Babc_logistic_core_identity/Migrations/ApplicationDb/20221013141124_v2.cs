﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Babc_logistic_core.Migrations.ApplicationDb
{
    public partial class v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GarpPassword",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GarpUser",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GisToken",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GarpPassword",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "GarpUser",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "GisToken",
                table: "AspNetUsers");
        }
    }
}
