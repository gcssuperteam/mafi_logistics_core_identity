﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Babc_logistic_core.Migrations
{
    public partial class addedproductdescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductDescription",
                table: "Inventorys",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductDescription",
                table: "Inventorys");
        }
    }
}
