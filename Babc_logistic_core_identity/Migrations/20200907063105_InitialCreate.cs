﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Babc_logistic_core.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InventorySets",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Description = table.Column<string>(nullable: true),
                    WarehouseNo = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventorySets", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Inventorys",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProductNo = table.Column<string>(nullable: true),
                    WarehouseNo = table.Column<string>(nullable: true),
                    Date = table.Column<string>(nullable: true),
                    Time = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    OriginalAmount = table.Column<decimal>(nullable: false),
                    AmountAtTime = table.Column<DateTime>(nullable: false),
                    VIPAtTime = table.Column<decimal>(nullable: false),
                    InventoryDone = table.Column<bool>(nullable: false),
                    InventorySetId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventorys", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Inventorys_InventorySets_InventorySetId",
                        column: x => x.InventorySetId,
                        principalTable: "InventorySets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Inventorys_InventorySetId",
                table: "Inventorys",
                column: "InventorySetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inventorys");

            migrationBuilder.DropTable(
                name: "InventorySets");
        }
    }
}
