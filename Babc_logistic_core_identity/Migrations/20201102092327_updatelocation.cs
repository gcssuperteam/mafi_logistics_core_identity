﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Babc_logistic_core.Migrations
{
    public partial class updatelocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LocationId",
                table: "InventorySets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LocationName",
                table: "InventorySets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "InventorySets");

            migrationBuilder.DropColumn(
                name: "LocationName",
                table: "InventorySets");
        }
    }
}
