﻿// Functions to ViewComponent "TAPackageSelector"
//
// This has to be incude by _Layout.cshtml for the ViewComponent to function correctly
//

$(document).on('click', '#btn-add-ta-pkg', {}, function (e)
{
    show_spinner($('#order-list'), 2, "bounce");
    $("#volume").prop('disabled', false);

    var url = $("#frm-add-ta-pkg").attr("action");
    var form = $("#frm-add-ta-pkg").serialize();

    var options = {};
    options.url = url;
    options.type = "POST";
    options.data = form;
    options.dataType = "text";
    options.success = function (data)
    {
        if (data != "FAILED")
        {
            $("#package-selector").replaceWith(data);
            hide_spinner($('#order-list'));
        }

        return false;
    };
    options.error = function ()
    {
        hide_spinner($('#order-list'));
        alert("Error while addin package!");
    };
    $.ajax(options);
});

$(document).on('click', '.btn-delete-ta-pkg', {}, function (e)
{
    show_spinner($('#order-list'), 2, "bounce");

    var controller = $(this).data("controller");
    var action = $(this).data("action");
    var id = $(this).data("id");
    var delivernote = $(this).data("delivernote");

    var options = {};
    options.url = "/" + controller + "/" + action;
    options.type = "POST";
    options.data = { delivernote: delivernote, id: id };
    options.dataType = "text";
    options.success = function (data)
    {
        if (data != "FAILED")
        {
            $("#package-selector").replaceWith(data);
            hide_spinner($('#order-list'));
        }

        return false;
    };
    options.error = function ()
    {
        hide_spinner($('#order-list'));
        alert("Error while removing package!");
    };
    $.ajax(options);
});

function updatePackage() {

    var url = "/Transport/UpdatePackage";
    var form = $("#frm-update-ta-pkg").serialize();

    var options = {};
    options.url = url;
    options.type = "POST";
    options.data = form;
    options.dataType = "text";
    options.success = function (data) {
        if (data != "FAILED") {
            $("#package-selector").replaceWith(data);
            hide_spinner($('#order-list'));
        }
        $('#dialog-editPackage').dialog('close');
       
        return false;
       
    };
    options.error = function () {
        hide_spinner($('#order-list'));

        alert("Error while addin package!");

    };
    $.ajax(options);
};

function Refresh() {

    var url = "/Transport/Refresh";
    var delivernote = $("#DeliverNoteNo").val()
    var options = {};
    options.url = url;
    options.type = "POST";
    options.data = { delivernote: delivernote };
    options.dataType = "text";
    options.success = function (data) {
        if (data.startsWith("OK")) {



        }
        else {

        }


        return false;

    };
    options.error = function () {
        hide_spinner($('#order-list'));
        alert("Error while refreshing!");
    };
    $.ajax(options);
};

function savePrice() {
    var options = {};
    options.url = "/Transport/UpdatePrice";
    options.type = "POST";
    options.data = { onr: $("#onr").val(), price: $("#price").val() };
    options.dataType = "text";
    //options.contentType = "application/json";
    options.success = function (data) {
        if (data.startsWith("OK")) {

            $("#dialog-OK").dialog("open");

        }
        else {
            $("#dialog-Error").dialog("open");
        }
        return false;
    };

    options.error = function () {
        hide_spinner($("#order-list"));
        alert("Error while executing delivery!");
    };
    $.ajax(options);
}






$(document).on('click', '.btn-copy-ta-pkg', {}, function (e) {
    show_spinner($('#order-list'), 2, "bounce");

    var controller = $(this).data("controller");

    var id = $(this).data("id");
    var delivernote = $(this).data("delivernote");

    var options = {};
    options.url = "/" + controller + "/CopyPackage";
    options.type = "POST";
    options.data = { delivernote: delivernote, id: id };
    options.dataType = "text";
    options.success = function (data) {
        if (data != "FAILED") {
            
            $("#package-selector").replaceWith(data);
            hide_spinner($('#order-list'));
        }

        return false;
    };
    options.error = function () {
        hide_spinner($('#order-list'));
        alert("Error while copying package!" + id + delivernote + action + controller);
    };
    $.ajax(options);
});

