using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Web.Mvc;
using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Babc_logistic_core.Controllers
{
    public class ProductionController : Controller
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private ProductionModel mPM;
        //private BaseDataModel mBDM;
        //private Babc_logistic_core.GIS.Model.ProductModel mProduct;
        //private Babc_logistic_core.GIS.Model.OrderModel mOM;
        //private PrintingModel mPRM;

        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;


        public ProductionController(IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            //mPM = new ProductionModel(config, user);
            //mProduct = new Babc_logistic_core.GIS.Model.ProductModel(config, user);
            //mOM = new Babc_logistic_core.GIS.Model.OrderModel(config, user);
            //mBDM = new Babc_logistic_core.GIS.Model.BaseDataModel(config, user);
            //mPRM = new PrintingModel(config, user);
            //_Config = config;

            _Config = config;
            _userManager = userManager;

        }


        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult MaterialOrderPickList()
         {
            var user = _userManager.GetUserAsync(User)?.Result;

            ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<MaterialPickVM> result = new List<MaterialPickVM>();
            List<OrderHead> tempLst = new List<OrderHead>();
            List<string> additionalLst = new List<string>();
            List<string> checkLst = new List<string>();

            MaterialOrderListParam param = new MaterialOrderListParam();
            OrderHead ohda = new OrderHead();

            try
            {
                List<FilterDTO> filterList = getFilterList("FILTERM");
                param.Filter = parseFilterConstants(filterList[0].Filter);
                param.ReverseReading = true;
                param.WithRows = true;
                param.Index = 1;
                param.MaxCount = 50;

                if (filterList[0].UseCache == true)
                {
                    param.Cache = new CacheParam { CacheId = user.GisToken + "_materialpicklist", ReadFrom = true, UpdateTo = false, CheckConsistency = true, UseInMemoryCache = true };
                    //param.Cache = new CacheParam { CacheId = "materialpicklist", ReadFrom = true, UpdateTo = false };
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error setting filter", e);
            }


            try
            {
                List<MaterialOrderHead> orderheads = mPM.GetMaterialOrderList(param);

                foreach(var oh in orderheads)
                {
                    MaterialPickVM mp = new MaterialPickVM();

                    mp.OH = oh;
                    mp.MaterialRowList = oh.MaterialRows;
                    mp.ManufacturedProductDescription = oh.ProductNo;
                    mp.ManufacturedProductDescription = oh.ProductDescription;
                    mp.ManufacturedAmount = oh.Amount - oh.DeliveredAmount;
                    mp.OrderRowDeliverDate = oh.PreferedDeliverDate;

                    result.Add(mp);
                }

                result = result.OrderBy(o => o.OrderRowDeliverDate).ToList();
            }
            catch (Exception e)
            {
                mLog.Error("Error in MaterialOrderPickList()", e);
            }

            return View(result);
        }


        [Authorize]
        public IActionResult MaterialList(string onr, string row, string? sign, bool? isFutureOrder)
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                ViewBag.sign = sign;
                if(isFutureOrder==null)
                {
                    isFutureOrder = false;
                }

                ViewBag.isFutureOrder = isFutureOrder.ToString();
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                result.OH = mPM.GetMaterialOrder(onr, row);
                OrderRow or = mOM.GetOrderRow(onr, row);

                // Get warehouse from orderrow of produced product (used in selection of exteneded warehouselist)
                result.ManufacturedWarehouse = or.WarehouseNo;
                result.ManufacturedAmount = result.OH.Amount - result.OH.DeliveredAmount; 

                result.MaterialRowList = result.OH.MaterialRows.OrderBy(o=>o.PreferedDeliverDate).ToList();
                
                result.Succeeded = true;
            }
            catch (Exception e)
            {
                mLog.Error("Error in MaterialList", e);
            }

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public IActionResult PickMaterial(string onr, string row, string materialrow, string warehouse, string amount, string deliverstate)
        {
            MaterialPickVM result = new MaterialPickVM();
 
            deliverstate = (string.IsNullOrEmpty(deliverstate)) ? "" : deliverstate;
            try
            {
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                MaterialDeliverResult delResult = pickMaterial(onr, GeneralFunctions.getIntFromStr(row), GeneralFunctions.getIntFromStr(materialrow), warehouse, GeneralFunctions.getDecimalFromStr(amount), deliverstate);

                if(delResult.Succeeded == true)
                {
                    result.OH = mPM.GetMaterialOrder(onr, row);
                    result.MaterialRowList = result.OH.MaterialRows;
                    result.Succeeded = true;

                    OrderRow or = mOM.GetOrderRow(onr, row);
                    MaterialRow mor = result.MaterialRowList.Where(m => m.MaterialRowNo == int.Parse(materialrow)).FirstOrDefault();
                    return ViewComponent("MaterialOrderRowSlice", new { or = mor, amount = result.OH.Amount, warehouse = or.WarehouseNo }); 
                }
                else
                {
                    return Content("# " + delResult.InternalDeliverMessage);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in PickMaterial", e);
                return Content("# Error while withrawing material: " + e.Message);
            }
        }

        private MaterialDeliverResult pickMaterial(string onr, int row, int materialrow, string warehouse, decimal amount, string deliverstate)
        {
            MaterialDeliverResult result = new MaterialDeliverResult();
            if (string.IsNullOrEmpty(deliverstate)) deliverstate = "";
            try
            {
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);

                result = mPM.DeliverMaterialOnOrderRow(new MaterialDeliverParam
                {
                    OrderNo = onr,
                    OrderRowNo = row,
                    MaterialRowNo = materialrow,
                    WarehouseNo = warehouse,
                    Amount = amount,
                    DeliverAllMaterialRows = false,
                    TransactionType = "X",
                    Deliverstate = deliverstate
                }); 

            }
            catch (Exception e)
            {
                mLog.Error("Error in pickMaterial", e);
            }


            return result;
        }

        [HttpPost]
        [Authorize]
        public IActionResult BackMaterial(string onr, string row, string matrow, string warehouse)
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                var delResult = mPM.BackMaterialOnOrderRow(new MaterialBackParam
                {
                    OrderNo = onr,
                    OrderRowNo = GeneralFunctions.getIntFromStr(row),
                    MaterialRowNo = GeneralFunctions.getIntFromStr(matrow),
                    WarehouseNo = warehouse
                });


                if (delResult.Succeeded == true)
                {
                    result.OH = mPM.GetMaterialOrder(onr, row);
                    result.MaterialRowList = result.OH.MaterialRows;
                    result.Succeeded = true;

                    OrderRow or = mOM.GetOrderRow(onr, row);

                    MaterialRow mor = result.MaterialRowList.Where(m => m.MaterialRowNo == int.Parse(matrow)).FirstOrDefault();
                    return ViewComponent("MaterialOrderRowSlice", new { or = mor, amount = result.OH.Amount, warehouse = or.WarehouseNo }); // PartialView("_MaterialRows", result);
                }
                else
                {
                    return Content("# " + delResult.InternalDeliverMessage);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in PickMaterial", e);
                return Content("# Error while withrawing material: " + e.Message);
            }
        }

        public IActionResult OperationList()
        {
            return View();
        }

        [Authorize]
        public IActionResult Location(string onr, string row, string matrow, string opn)
        {
            WarehouseMoveVM result = new WarehouseMoveVM();

            try
            {
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);
                Babc_logistic_core.GIS.Model.ProductModel mProduct = new Babc_logistic_core.GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);
                

                MaterialOrderHead order = mPM.GetMaterialOrder(onr, row);
                MaterialRow materialRow = order.MaterialRows.Where(r => r.MaterialRowNo == int.Parse(matrow)).First();

                Product product = mProduct.GetProductById(materialRow.ProductNo);

                result.OrderNo = onr;
                result.RowNo = row;
                result.MaterialRow = matrow;
                result.OperationNo = opn;
                result.ProductNo = product.ProductNo;
                result.WarehouseNoTo = materialRow.WarehouseNo;
                
                result.WarehouseList = product.WarehouseList;
                result.WarehouseList.RemoveAll(w => w.Stock <= 0);
                result.Amount = GeneralFunctions.getStrFromDecimal(materialRow.Amount);

                

                WarehouseNumber wn = product.WarehouseList.Where(w => w.Id == materialRow.WarehouseNo).First();

                if (wn != null)
                {
                    if (materialRow.Amount > wn.Stock)
                    {
                        decimal currentStock = 0;

                        // If stock is less then 0, we set i to zero
                        if (wn.Stock >= 0)
                        {
                            currentStock = wn.Stock;
                        }

                        result.Amount = (materialRow.Amount - currentStock).ToString();
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in Location()", e);
            }

            return View(result);
        }


        [HttpPost]
        [Authorize]
        public IActionResult ExecuteWarehouseMove(WarehouseMoveVM model)
        {
            try
            {
                Babc_logistic_core.GIS.Model.ProductModel mProduct = new Babc_logistic_core.GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                MoveParam mp = new MoveParam();
                mp.ProductNo = model.ProductNo;
                mp.MoveFrom = model.WarehouseNoFrom;
                mp.MoveTo = model.WarehouseNoTo;
                mp.Amount = GeneralFunctions.getDecimalFromStr(model.Amount);

                MoveResult result = mProduct.MoveProductBetweenWarehouses(mp);

                //MaterialPickVM pickResult = pickMaterial(model.ProductNo, model.OrderNo, model.RowNo, model.OperationNo);

                if (!result.Succeeded)
                {
                    mLog.Debug(result.InternalMoveMessage);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in ExecuteWarehouseMove()", e);
            }

            return RedirectToAction("MaterialList", "Production", new { onr = model.OrderNo, row = model.RowNo, opn = model.OperationNo });
        }

        [HttpPost]
        [Authorize]
        public IActionResult OperationList(string order_and_row)
        {
            ProductionOrderHead result = new ProductionOrderHead();

            try
            {
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);

                string[] sOrderAndRow = order_and_row.Split(new char[] { ' ', '-' });

                if (sOrderAndRow.Length > 1)
                {
                    result = mPM.GetProductionOrder(sOrderAndRow[0].Trim(), sOrderAndRow[sOrderAndRow.Length-1].Trim());

                    // Remove operations that are already done
                    result.OperationRows = result.OperationRows.Where(o => o.Status != "5")?.ToList();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error getting Operations", e);
            }

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public string PrintDeliverNote(string onr, string row, bool pallets_card)
        {
            PrintingModel mPRM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            ERPReport report = new ERPReport();

            try
            {
                report = _Config.GetSection("PoductionDeliverNote").Get<ERPReport>();

                try
                {
                    report.Dialogs.Find(d => d.Id == "Order:").Value = onr.Trim();
                }
                catch { }

                try
                {
                    report.Dialogs.Find(d => d.Id == "incl pallet card J/N").Value = pallets_card == true ? "J" : "N";
                }
                catch { }

                try
                {
                    report.Dialogs.Find(d => d.Id == "Row from:").Value = row.Trim();
                    report.Dialogs.Find(d => d.Id == "Row to:").Value = row.Trim();
                }
                catch { }

            }
            catch
            {
                report.DocGen = "283";
                report.Report = "63";
                report.Medium = "P";
                report.Form = "FS";
            }

            try
            {
                report.IndexFrom = onr;
                report.IndexTo = onr;

                mPRM.Print(report);
            }
            catch (Exception)
            {

            }

            return "";
        }

        [HttpPost]
        [Authorize]
        public string PrintMaterialSpecification(string onr, string row)
        {
            PrintingModel mPRM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            ERPReport report = new ERPReport();

            try
            {
                report = _Config.GetSection("MaterialSpecification").Get<ERPReport>();
            }
            catch
            {
                report.DocGen = "283";
                report.Report = "61";
                report.Medium = "P";
                report.Form = "A4S";
            }

            try
            {
                report.IndexFrom = onr;
                report.IndexTo = onr;

                mPRM.Print(report);
            }
            catch (Exception)
            {

            }

            return "";
        }

        private string parseFilterConstants(string filter)
        {
            int count = 0;

            if (filter.Contains("#DAT"))
            {
                while (filter.Contains("#DAT"))
                {

                    int pos = filter.IndexOf("#DAT");
                    string whole = filter.Substring(pos, 6);
                    string date = DateTime.Now.AddDays(int.Parse(whole.Substring(4))).ToString("yyMMdd");
                    filter = filter.Replace(whole, date);
                    count++;

                    // If this high count something went wrong
                    if (count > 20)
                    {
                        break;
                    }
                }
            }

            return filter;
        }

        private List<FilterDTO> getFilterList(string prefix)
        {
            List<FilterDTO> result = new List<FilterDTO>();

            try
            {
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);

                List<BaseTable> filterlist = mBDM.GetBaseTableList(prefix);

                if (filterlist != null)
                {
                    foreach (BaseTable bt in filterlist)
                    {
                        result.Add(new FilterDTO { Id = bt.Id, Description = bt.Description, Type = bt.Type, Filter = bt.Description2, UseCache = bt.Code1 == "T" ? true : false }); 
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }
    }


}