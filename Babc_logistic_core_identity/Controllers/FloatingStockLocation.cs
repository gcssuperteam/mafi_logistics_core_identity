﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.CodeAnalysis;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Babc_logistic_core.Controllers
{
    public class FloatingStockLocation : Controller
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly InventoryContext _context;
        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;

        private IWebHostEnvironment _Env;
        //private GIS.Model.ProductModel mPM;
        //private GIS.Model.WarehouseModel mWM;

        public FloatingStockLocation(IConfiguration config, IWebHostEnvironment env, InventoryContext context, UserManager<ApplicationUser> userManager)
        {
            _Config = config;
            _Env = env;
            _userManager = userManager; 

            //mPM = new GIS.Model.ProductModel(config, user);
            //mWM = new WarehouseModel(config, user);

            _context = context;
        }


        public IActionResult Index()
        {

            return View();
        }

        [Authorize]
        public IActionResult Inventory(int setid)
        {
            InventoryOverviewVM result = new InventoryOverviewVM();
            WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
            try
            {
                result.ProductPlaceList = mWM.GetWarehouseLocationList("*", true);
                result.ProductNo = "";
                result.Warehouse = "";
                result.InventorySetList = InventoryLib.GetAllInventorySet();

                if (setid != 0)
                {
                    result.CurrentInventorySet = InventoryLib.GetInventorySet(setid);
                    result.CurrentInventorySetId = setid;
                    result.InventoryList = InventoryLib.GetAllInventorOnSet(setid).OrderBy(p => p.ProductNo).ToList();

                }
                else
                {
                    result.CurrentInventorySet = InventoryLib.GetAllInventorySet().First();
                    result.CurrentInventorySetId = result.CurrentInventorySet.ID;

                    if (result.CurrentInventorySet != null)
                    {
                        result.InventoryList = InventoryLib.GetAllInventorOnSet(result.CurrentInventorySetId).OrderByDescending(i => i.ID).ToList(); //OrderBy(p => p.ProductNo).ToList();
                    }
                }

                foreach (Inventory inv in result.InventoryList)
                {
                    // Mark all invetory on same product
                    if (result.InventoryList.Where(p => p.ProductNo == inv.ProductNo).Count() > 1)
                    {
                        inv.Note = "M";
                    }
                }
                
            }
            catch (Exception e)
            {
                logger.Error("GET: Error in FloatingStockLocation.Inventory()", e);
            }

            return View(result);
        }


        [HttpPost]
        [Authorize]
        public IActionResult Inventory(InventoryOverviewVM inv)
        {
            InventoryOverviewVM result = inv;
            if(result.ProductPlaceList==null)
            {
                result.ProductPlaceList = new List<ProductPlaceWithWarehouse>();
                result.ProductPlaceList.Add(new ProductPlaceWithWarehouse() { WarehouseNumbers = new List<WarehouseNumber>() { new WarehouseNumber() { Id = "-" } }, IsFree = false });
            }
            WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                // Only if a inventory is sent, this call can also be a selection of InventorySet from Select
                if (!string.IsNullOrEmpty(inv.ProductNo))
                {
                    Product product = mPM.GetProductById(inv.ProductNo);

                    Inventory inventory = new Inventory();

                    inventory.ProductNo = inv.ProductNo;
                    //inventory.Place = inv.Place;
                    inventory.WarehouseNo = inv.Warehouse;
                    inventory.Amount = inv.Amount;
                    inventory.Date = DateTime.Now.ToString("yyMMdd");
                    inventory.Time = DateTime.Now.ToString("HHmmss");
                    inventory.InventorySetId = inv.CurrentInventorySetId;
                    inventory.InventoryDone = false;

                    if (!string.IsNullOrEmpty(inv.Warehouse))
                    {
                        WarehouseNumber wn = product.WarehouseList.Where(w => w.Id == inv.Warehouse).FirstOrDefault();

                        
                        if (wn?.Id != "")
                        {
                            inventory.OriginalAmount = wn.Stock;
                            inventory.VIPAtTime = GeneralFunctions.getDecimalFromStr(wn.VIP);
                            inventory.Place = wn.WarehouseLocation;

                            try
                            {
                                //if (!string.IsNullOrEmpty(inv.Place))
                                //{
                                //    wn.WarehouseLocation = inv.Place;
                                //    inventory.Place = inv.Place;
                                //}

                                //// Update warehouseno with last modified date
                                //if (!string.IsNullOrEmpty(inv.FirstMovmentDate))
                                //{
                                //    wn.Description2 = inv.FirstMovmentDate;
                                //}

                                //// Only update if one re more of the fields ar changed
                                //if(!string.IsNullOrEmpty(inv.Place) || !string.IsNullOrEmpty(inv.FirstMovmentDate))
                                //{
                                //    mWM.UpdateWarehouse(wn);
                                //}

                                if (InventoryLib.GetAllInventorOnSet(inv.CurrentInventorySetId).Find(i => i.WarehouseNo == wn.Id) != null)
                                {
                                    result.Message = "Inventory on WarehouseNo " + inv.Warehouse + " already exists in list";
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Error("UpdatingAGL.ANM2", e);
                            }

                        }
                    }
                    else
                    {
                        inventory.OriginalAmount = product.Stock.Value;
                        inventory.VIPAtTime = product.AverageCostPrice.Value;
                    }

                    InventoryLib.AddInventoryBulk(new List<Inventory> { inventory });

                    // Check if WarehouseNo already in set, if so we warn about it
                }

                result.ProductNo = "";
                result.Warehouse = "";
                //result.Place = "";
                result.CurrentInventorySet = InventoryLib.GetInventorySet(inv.CurrentInventorySetId);
                result.CurrentInventorySetId = inv.CurrentInventorySetId;
                result.InventorySetList = InventoryLib.GetAllInventorySet();
                result.InventoryList = InventoryLib.GetAllInventorOnSet(inv.CurrentInventorySetId).OrderByDescending(i=>i.ID).ToList();

                foreach (Inventory inventory in result.InventoryList)
                {
                    // Mark all invetory on smae product
                    if (result.InventoryList.Where(p => p.ProductNo == inventory.ProductNo).Count() > 1)
                    {
                        inventory.Note = "M";
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("POST: Error in FloatingStockLocation.Inventory()", e);
            }

            return View(result);
        }

        [Authorize]
        public string FindProduct(string search)
        {
            Product result = new Product();

            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                var p = mPM.GetProductById(search);
                
                if (p?.ProductNo != null)
                {
                    if(!string.IsNullOrEmpty(p.ProductText[14]?.Value))
                    {
                        // We borrow this field to simplify js code in view....
                        p.WarehouseNo = p.ProductText[14].Value;
                    }
                    
                    result = p;
                }
                else
                {
                    result = new Product { ProductNo = null, Description = "" };
                }
            }
            catch (Exception e)
            {

            }

            return JsonConvert.SerializeObject(result);
        }

        [Authorize]
        public string GetWarehouseList(string product, int locationid)
        {
            GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<WarehouseNumber> result = new List<WarehouseNumber>();

            InventorySet invSet = InventoryLib.GetInventorySet(locationid);

            Product prod = mPM.GetProductById(product);
            prod.WarehouseList.RemoveAll(w => w.Stock == 0);
            foreach (WarehouseNumber wn in prod.WarehouseList)
            {
                if (wn.WarehouseLocation.StartsWith(invSet?.LocationId) && wn.SaleableCode != "y")
                {
                    result.Add(wn);
                }
            }

            return JsonConvert.SerializeObject(result);
        }


        [Authorize]
        public IActionResult AddToInventorySet(Inventory inventory)
        {
            try
            {


            }
            catch (Exception e)
            {
                logger.Error("Error in AddToInventorySet", e);
            }

            return View();
        }

        [Authorize]
        public IActionResult DeleteInventory(int id)
        {
            Inventory result = null;

            try
            {
                result = InventoryLib.GetInventory(id);
            }
            catch (Exception)
            {
            }

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public IActionResult DeleteInventory(Inventory param)
        {
            try
            {
                Inventory addedSet = InventoryLib.DeleteInventory(param);
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Inventory", new { setid = param.InventorySetId });
        }

        [Authorize]
        public IActionResult EmtySetList(int setid)
        {
            InventorySet result = null;

            try
            {
                result = InventoryLib.GetInventorySet(setid);
            }
            catch (Exception)
            {
            }

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public IActionResult EmtySetList(InventorySet param)
        {
            try
            {
                InventoryLib.DeleteAllInventorOnSet(param.ID);
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Inventory", new { setid = param.ID });
        }

        [Authorize]
        public IActionResult SumInventory(int setid, string sortby)
        {
            InventoryOverviewVM result = new InventoryOverviewVM();

            try
            {
                result.CurrentInventorySet = InventoryLib.GetInventorySet(setid);
                result.CurrentInventorySetId = setid;
                result.InventoryList = InventoryLib.GetAllInventorOnSet(setid);

                foreach (Inventory inv in result.InventoryList)
                {
                    // Mark all invetory on same product
                    if (result.InventoryList.Where(p => p.WarehouseNo == inv.WarehouseNo).Count() > 1)
                    {
                        inv.Note = "W";
                    }
                }

                if(string.IsNullOrEmpty(sortby))
                {
                    result.InventoryList = result.InventoryList.OrderByDescending(o => o.ProductNo).ToList();
                }
                else if (sortby == "batch")
                {
                    result.InventoryList = result.InventoryList.OrderByDescending(o => o.WarehouseNo).ToList();
                }
                else if (sortby == "place")
                {
                    result.InventoryList = result.InventoryList.OrderByDescending(o => o.Place ).ToList();
                }
                else if (sortby == "diff")
                {
                    result.InventoryList = result.InventoryList.OrderByDescending(o => o.Amount - o.OriginalAmount).ToList();
                }
                else if (sortby == "diffvalue")
                {
                    result.InventoryList = result.InventoryList.OrderByDescending(o => ((o.Amount - o.OriginalAmount) * o.VIPAtTime)).ToList();
                }

            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        [Authorize]
        public IActionResult SumInventoryOnProduct(int setid)
        {
            InventoryOverviewVM result = new InventoryOverviewVM();

            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                result.CurrentInventorySet = InventoryLib.GetInventorySet(setid);
                result.CurrentInventorySetId = setid;
                List<Inventory> allInventory = InventoryLib.GetAllInventorOnSet(setid);

                result.InventoryList = new List<Inventory>();

                var sumOnProduct = allInventory.GroupBy(p => p.ProductNo);

                foreach (var group in sumOnProduct)
                {
                    Inventory sum = new Inventory();

                    sum.ProductNo = group.Key;
                    Product product = mPM.GetProductById(sum.ProductNo);
                    sum.ProductDescription = product?.Description;

                    foreach (Inventory inv in group)
                    {
                        sum.Amount += inv.Amount;
                        sum.OriginalAmount += inv.OriginalAmount;
                        sum.VIPAtTime += Math.Round(inv.VIPAtTime * inv.OriginalAmount, 2);
                    }

                    try
                    {
                        sum.VIPAtTime = Math.Round(sum.VIPAtTime / sum.OriginalAmount, 2);
                    }catch { }
                    

                    result.InventoryList.Add(sum);
                }
            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        [Authorize]
        public IActionResult CreateInventoryFile(int setid)
        {
            InventoryFileVM result = new InventoryFileVM();
            InventoryFileResult fileResult = new InventoryFileResult();

            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                var inventorySet = InventoryLib.GetInventorySet(setid);
                var inventorys = InventoryLib.GetAllInventorOnSet(setid);

                InventoryFileParam inventory = new InventoryFileParam();
                inventory.ReturnInventoryFile = true;
                inventory.Inventorys = new List<InventoryParam>();

                result.SetID = setid;
                result.SetName = inventorySet.Description;

                var groupInventorys = inventorys.GroupBy(i => new { i.WarehouseNo, i.ProductNo });

                foreach(var group in groupInventorys)
                {
                    try
                    {
                        Product product = mPM.GetProductById(group.Key.ProductNo);
                        InventoryParam param = new InventoryParam();

                        param.ProductNo = group.Key.ProductNo;
                        param.WarehouseNo = group.Key.WarehouseNo;
                        param.Amount = group.Sum(a => a.Amount);
                        param.Date = group.Last().Date;
                        param.Time = group.Last().Time;
                        param.PlaceName = "";
                        param.PlaceNo = "";
                        param.Note = group.Last().Note;
                        param.User = "";
                        param.Terminal = "";
                        param.Rating = "";

                        inventory.Inventorys.Add(param);
                    }
                    catch(Exception e)
                    {
                        logger.Error("FloatingSTockLocation.CreateInventoryFile()", e);
                    }
                }

                fileResult = mPM.CreateInventoryFile(inventory);

                string path = Path.Combine(_Env.ContentRootPath, "InventoryFiles");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                result.CreatedTime = DateTime.Now;
                result.Filename = "inventory_" + result.CreatedTime.ToString("yyyyMMdd_hhmmss") + ".txt";
                string file = Path.Combine(path, result.Filename);
                System.IO.File.WriteAllText(file, fileResult.InventoryFile);
            }
            catch (Exception e)
            {
                logger.Error("Error in ExecuteInventory", e);
            }

            return View(result);
        }

        [Authorize]
        public IActionResult GetInventoryFile(string filename, string how)
        {
            InventoryFileVM result = new InventoryFileVM();
            string fileContent = "";
            

            try
            {
                string path = Path.Combine(_Env.ContentRootPath, "InventoryFiles");
                string file = Path.Combine(path, filename);

                if (System.IO.File.Exists(file))
                {
                   fileContent = System.IO.File.ReadAllText(file);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in ExecuteInventory", e);
            }

            if(how == "download")
            {
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(fileContent)), "text/plain") { FileDownloadName = filename }; 
            }
            else
            {
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(fileContent)), "text/plain");
            }
            
        }

        [Authorize]
        public IActionResult ShowAllProductsNotInSet(int setid)
        {
            ProductsNotInSetVM result = new ProductsNotInSetVM();
            result.ProductList = new List<MissingProduct>();

            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                result.Set = InventoryLib.GetInventorySet(setid);

                if(result.Set != null)
                {
                    List<Inventory> invProdList = InventoryLib.GetAllInventorOnSet(setid);
                    List<Product> prodList = mPM.GetProductList("1", "*", "TYP!=z;");

                    foreach(Product p in prodList)
                    {
                        // Check if any warehouse with current location is found, and that it has som sort of stock
                        var warehouse = p.WarehouseList.Where(w => w.WarehouseLocation.StartsWith(result.Set.LocationId) && w.Stock > 0);
                        if(warehouse.Count() > 0)
                        {
                            // Check if this product isin inventorylist
                            if(invProdList.Where(i=>i.ProductNo == p.ProductNo).Count() == 0)
                            {
                                // This product is not in the list and we add it to list of products that not yet been under stocktaking
                                foreach(var wh in warehouse)
                                {
                                    result.ProductList.Add(new MissingProduct { ProductNo = p.ProductNo, Description = p.Description, Place = wh.WarehouseLocation, WarehouseNo = wh.Id, Stock = wh.Stock });
                                }
                                
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                logger.Error("Error in ShowAllProductsNotInSet", e);
            }

            return View(result);
        }
    }
}
