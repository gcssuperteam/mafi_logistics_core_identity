﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Babc_logistic_core.GIS.DTO;
//using Babc_logistic_core.GIS.Model;
////using System.Web.Mvc;
//using Babc_logistic_core.Models;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Mvc;
//using AllowAnonymousAttribute = Microsoft.AspNetCore.Authorization.AllowAnonymousAttribute;
//using Controller = Microsoft.AspNetCore.Mvc.Controller;

//namespace Babc_logistic_core.Controllers
//{
//    public class AccountController : Controller
//    {
//        public AccountController() //: this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
//        {
//        }

//        //public AccountController(UserManager<ApplicationUser> userManager)
//        //{
//        //    UserManager = userManager;
//        //}

//        //public UserManager<ApplicationUser> UserManager { get; private set; }

//        //
//        // GET: /Account/Login
//        [AllowAnonymous]
//        public ActionResult Login(string returnUrl)
//        {
//            ViewBag.ReturnUrl = returnUrl;
//            return View();
//        }

//        //
//        // POST: /Account/Login
//        [HttpPost]
//        [AllowAnonymous]
//        [ValidateAntiForgeryToken]
//        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
//        {
//            if (ModelState.IsValid)
//            {
//                UserModel um = new UserModel("", true);
//                string token = um.Login(model.UserName, model.Password);
//                //um.Token = token;
//                User user = null;

//                if (!string.IsNullOrEmpty(token))
//                {
//                    token = token.Trim(new char[] { '"', '\\', '/' });
//                    Session["token"] = token;
//                    user = um.GetUser(token, model.UserName);
//                }

//                //var user = await UserManager.FindAsync(model.UserName, model.Password);
//                if (user != null)
//                {
//                    RebornPrincipalSerializeModel serializeModel = new RebornPrincipalSerializeModel();
//                    serializeModel.UserId = user.Id;
//                    serializeModel.FirstName = user.Name;
//                    serializeModel.CurrentCompany = user.CurrentCompany;
//                    serializeModel.UserToken = token;
//                    serializeModel.UserRole = user.UserRoleName;

//                    JavaScriptSerializer serializer = new JavaScriptSerializer();

//                    string userData = serializer.Serialize(serializeModel);
//                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
//                             1,
//                             model.UserName,
//                             DateTime.Now,
//                             DateTime.Now.AddMinutes(480),
//                             false,
//                             userData);

//                    string encTicket = FormsAuthentication.Encrypt(authTicket);
//                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
//                    Response.Cookies.Add(faCookie);

//                    return RedirectToLocal(returnUrl);
//                }
//                else
//                {
//                    ModelState.AddModelError("", "Invalid username or password.");
//                }
//            }

//            // If we got this far, something failed, redisplay form
//            return View();
//        }

 
//         //
//        // POST: /Account/LogOff
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult LogOff()
//        {
//            //FormsAuthentication.SignOut();
//            //UserModel um;

//            //if (Session["token"] != null)
//            //{
//            //    um = new UserModel(Session["token"].ToString(), true);
//            //    Session.Remove("token");
//            //}
//            //else
//            //{
//            //    um = new UserModel("anonymous", true);
//            //}

//            //if (Session["uid"] != null)
//            //{
//            //    um.Logoff(Session["uid"].ToString());
//            //    Session.Remove("uid");
//            //}

//            //AuthenticationManager.SignOut();
//            return RedirectToAction("Index", "Home");
//        }

 

  

//        //protected override void Dispose(bool disposing)
//        //{
//        //    if (disposing && UserManager != null)
//        //    {
//        //        UserManager.Dispose();
//        //        UserManager = null;
//        //    }
//        //    base.Dispose(disposing);
//        //}

//        //// Used for XSRF protection when adding external logins
//        //private const string XsrfKey = "XsrfId";

//        //private IAuthenticationManager AuthenticationManager
//        //{
//        //    get
//        //    {
//        //        return HttpContext.GetOwinContext().Authentication;
//        //    }
//        //}

//        //private async Task SignInAsync(ApplicationUser user, bool isPersistent)
//        //{
//        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
//        //    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
//        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
//        //}

//        //private void AddErrors(IdentityResult result)
//        //{
//        //    foreach (var error in result.Errors)
//        //    {
//        //        ModelState.AddModelError("", error);
//        //    }
//        //}

//        //private bool HasPassword()
//        //{
//        //    var user = UserManager.FindById(User.Identity.GetUserId());
//        //    if (user != null)
//        //    {
//        //        return user.PasswordHash != null;
//        //    }
//        //    return false;
//        //}

//        //public enum ManageMessageId
//        //{
//        //    ChangePasswordSuccess,
//        //    SetPasswordSuccess,
//        //    RemoveLoginSuccess,
//        //    Error
//        //}

//        //private ActionResult RedirectToLocal(string returnUrl)
//        //{
//        //    if (Url.IsLocalUrl(returnUrl))
//        //    {
//        //        return Redirect(returnUrl);
//        //    }
//        //    else
//        //    {
//        //        return RedirectToAction("Index", "Home");
//        //    }
//        //}

//        //private class ChallengeResult : HttpUnauthorizedResult
//        //{
//        //    public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
//        //    {
//        //    }

//        //    public ChallengeResult(string provider, string redirectUri, string userId)
//        //    {
//        //        LoginProvider = provider;
//        //        RedirectUri = redirectUri;
//        //        UserId = userId;
//        //    }

//        //    public string LoginProvider { get; set; }
//        //    public string RedirectUri { get; set; }
//        //    public string UserId { get; set; }

//        //    public override void ExecuteResult(ControllerContext context)
//        //    {
//        //        var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
//        //        if (UserId != null)
//        //        {
//        //            properties.Dictionary[XsrfKey] = UserId;
//        //        }
//        //        context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
//        //    }
//        //}
//    }
//}