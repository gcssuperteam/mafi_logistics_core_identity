﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using GDN.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Babc_logistic_core.Controllers
{
    public class GoodsController : Controller
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //OrderModel mOM;
        //CustomerModel mCM;
        //GenericTableModel mGTM;
        //BaseDataModel mBDM;
        private IMemoryCache _cache;
        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;


        public GoodsController(IConfiguration config, IMemoryCache cache, UserManager<ApplicationUser> userManager)
        {
            //mOM = new OrderModel(config, user);
            //mCM = new CustomerModel(config, user);
            //mGTM =  new GenericTableModel(config, user);
            //mBDM = new BaseDataModel(config, user);
            _Config = config;
            _userManager = userManager;

            _cache = cache;
        }

        // GET: Goods
        //[Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Index(string fsno)
        {
            List<GoodsViewModel> result = new List<GoodsViewModel>();

            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                Transport transport = mOM.GetTransportForDeliverNote(fsno);

                if(_cache.Get("delivernotes") != null)
                {
                    result = (List<GoodsViewModel>)_cache.Get("delivernotes");
                    //result = JsonConvert.DeserializeObject<List<GoodsViewModel>>(_cache.Get("delivernotes").ToString());
                }

                //if (HttpContext.Session.GetString("delivernotes") != null)
                //{

                //    result = JsonConvert.DeserializeObject<List<GoodsViewModel>>(HttpContext.Session.GetString("delivernotes"));
                //    //result = (List<GoodsViewModel>)Session["delivernotes"];
                //}

                if (!string.IsNullOrEmpty(transport?.DeliverNo))
                {
                    GoodsViewModel gvm = new GoodsViewModel();

                    gvm.DeliverNoteNo = transport.DeliverNo;
                    gvm.OrderNo = transport.OrderNo;
                    gvm.PackageCount = GeneralFunctions.getStrFromDecimal(transport.PackageAmount1 + transport.PackageAmount2 + transport.PackageAmount3);

                    result.Add(gvm);

                    _cache.Set("delivernotes", result);
                    //HttpContext.Session.SetString ("delivernotes", JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GoodsController.Index (POST)", e);
            }

            return View(result);
        }

        [Authorize]
        public ActionResult SubmitDeliver()
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
                GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);

                List<GoodsViewModel> deliverList = (List<GoodsViewModel>)_cache.Get("delivernotes");

                foreach (GoodsViewModel gvm in deliverList)
                {
                    OrderHead oh = mOM.GetOrderById(gvm.OrderNo, false);

                    // ** OM INTE BTEVILLKOR HAR V FLAGGA ***
                    BaseTable btPayment = mBDM.GetBaseTable("01", oh.PaymentTerms);

                    // Change invoice flag ONLY if paymentterms invoiceflag is NOT "V" and PickListState is NOT "F"
                    if (btPayment?.Code2.ToUpper().CompareTo("V") != 0 && oh.PickListState != "F")
                    {
                        UpdateTableIndexObject update = new UpdateTableIndexObject();

                        update.TableName = "HKA";
                        update.Key = gvm.DeliverNoteNo.PadRight(6) + "  1";
                        update.IndexFields = new List<FieldObject> { new FieldObject { FieldName = "HNR", FieldValue = gvm.DeliverNoteNo } };
                        update.Fields = new List<FieldObject> { new FieldObject { FieldName = "FAF", FieldValue = "1" } };

                        mGTM.UpdateTableByIndex(update, "false");
                    }

                    UpdateOrderRowTextZero(gvm.OrderNo, gvm.DeliverNoteNo);
                }

                _cache.Remove("delivernotes");
                //HttpContext.Session.Clear(); // Session["delivernotes"] = null;
            }
            catch (Exception e)
            {
                logger.Error("Error in GoodsController.SubmitDeliver()", e);
            }

            return Redirect("Index");
        }

        private void UpdateOrderRowTextZero(string orderNo, string fsno)
        {
            List<OrderRowText> orTextList = new List<OrderRowText>();

            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);

                OrderHead order = mOM.GetOrderById(orderNo, false);
                Customer customer = mCM.GetCustomerById(order.DeliverCustomerNo);

                OrderRowText orText = new OrderRowText();
                string date = System.DateTime.Today.ToString("yyMMdd");

                orText.OrderNo = orderNo;
                orText.DeliverNoteState = "1";
                //orText.InvoiceState = "0";
                orText.OrderConfirmationState = "1";
                orText.PickListState = "1";
                orText.RowNo = "0";
                orText.TextRowNo = "255";
                orText.Text = "Delivery note " + fsno + " was shipped " + date;

                if (string.IsNullOrEmpty(customer.UnifiedInvoice))
                {
                    orText.InvoiceState = "0";
                }

                orTextList.Add(orText);
                
                
                
                mOM.UpdateOrderRowTextList(orTextList);

            }
            catch (Exception e)
            {
                logger.Error("Error in GoodsController.UpdateOrderRowTextZero()", e);
            }
        }

        [Authorize]
        public ActionResult ClearSession()
        {
            try
            {
                HttpContext.Session.Clear(); //Session["delivernotes"] = null;
            }
            catch (Exception e)
            {
                logger.Error("Error in GoodsController.ClearSession()", e);
            }

            return Redirect("Index");
        }
    }
}