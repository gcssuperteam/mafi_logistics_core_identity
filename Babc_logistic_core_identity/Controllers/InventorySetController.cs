﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Babc_logistic_core.Database;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Mvc;

namespace Babc_logistic_core.Controllers
{
    public class InventorySetController : Controller
    {
        public IActionResult Index()
        {
            List<InventorySet> result = InventoryLib.GetAllInventorySet();

            return View(result);
        }

        public IActionResult Create()
        {

            try
            {

            }
            catch (Exception)
            {

            }

            return View();
        }

        [HttpPost]
        public IActionResult Create(InventorySet param)
        {

            try
            {
                InventorySet addedSet = InventoryLib.AddInventorySet(param);
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            InventorySet result = null;

            try
            {
                result = InventoryLib.GetInventorySet(id);
            }
            catch (Exception)
            {
            }

            return View(result);
        }

        [HttpPost]
        public IActionResult Delete(InventorySet param)
        {

            try
            {
                InventorySet addedSet = InventoryLib.DeleteInventorySet(param);
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            InventorySet result = null;

            try
            {
                result = InventoryLib.GetInventorySet(id);
            }
            catch (Exception)
            {
            }

            return View(result);
        }

        [HttpPost]
        public IActionResult Edit(InventorySet param)
        {

            try
            {
                InventorySet addedSet = InventoryLib.UpdateInventorySet(param);
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Index");
        }
    }
}
