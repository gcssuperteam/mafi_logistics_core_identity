using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Babc_logistic_core.Database;
using Microsoft.AspNetCore.Identity;
using System.Security.Cryptography;

namespace Babc_logistic_core.Controllers
{
    public class OrderOverViewController : Controller
    {


        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private OrderModel mOM;
        //private BaseDataModel mBDM;
        //private LogisticModel mLOM;
        //private UserModel mUM;
        //private CustomerModel mCM;
        //private PrintingModel mPM;
        //private ManufacturingModel mMM;
        //private GenericTableModel mGTM;
        //private Babc_logistic_core.GIS.Model.ProductModel mPRM;

        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;

        private string mGeneralIndexSearchCount = "0";

        public OrderOverViewController(IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            //mOM = new OrderModel(config, user);
            //mBDM = new BaseDataModel(config, user);
            //mLOM = new LogisticModel(config, user);
            //mUM = new UserModel(config, user);
            //mCM = new CustomerModel(config, user);
            //mPM = new PrintingModel(config, user);
            //mMM = new ManufacturingModel(config, user);
            //mGTM = new GenericTableModel(config, user);
            _Config = config;
            _userManager = userManager;

            //mPRM = new Babc_logistic_core.GIS.Model.ProductModel(config, user);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Index(string search_value, string search_type)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            List<OrderHead> lst = new List<OrderHead>();

            if (string.IsNullOrEmpty(search_value))
            {
                return View();
            }

            if (search_type.Equals("Datum"))
            {
                lst = mOM.GetOrderByIdxList("1", "*", "LDT>=" + search_value, false, 20, true);
            }
            else if (search_type.Equals("Kund"))
            {
                lst = mOM.GetOrderByIdxList("2", search_value, "LEF<=4", false, 50, false);
            }
            else if (search_type.Equals("Säljarkod"))
            {
                lst = mOM.GetOrderByIdxList("4", search_value, "*", false, 50, false);
            }

            List<OrderHead> sortedList = lst.OrderByDescending(o => o.OrderNo).ToList();

            ViewBag.search_value = search_value;
            ViewBag.search_type = search_type;

            return View(sortedList);
        }

        [Authorize]
        public IActionResult PickList(string startDate)
        {
            var user = _userManager.GetUserAsync(User)?.Result;
            DateTime date = new DateTime();

            if(string.IsNullOrEmpty(startDate))
            {
                date = DateTime.Now;
                ViewBag.startDate = date;
                ViewBag.nextDate = date.AddDays(1).Date.ToString("yyMMdd");
                ViewBag.previousDate = date.AddDays(-1).Date.ToString("yyMMdd");
            }
            else
            {
                date = GeneralFunctions.getDateFromStr(string.Format(startDate));
                ViewBag.startDate = date.ToString("yyMMdd");
                ViewBag.nextDate = date.AddDays(1).Date.ToString("yyMMdd");
                ViewBag.previousDate = date.AddDays(-1).Date.ToString("yyMMdd");
            }

            LogisticModel mLOM = new LogisticModel(_Config, user);
            ManufacturingModel mMM = new ManufacturingModel(_Config, _userManager.GetUserAsync(User)?.Result);
            OrderOverviewVM result = new OrderOverviewVM();

            List<Day> templist = new List<Day>();
              result.Day_0 = new Day { Orders = new List<OrderHead>(), DayName = "Rest", DayNumber = 0 };
            result.Day_1 = new Day { Orders = new List<OrderHead>(), DayName = date.AddDays(0).DayOfWeek.ToString(), DayNumber = 1 };

            List<OrderHead> lst = new List<OrderHead>();
            StringBuilder filter = new StringBuilder();
            List<OrderHead> tempLst = new List<OrderHead>();

            Day tempDay = new Day();
            PickListParam olp = new PickListParam();
            string day0 = string.Empty;
            string day1 = string.Empty;
            string day2 = string.Empty;
            string day3 = string.Empty;
            string day4 = string.Empty;
            string day5 = string.Empty;

            try
            {
                List<FilterDTO> filterList = getFilterList("FILTERS");
                olp.Filter = parseFilterConstants(filterList[0].Filter);

                if (filterList[0].UseCache == true)
                {
                    olp.Cache = new CacheParam { CacheId = user.GisToken + "_supplie", ReadFrom = true, UpdateTo = false };
                }

            }
            catch (Exception e)
            {
                logger.Error("Error setting filter", e);
            }

            olp.Index = 1;
            olp.IndexFilter = "*";
            olp.IndexCount = int.Parse(mGeneralIndexSearchCount);
            olp.OrderSerie = "A";
            olp.IncludeDeliveredRows = false;
            olp.WithRows = true;
            olp.MaxCount = 200;
            olp.ReverseReading = true;
            olp.IncludeProduct = true;
            olp.Filter += ";BLT<=" + GeneralFunctions.getStrFromDate(date.AddDays(30));
           
               
            int i = 1;
            int b = 1;
            int c = 0;
            List<OrderHead> temp = new List<OrderHead>();

            lst = mLOM.GetOrderListWithAdditionalOrder(olp);
            lst.RemoveAll(x => string.IsNullOrEmpty(x.PreferedDeliverDate));
            // var templist_day1 = lst.FindAll(p => p.PreferedDeliverDate <= GeneralFunctions.getStrFromDate(DateTime.Now.AddDays(b)));
            //result.Day_1 = new Day { Orders = new List<OrderHead>(), DayName = DateTime.Now.DayOfWeek.ToString(), DayNumber = 1 };
            //foreach (var oh in templist_day1)
            //{
            //  result.Day_1.Orders.Add(oh);
            // }


            var templist1 = new List<OrderHead>();

            string referenceDate = string.Empty;
            if (lst != null)
            {
                foreach (var oh in lst)
                {


                    // Mark order that has materialplanned rows
                    oh.HasMaterialPlannedRows = (oh.OrderRows.Where(r => r.MTF.CompareTo("0") > 0)?.ToList().Count > 0);

                    oh.IsWholeOrderDeliverable = true;
                    oh.NonDeliverableRows = 0;
                   
             
                try
                {
                 
                    
                    oh.OrderRows.RemoveAll(r => string.IsNullOrEmpty(r.ProductNo));
                        
                }
                    catch (Exception e)
                    {
                        logger.Debug("Error : " + e.Message);
                    }

                    decimal amountTvk = 0;

                    foreach (OrderRow or in oh.OrderRows)
                    {

                        if (or.MDF.Equals("1") || or.MDF.Equals("2"))
                        {
                            //List<MaterialRow> mr = mMM.getMaterialRowList(or.OrderNo, or.RowNo.ToString());
                            //materials.Add(or.OrderNo + or.RowNo.ToString(), mr);
                            or.MaterialRows = mMM.getMaterialRowList(or.OrderNo, or.RowNo.ToString());
                        }

                        if (or.Product != null)
                        {

                            if (or.Amount.Value <= or.Product.Stock || or.Product.StockUpdateType == "9" || or.MTF=="5")

                                oh.IsWholeOrderDeliverable = true;

                            else if (or.MaterialRows != null)
                            {
                                bool HasMeterialRowsStock = false;
                                if (or.MaterialRows.Count > 0)
                                {
                                    foreach (var mtr in or.MaterialRows)
                                    {
                                        if ((mtr.Amount - mtr.DeliveredAmount) <= mtr.Product.Stock)
                                        {
                                            oh.IsWholeOrderDeliverable = true;
                                        }
                                        else
                                        {
                                            oh.IsWholeOrderDeliverable = false;
                                            oh.NonDeliverableRows++;
                                        }
                                    }
                                }


                            }

                            else
                            {
                               
                                    oh.IsWholeOrderDeliverable = false;
                                
                                oh.NonDeliverableRows++;
                            }

                            if (or.Product.OriginType?.Equals("3") == true)
                            {
                                amountTvk += or.Amount.Value;
                            }
                        }
                        else
                        {
                            oh.NonDeliverableRows++;
                        }


                    }

                    try
                    {
                        OrderRow orDelDate = oh.OrderRows.Where(f => f.DeliverState != "5").OrderBy(r => r.PreferedDeliverDate).First();

                        if (orDelDate != null)
                        {
                            if (orDelDate.OrderNo.Trim() == oh.OrderNo.Trim())
                            {
                                oh.PreferedDeliverDate = orDelDate.PreferedDeliverDate;
                                
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        logger.Debug("Error : " + e.Message);
                    }

                    if (amountTvk > 100)
                    {
                        oh.isLargeOrder = true;
                    }
                    else
                    {
                        oh.isLargeOrder = false;
                    }
                    if (GeneralFunctions.getDateFromStr(oh.PreferedDeliverDate).Date.CompareTo(date.Date) < 0)
                    {

                        bool Materialrowstock = false;

                        if (oh.OrderRows.Any(z => z.MaterialRows != null))
                        {
                            if (oh.OrderRows.Any(x => x.MaterialRows != null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                            {
                                Materialrowstock = false;
                            }
                            else
                            {
                                Materialrowstock = true;
                            }

                        }
                        if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                        {
                            oh.IsWholeOrderDeliverable = false;
                        }
                        if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                        {
                            oh.IsWholeOrderDeliverable = false;
                        }



                        result.Day_0.Orders.Add(oh);
                    }
                    if (GeneralFunctions.getDateFromStr(oh.PreferedDeliverDate).Date.CompareTo(date.Date) == 0)
                    {

                        bool Materialrowstock = false;

                        if (oh.OrderRows.Any(z => z.MaterialRows != null))
                        {
                            if (oh.OrderRows.Any(x => x.MaterialRows != null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                            {
                                Materialrowstock = false;
                            }
                            else
                            {
                                Materialrowstock = true;
                            }

                        }
                        if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                        {
                            oh.IsWholeOrderDeliverable = false;
                        }
                        if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                        {
                            oh.IsWholeOrderDeliverable = false;
                        }


                        result.Day_1.DayDate = GeneralFunctions.getStrFromDate(date.AddDays(b));
                        result.Day_1.Orders.Add(oh);
                    }

                }
           
                while (b <= 30 && c <= 7)
                {


                    templist1 = lst.FindAll(p => p.PreferedDeliverDate == GeneralFunctions.getStrFromDate(date.AddDays(b)));

                    if (templist1 != null || templist1.Count == 0)
                    {
                        if (result.Day_1.Orders.Count==0 && templist1.Count > 0)
                        {

                           foreach (var oh in templist1)
                            {
                                if (oh.OrderRows.Count > 0)
                                {
                                    bool Materialrowstock = false;

                                    if (oh.OrderRows.Any(z => z.MaterialRows != null))
                                    {
                                        if (oh.OrderRows.Any(x => x.MaterialRows != null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                                        {
                                            Materialrowstock = false;
                                        }
                                        else
                                        {
                                            Materialrowstock = true;
                                        }

                                    }
                                    if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    result.Day_1.DayName = date.AddDays(b).DayOfWeek.ToString();
                                    
                                    result.Day_1.DayDate = GeneralFunctions.getStrFromDate(date.AddDays(b));
                                    result.Day_1.Orders.Add(oh);
                                }
                            }
                            
                        }
                        else if (c == 0 && templist1.Count > 0)
                        {

                            result.Day_2 = new Day { Orders = new List<OrderHead>(), DayName = date.AddDays(b).DayOfWeek.ToString(), DayNumber = 2 };

                            foreach (var oh in templist1)
                            {
                                if (oh.OrderRows.Count > 0)
                                {
                                    bool Materialrowstock = false;

                                    if (oh.OrderRows.Any(z => z.MaterialRows != null))
                                    {
                                        if (oh.OrderRows.Any(x => x.MaterialRows != null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                                        {
                                            Materialrowstock = false;
                                        }
                                        else
                                        {
                                            Materialrowstock = true;
                                        }

                                    }
                                    if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    result.Day_2.DayDate = GeneralFunctions.getStrFromDate(date.AddDays(b));
                                    result.Day_2.Orders.Add(oh);
                                }
                            }
                            c++;
                        }
                        else if (c == 1 && templist1.Count > 0)
                        {
                            result.Day_3 = new Day { Orders = new List<OrderHead>(), DayName = date.AddDays(b).DayOfWeek.ToString(), DayNumber = 3 };
                            foreach (var oh in templist1)
                            {
                                if (oh.OrderRows.Count > 0)
                                {
                                    bool Materialrowstock = false;

                                    if (oh.OrderRows.Any(z => z.MaterialRows != null))
                                    {
                                        if (oh.OrderRows.Any(x => x.MaterialRows != null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                                        {
                                            Materialrowstock = false;
                                        }
                                        else
                                        {
                                            Materialrowstock = true;
                                        }

                                    }
                                    if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    result.Day_3.DayDate = GeneralFunctions.getStrFromDate(date.AddDays(b));
                                    result.Day_3.Orders.Add(oh);
                                }
                            }
                            c++;
                        }
                        else if (c == 2 && templist1.Count > 0)
                        {
                            result.Day_4 = new Day { Orders = new List<OrderHead>(), DayName = date.AddDays(b).DayOfWeek.ToString(), DayNumber = 4 };
                            foreach (var oh in templist1)
                            {
                                if (oh.OrderRows.Count > 0)
                                {
                                    bool Materialrowstock = false;

                                    if (oh.OrderRows.Any(z => z.MaterialRows != null))
                                    {
                                        if (oh.OrderRows.Any(x => x.MaterialRows != null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                                        {
                                            Materialrowstock = false;
                                        }
                                        else
                                        {
                                            Materialrowstock = true;
                                        }

                                    }
                                    if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    result.Day_4.DayDate = GeneralFunctions.getStrFromDate(date.AddDays(b));
                                    result.Day_4.Orders.Add(oh);
                                }
                            }
                            c++;
                        }
                        else if (c == 3 && templist1.Count > 0)
                        {
                            result.Day_5 = new Day { Orders = new List<OrderHead>(), DayName = date.AddDays(b).DayOfWeek.ToString(), DayNumber = 5 };
                            foreach (var oh in templist1)
                            {
                                if (oh.OrderRows.Count > 0)
                                {
                                    bool Materialrowstock = false;

                                    if (oh.OrderRows.Any(z => z.MaterialRows != null))
                                    {
                                        if (oh.OrderRows.Any(x => x.MaterialRows !=null && x.MaterialRows.Any(y => (y.Amount.Value - y.DeliveredAmount.Value) > y.Product.Stock)))
                                        {
                                            Materialrowstock = false;
                                        }
                                        else
                                        {
                                            Materialrowstock= true;
                                        }

                                    }
                                    if (oh.OrderRows.Any(f => f.ProductNo != "9992" && ((f.Amount.Value - f.DeliveredAmount) > f.Product.Stock.Value) && f.MTF != "5" && f.MaterialRows == null))
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    if (oh.OrderRows.Any(x => x.MaterialRows != null) && !Materialrowstock)
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                    result.Day_5.DayDate = GeneralFunctions.getStrFromDate(date.AddDays(b));
                                    result.Day_5.Orders.Add(oh);
                                }
                            }
                            c++;
                        }
                 
                        templist1.Clear();


                    }
                    b++;
                }




            }

            return View(result);
        }
        private List<FilterDTO> getFilterList(string prefix)
        {
            List<FilterDTO> result = new List<FilterDTO>();

            try
            {
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);

                List<BaseTable> filterlist = mBDM.GetBaseTableList(prefix);

                if (filterlist != null)
                {
                    foreach (BaseTable bt in filterlist)
                    {
                        result.Add(new FilterDTO { Id = bt.Id, Description = bt.Description, Type = bt.Type, Filter = bt.Description2, UseCache = bt.Code1 == "T" ? true : false });
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }

        private string parseFilterConstants(string filter)
        {
            int count = 0;

            if (filter.Contains("#DAT"))
            {
                while (filter.Contains("#DAT"))
                {

                    int pos = filter.IndexOf("#DAT");
                    string whole = filter.Substring(pos, 6);
                    string date = DateTime.Now.AddDays(int.Parse(whole.Substring(4))).ToString("yyMMdd");
                    filter = filter.Replace(whole, date);
                    count++;

                    // If this high count something went wrong
                    if (count > 20)
                    {
                        break;
                    }
                }
            }

            return filter;
        }

        [Authorize]
        public IActionResult GetStartedOrderList()
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<OrderHead> lst = new List<OrderHead>();

            string filter = "LEF<=4;PLF==P;FAF!=5;OTY==1;BLT>=" + DateTime.Now.AddMonths(-5).ToString("yyMMdd");
            lst = mOM.GetOrderPickList("1", "*", mGeneralIndexSearchCount, filter, "A", true, true, 100, true);

            List<OrderHead> temp = new List<OrderHead>();

            foreach (OrderHead oh in lst)
            {
                if (oh.OrderRows.Count > 0)
                {
                    BaseTable ls = mBDM.GetBaseTable("03", oh.DeliverWay);
                    if (ls != null)
                        oh.DeliverWay = ls.Description;

                    temp.Add(oh);
                }
            }

            List<OrderHead> sortedList = temp.OrderByDescending(o => o.OrderNo).ToList();

            return View(sortedList);
        }

        [Authorize]
        public ActionResult GetDeliveredOrderWithoutTransport()
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<OrderHead> lst = new List<OrderHead>();

            string filter = "PLF==5;FAF!=5;OTY==1;LSE<=89;BLT>=" + DateTime.Now.AddDays(-7).ToString("yyMMdd");
            lst = mOM.GetOrderForDeliverNoteList("*", 1000, filter, 50, true);

            List<OrderHead> temp = new List<OrderHead>();

            List<OrderHead> sortedList = new List<OrderHead>();

            if (lst != null)
                sortedList = lst.OrderByDescending(o => o.OrderNo).ToList();


            return View(sortedList);
        }

        [Authorize]
        public IActionResult OverViewDetails(string id)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
            CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);
            ManufacturingModel mMM = new ManufacturingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<OrderHead> lst = mOM.GetOrderPickList("1", id, "0", "*", "A", true, false, 1, false);
            OrderHead oh = null;
            Dictionary<string, List<MaterialRow>> materials = new Dictionary<string, List<MaterialRow>>();

            try
            {

                if (lst.Count > 0)
                    oh = lst[0];
                else
                    oh = null;

                if (oh != null)
                {
                    BaseTable ls = mBDM.GetBaseTable("03", oh.DeliverWay);
                    if (ls != null)
                        oh.DeliverWay = ls.Description;

                    BaseTable lv = mBDM.GetBaseTable("02", oh.DeliverTerms);
                    if (lv != null)
                        oh.DeliverTermsDescription = lv.Description;

                    Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);

                    ViewBag.Customer = customer;

                }

                if (oh?.OrderRows == null)
                    oh.OrderRows = new List<OrderRow>();

                // Remove all rows without product
                oh.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

                //Remove all rows with products in "wrong" Warehouseno if "UseOnlyThis" is checked.
                BaseTable btWH = mBDM.GetBaseTable("WELOGG", "WH");


                // Get warehouse stock and set on product level
                if (btWH?.Id == "WH")
                {
                    string WarehouseNo = btWH?.Description.Trim();
                    bool UseOnlyThis = btWH.Code1.Equals("T") ? true : false;

                    if (UseOnlyThis)
                    {
                        oh.OrderRows.RemoveAll(o => (o.WarehouseNo != WarehouseNo));
                    }

                    foreach (OrderRow or in oh.OrderRows)
                    {
                        decimal? wh = or.Product.WarehouseList.Find(w => w.Id == btWH.Description.Trim())?.Stock;

                        if (wh.HasValue)
                        {
                            or.Product.Stock = wh.Value;
                        }
                        else
                        {
                            or.Product.Stock = 0;
                        }

                        if (or.Product.StockUpdateType.CompareTo("5") >= 0)
                        {
                            or.AmountToDeliver = or.Amount - or.DeliveredAmount;
                        }
                    }
                }

                foreach (OrderRow or in oh.OrderRows)
                {
                    if (or.MDF.Equals("1") || or.MDF.Equals("2"))
                    {
                        //List<MaterialRow> mr = mMM.getMaterialRowList(or.OrderNo, or.RowNo.ToString());
                        //materials.Add(or.OrderNo + or.RowNo.ToString(), mr);
                        or.MaterialRows = mMM.getMaterialRowList(or.OrderNo, or.RowNo.ToString());
                    }
                }

                //logger.Debug("Details materials :" + materials.Count);
                //ViewBag.Materials = materials;

                //var sortedRows = (from OrderRow r in oh.OrderRows
                //                  orderby r.Product.WarehouseNo ascending
                //                  select r).ToList();
                // oh.OrderRows = sortedRows;

                List<OrderRow> sortedList = oh.OrderRows.OrderBy(o => o.PreferedDeliverDate)
                                .ThenBy(o => o.Product.Code1)
                                .ThenBy(o => o.RowNo)
                                .ToList();

                oh.OrderRows = sortedList;
                oh.isPrioOrder = CheckPrioOrder(oh.OrderNo);

            }
            catch (Exception e)
            {
                logger.Debug("Error in Details" + e.Message);
            }

            return PartialView(oh);
        }

        private bool CheckPrioOrder(string orderid)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            bool isPrio = false;
            List<OrderRowText> result = mOM.GetOrderRowTextList(orderid, "0");
            if (result.Count > 0)
            {
                string txt = result.FirstOrDefault().Text;
                if (!string.IsNullOrWhiteSpace(txt))
                {
                    if (txt.Substring(0, 1).Equals("#"))
                        isPrio = true;
                }
            }

            return isPrio;


        }




    }

}

