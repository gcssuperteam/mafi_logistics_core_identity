using Babc_logistic_core.Database;
using Babc_logistic_core.Database.GIS.DTO;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Babc_logistic_core.ViewComponents;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using BABC_Logistics_core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Composition;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace BABC_Logistics_core.Views.Order
{
    public class TransportController : Controller
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;

        private readonly IMemoryCache _memoryCache;
        private MemoryCacheEntryOptions _cacheExpiryOptions = null;

        //private OrderModel mOM;
        //private BaseDataModel mBDM;
        //private LogtradeModel mLM;
        //private DeliverNoteModel mDNM;
        //private PrintingModel mPM;

        
        public TransportController(IConfiguration config, IMemoryCache cache, UserManager<ApplicationUser> userManager)
        {
            //mOM = new OrderModel(config, user);
            //mBDM = new BaseDataModel(config, user);
            //mLM = new LogtradeModel(config, user);
            //mDNM = new DeliverNoteModel(config, user);
            //mPM = new PrintingModel(config, user);

            _Config = config;
            _userManager = userManager;

            _memoryCache = cache;

            _cacheExpiryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(60),
                Priority = CacheItemPriority.High
            };

        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult GetTransport(string delivernote, string onr, string sign)
        
        {
            TransportVM result = new TransportVM();
     
            result.TimedUnloading = new TimedUnloading();
            ViewBag.sign = sign;
            try
            {
                
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                DeliverNoteModel mDN = new DeliverNoteModel(_Config, _userManager.GetUserAsync(User)?.Result);
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
                LogtradeModel mLM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);

         

                result.TimedUnloading.Date = DateTime.Now.AddDays(1);
                //result.TimedUnloading.TimeFrom = DateTime.Now;
                //result.TimedUnloading.Date = DateTime.Now;

                if (string.IsNullOrEmpty(delivernote))
                {
                    delivernote = mOM.GetOrderById(onr, false)?.LastDeliverNote;
                }

                // If still empty (no deliver exists)
                if (string.IsNullOrEmpty(delivernote))
                {
                    return RedirectToAction("PickOrder", "Order", new { id = onr, force = true });
                }

                result.Shipment = mLM.GetShipment(new GetShipmentParam { DeliverNote = delivernote, GarpUser = "GIS", GarpTerminal = "GS" });
                List<LogtradeGoodsItem> logtradeGoodsItems = new List<LogtradeGoodsItem>();
               

                result.Transport = mOM.GetTransportForDeliverNote(delivernote);
                      
                OrderHead oh = mOM.GetOrderById(onr, true);
                
                if(oh.OrderRows.Exists(x=>x.ProductNo=="9992"))
                {
                    OrderRow or = oh?.OrderRows.Find(x => x.ProductNo == "9992");
                        if(or.Textrows.Exists(x=>x.Text.StartsWith("Free-Text:")))
                    {
                        result.Shipment.ShipmentFreeText = or.Textrows.Find(x => x.Text.StartsWith("Free-Text:")).Text;
                       
                    }
                       
                }
                decimal netWeight = 0;

                var dNote = mDN.GetDeliverNote(delivernote, true);
                
                foreach (DeliverNoteRow d in dNote.Rows)
                {
                    netWeight += d.Product.Weight.Value*d.DeliveredAmount;
                }
                result.NetWeight = decimal.Round(netWeight,2);

                List<BaseTable> pkgList = mBDM.GetBaseTableList("1S");

                result.Transport.ModeOfTransportList = new List<ModeOfTransport>();
                result.Season = sign;
                foreach (BaseTable bt in pkgList)
                {
                    result.Transport.ModeOfTransportList.Add(new ModeOfTransport { Id = bt.Id, Description = bt.Description });
                }

                var deliverWayList = mBDM.GetBaseTableList("03");

                result.Shipment.GoodsItemList = mLM.GetGarpPackageList(delivernote);

                foreach (var dw in deliverWayList)
                {
                    if (dw.Description2.PadRight(35).Substring(30, 2) == "TL")
                    {
                        dw.Description += " - (TL)";
                    }
                    (result.DeliverWayList ??= new List<BaseTable>()).Add(dw);
                }
                int packageCount = 0;

                if (!ViewData.ContainsKey("packageitemlist"))
                {
                    List<PackageItem> tempList = new List<PackageItem>();

                    foreach (var item in result.Shipment.GoodsItemList)
                    {
                        PackageItem packageItem = new PackageItem();
                        packageItem.ID = item.GarpId;
                        packageItem.DeliverNoteNo = result.Shipment.DeliverNote;
                        packageItem.Volume = GeneralFunctions.getStrFromDecimal(item.Volyme);
                        packageItem.Width = "0";
                        packageItem.Quantity = item.Amount;
                        packageItem.Weight = GeneralFunctions.getStrFromDecimal(item.Weight);
                        packageItem.PackageType = item.PackageTypeCode;
                        if(item.LoadingMeters>0)
                        {
                            packageItem.GoodsMeasurementType = "F";
                            packageItem.Volume = GeneralFunctions.getStrFromDecimal(item.LoadingMeters);
                            
                        }
                        else
                        {
                        packageItem.Volume = GeneralFunctions.getStrFromDecimal(item.Volyme);
                        }
                        packageCount += item.Amount;
                        tempList.Add(packageItem);
                    }
                    ViewBag.PackageQty = packageCount;
                    ViewData["packageitemlist"] = JsonConvert.SerializeObject(tempList);

                }
               /* else
                {
                    var pkgItemList = getTempDataGoodsItemList(delivernote);

                    int idx = 0;
                    foreach (var item in result.Shipment.GoodsItemList)
                    {
                        if (idx < 3)
                        {
                            if (pkgItemList.Count >= idx)
                            {
                                (item.PackageItems ??= new List<LogtradePackageItem>()).Add(new LogtradePackageItem
                                {
                                    Height = GeneralFunctions.getDecimalFromStr(pkgItemList[idx].Height),
                                    Width = GeneralFunctions.getDecimalFromStr(pkgItemList[idx].Width),
                                    Length = GeneralFunctions.getDecimalFromStr(pkgItemList[idx].Length)
                                });
                            }
                        }
                    }
                }*/
            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public IActionResult GetTransport(TransportVM vm, string sign)
        {
            Transport result = null;
            try
            {
           
                result = UpdateTransport(vm.Transport);

                //updateDeliverNoteLabelTexts(result);
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("GetTransport", new { delivernote = result.DeliverNo });
        }

        [HttpPost]
        [Authorize] 
        public IActionResult BookingResult(TransportVM vm)
        {
            LogtradeShipmentResponse result = new LogtradeShipmentResponse();

            try
            {

                if(vm.AddPackageInformation)
                {
                    AddPackageInformationToFS(vm, false); 
                }
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                LogtradeModel mLM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);
                DeliverNoteModel mDNM = new DeliverNoteModel(_Config, _userManager.GetUserAsync(User)?.Result);

                var delnote = mDNM.GetDeliverNote(vm.Transport.DeliverNo, true);
                delnote.ZipCity = vm.Shipment.RecipientZip + " " + vm.Shipment.RecipientCity;
                delnote.DeliverWay = vm.Transport.DeliverWayId;
                


                mDNM.UpdateDeliverNoteHead(delnote);
                
                UpdateZipCity(vm.Transport.OrderNo,vm.Transport.DeliverNo, vm.Shipment.RecipientZip, vm.Shipment.RecipientCity);
                // Update Transport (DeliverNote) with values changed by user
                UpdateTransport(vm.Transport);

                var goodsitemlist = mLM.GetGarpPackageList(vm.Transport.DeliverNo);
                UpdateGarpTransport(vm.Transport.DeliverNo, goodsitemlist);

                LogtradeShipment shipment = mLM.GetShipment(new GetShipmentParam { DeliverNote = vm.Transport.DeliverNo, GarpUser = "GIS", GarpTerminal = "GS" });

                if(goodsitemlist!=null)
                {
                    shipment.GoodsItemList = goodsitemlist;
                }


                if(shipment.GoodsItemList.Any(x=>x.PackageTypeCode=="SP"))
                {
                    shipment.ShipmentAction = "Save";
                    shipment.GoodsItemList.RemoveAll(x => x.PackageTypeCode == "SP");
                }

                result.OrderNo = shipment.OrderNo;

                shipment.SenderReference = vm.Shipment.SenderReference;
                shipment.ShipmentFreeText = vm.Shipment.ShipmentFreeText;
                shipment.ShipmentInstruction = vm.Shipment.ShipmentInstruction + " " + vm.Shipment.ShipmentFreeText;
               // shipment.ProductCode = vm.Transport.DeliverWayId;
                //shipment.TermsOfDeliveryCode = vm.Transport.DeliverTermId;

                /*
                    MAFI special - Checka om garantibokning finns och tryck i så fall in en ShipmentTemplate
                */
                try
                {
                    if (shipment.ShipmentServiceList.Find(s => s.Description.Contains("Garanti")) != null)
                    {
                        shipment.SenderAddressId = "003G";
                    }
                }
                catch (Exception e)
                {

                }

                /*
                    MAFI special - Tidslossning
                */
                try
                {
                    var timeUnloading = shipment.ShipmentServiceList.Find(s => s.Description.Contains("Tidslossning"));
                    if (timeUnloading != null)
                    {
                        timeUnloading.PropertyDescriptions[0].Value = vm.TimedUnloading.Date.Add(vm.TimedUnloading.TimeFrom.TimeOfDay).ToString("yyyy-MM-ddTHH:mm:ss");
                        timeUnloading.PropertyDescriptions[1].Value = vm.TimedUnloading.Date.Add(vm.TimedUnloading.TimeTo.TimeOfDay).ToString("yyyy-MM-ddTHH:mm:ss");
                    }
                    //else // If service i smissing
                    //{
                    //    LogtradeShipmentService service = new LogtradeShipmentService();

                    //    // Add from time
                    //    LogtradeProperty from = new LogtradeProperty();
                    //    from.Value = vm.TimedUnloading.Date.Add(vm.TimedUnloading.TimeFrom.TimeOfDay).ToString("yyyy-MM-ddTHH:mm:ss");
                    //    from.Name = "Tidigast";
                    //    from.Type = "DateTime";
                    //    (service.PropertyDescriptions ??= new List<LogtradeProperty>()).Add(from);

                    //    // Add to time
                    //    LogtradeProperty to = new LogtradeProperty();
                    //    to.Value = vm.TimedUnloading.Date.Add(vm.TimedUnloading.TimeFrom.TimeOfDay).ToString("yyyy-MM-ddTHH:mm:ss");
                    //    to.Name = "Senast";
                    //    to.Type = "DateTime";
                    //    (service.PropertyDescriptions ??= new List<LogtradeProperty>()).Add(to);


                    //    (shipment.ShipmentServiceList ??= new List<LogtradeShipmentService>()).Add(service);
                    //}
                }
                catch (Exception e)
                {

                }

                // CHange values that user can change in the view
                var list = mLM.GetPackageTypeMappingList(shipment.ProductCode);
                if (list != null && list.Count() > 0)
                {
                    foreach (var package in shipment.GoodsItemList)
                    {
                        package.PackageTypeCode = list.Find(x => x.Garp == package.PackageTypeCode).Logtrade;

                    }
                }
                if (!vm.SaveShipment)
                {
                    result = mLM.CreateShipment(shipment);

                    try
                    {
                        updateTransportCostToOrder(shipment.OrderNo, result.Price, vm.Shipment?.SenderReference);
                    }
                    catch (Exception e)
                    {
                        logger.Error("Error in BookingResult while adding transport price", e);
                    }

                } else
                {
                    result.Status = "Shipment has been saved to Garp";
                    result.Price = 0;                       
                }

                try
                {
                    OrderHead oh = mOM.GetOrderById(shipment.OrderNo, true);
                    oh.DeliverWay = vm.Transport.DeliverWayId;
                   // oh.DeliverWayDescription = vm.Transport.DeliverWayDescription;
                    mOM.UpdateOrderHead(oh);

                    OrderRow or = oh.OrderRows.Find(o => (o.ProductNo == "9992" || o.ProductNo =="9996") && o.DeliverState!="5");



              
                    ViewBag.DeliveryNoteState = delnote.DeliveryNoteState;
                  
                    if (or != null)
                    {

                        var exceptions = _Config.GetSection("SpecialFreight").Get<SpecialFreight>();
                        FreightExceptions exception = new FreightExceptions();

                        if (exceptions.Customers.Any(x => x.Customer == oh.InvoiceCustomerNo))
                        {
                            exception = exceptions.Customers.Find(x => x.Customer == oh.InvoiceCustomerNo);
                        }

                        if (or.Price == 0)
                        {
                            if (!string.IsNullOrEmpty(exception.Customer))
                            {
                                result.CalculatedFreight = FreightException(exception, delnote, or.CostPrice.Value);
                                updateTransportCostToOrder(shipment.OrderNo, result.CalculatedFreight.OldPrice, vm.Shipment?.SenderReference);
                                result.CalculatedFreight.IsFreightException = true;
                            }
                            else
                            {
                                result.CalculatedFreight = CalculateFreightPrice(or.OrderNo, or.CostPrice.Value, _userManager.GetUserAsync(User)?.Result);
                                result.CalculatedFreight.IsFreightException = false;
                            }

                            ViewBag.CustomerPrice = result.CalculatedFreight.NewPrice;
                        }
                        else
                        {
                            result.CalculatedFreight = new CalculatedFreight();
                            result.CalculatedFreight.IsFreightChanged = false;
                            result.CalculatedFreight.FreightThreshold = 0;
                            result.CalculatedFreight.OldPrice = or.Price.Value;
                            result.CalculatedFreight.NewPrice = or.Price.Value;
                            result.CalculatedFreight.OrderValue = 0;
                            result.CalculatedFreight.isAboveFreightThreshold = false;
                            result.CalculatedFreight.FixedFreightSurcharge = 0;
                            result.CalculatedFreight.FixedTransportSurcharge = 0;
                            result.CalculatedFreight.IsFreightFreeDay = false;
                            result.CalculatedFreight.SurchargeRate = 0;

                            ViewBag.CustomerPrice = or.Price;
                        }

                     
                    }
                    else
                    {
                        result.CalculatedFreight = new CalculatedFreight();
                        result.CalculatedFreight.IsFreightException = false;
                        result.CalculatedFreight.IsFreightChanged = false;
                        result.CalculatedFreight.FreightThreshold = 0;
                        result.CalculatedFreight.OldPrice = 0;
                        result.CalculatedFreight.NewPrice = 0;
                        result.CalculatedFreight.OrderValue = 0;
                        result.CalculatedFreight.isAboveFreightThreshold = false;
                        result.CalculatedFreight.FixedFreightSurcharge = 0;
                        result.CalculatedFreight.FixedTransportSurcharge = 0;
                        result.CalculatedFreight.IsFreightFreeDay = false;
                        result.CalculatedFreight.SurchargeRate = 0;

                        ViewBag.CustomerPrice = 0;
                    }

                    //Save Free-text shipment text as OrderRowText on freight item 9992
                    if (!string.IsNullOrEmpty(vm.Shipment.ShipmentFreeText))
                    {
                        List<OrderRow> templst = new List<OrderRow>();
                        string freetext = "Free-Text: " + vm.Shipment.ShipmentFreeText;
                        List<string> freeTextList = new List<string>();
                        if (freetext.Length > 60)
                        {
                            freeTextList = Regex.Split(freetext, @"(.{1,60})(?:\s|$)|(.{18})")
                  .Where(x => x.Length > 0)
                  .ToList();


                        }
                        if (freeTextList.Count() > 1)
                        {
                            if (or.Textrows.Exists(x => x.Text.StartsWith("Free-Text:")))
                            {
                                or.Textrows.Find(x => x.Text.StartsWith("Free-Text:")).Text = freeTextList[0];
                                int i = 1;
                                while (freeTextList.Count() > i)
                                {
                                    OrderRowText ort = new OrderRowText();
                                    ort.Text = freeTextList[i];
                                    ort.DeliverNoteState = "0"; //TBD if it should be 1 or 0
                                    ort.TextRowNo = "255";
                                    or.Textrows.Add(ort);
                                    i++;
                                }

                            }
                            else
                            {
                                int i = 0;
                                while (freeTextList.Count() > i)
                                {
                                    OrderRowText ort = new OrderRowText();
                                    ort.Text = freeTextList[i];
                                    ort.DeliverNoteState = "0"; //TBD if it should be 1 or 0
                                    ort.TextRowNo = "255";
                                    or.Textrows.Add(ort);
                                    i++;
                                }
                            }

                        }
                        else
                        {
                            if (or.Textrows.Exists(x => x.Text.StartsWith("Free-Text:")))
                            {
                                or.Textrows.Find(x => x.Text.StartsWith("Free-Text:")).Text = freetext.PadRight(256).Substring(0, 255);
                            }
                            else
                            {


                                OrderRowText ort = new OrderRowText();
                                ort.Text = freetext.PadRight(256).Substring(0, 255);
                                ort.DeliverNoteState = "0"; //TBD if it should be 1 or 0
                                or.Textrows.Add(ort);
                            }
                        }
                        or.CostPrice = result.CalculatedFreight.OldPrice;
                        templst.Add(or);
                        mOM.UpdateOrderRowList(templst);

                    }
                }
                catch (Exception)
                {
                    result.CalculatedFreight = new CalculatedFreight();
                    result.CalculatedFreight.IsFreightChanged = false;
                    result.CalculatedFreight.FreightExceptions = new FreightExceptions();
                }

            }
            catch (Exception e)
            {
                logger.Error("Error in BookingResult", e);
            }

            return View(result);
        }


        public CalculatedFreight FreightException(FreightExceptions exception, DeliverNoteOH delnote, decimal price)
        {
            CalculatedFreight result = new CalculatedFreight();
            result.FreightExceptions = exception;
            try
            {
             
                result.NewPrice = 0;

                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);
                Babc_logistic_core.GIS.Model.ProductModel mPM = new Babc_logistic_core.GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);
                var firstuser = _userManager.GetUserAsync(User)?.Result;
                var Order = mOM.GetOrderById(delnote.OrderNo, true);
                Customer C = mCM.GetCustomerById(Order.InvoiceCustomerNo);
                price += C.Numeric_2.Value;
                //Order in Swedish corporation is updated with freight cost price, and calculated product price in orders currency
                decimal currencyValue = GeneralFunctions.getDecimalFromStr(Order.CurrencyValue);
                result.OldPrice = price;
                result.NewPrice = decimal.Round(price/currencyValue, 2);
                result.IsFreightChanged = true;
                result.FreightThreshold = 0;
                result.OrderValue = 0;
                result.isAboveFreightThreshold = false;
                result.FixedFreightSurcharge = 0;
                result.FixedTransportSurcharge = 0;
                result.IsFreightFreeDay = false;
                result.SurchargeRate = 0;

             

                //Setting PurchaseModel and OrderModel to Corporation 2
                ApplicationUser userSecondCompany = new ApplicationUser();
                userSecondCompany.UserName = firstuser.UserName;
                userSecondCompany.Email = firstuser.Email;
                userSecondCompany.GarpUser = firstuser.GarpUser;
                userSecondCompany.GarpPassword = firstuser.GarpPassword;
                userSecondCompany.GisToken = firstuser.GisToken;


                //userSecondCompany = _userManager.GetUserAsync(User)?.Result;
                userSecondCompany.GisToken = exception.GisToken;
                PurchaseModel mPMSecondCompany = new PurchaseModel(_Config, userSecondCompany);
                OrderModel mOMSecondCompany = new OrderModel(_Config, userSecondCompany);

                //Get Purchase order number in Corporation 2
                PurchaseOrderHead purchaseOrder = new PurchaseOrderHead();  
                if(!string.IsNullOrEmpty(Order.Text3))
                {
                    string orderNumber = Order.Text3.PadRight(17).Split(exception.OrderMark)[1].Trim();
                    purchaseOrder = mPMSecondCompany.GetOrderById(orderNumber, true); 

                }

                //Get Order Number in Corporation 2 from Purchase order lines, and deliver rows in Corporation 2
                if(purchaseOrder != null)
                {
                    List<DeliverRowParam> listRowParam = new List<DeliverRowParam>();
                    List<PurchaseDeliverRowParam> PurchaseRowParam = new List<PurchaseDeliverRowParam>();
                    OrderHead orderHead = new OrderHead();
                    try
                    {
                        List<Product> ListProducts = new List<Product>();
                        if (purchaseOrder.OrderRows != null)
                        {
                            string OrderNumber = "";
                            foreach (var row in purchaseOrder.OrderRows)
                            {
                                if (delnote.Rows.Any(x => x.ProductNo == row.ProductNo))
                                {
                                    if (row.Textrows.Count > 0)
                                    {
                                        var tempTextLine = row?.Textrows[0]?.Text?.PadRight(50)?.Split(exception.PurchaseOrderLineMark);
                                        OrderNumber = tempTextLine[1].Trim().Split("-")[0].Trim();
                                        string RowNumber = tempTextLine[1].Trim().PadRight(22).Split("-")[1].Trim();

                                        decimal RowQty = delnote.Rows.Find(x => x.ProductNo == row.ProductNo).DeliveredAmount;
                                        decimal pRowQty = RowQty;
                                        //if (row.AmountToDeliver.Value < RowQty)
                                        //{
                                        //    pRowQty = row.AmountToDeliver.Value;
                                        //}
                                        string warehouse = "";
                                        warehouse = row.WarehouseNo;
                                        PurchaseRowParam.Add(GetDeliverPurchaseRowParam(row.OrderNo, row.RowNo.ToString(), pRowQty, warehouse));
                                        listRowParam.Add(GetDeliverRowParam(OrderNumber, RowNumber, RowQty, warehouse));

                                    }
                                }
                            }
                            List<OrderRow> freightItems = new List<OrderRow>();
                          
                            //Get Customer Order in Corporation 2, check if there is a freight row, if not add row, and calculate freight cost. 
                            var delOrder = mOM.GetOrderForDeliverNote(Order.LastDeliverNote);
  
                            OrderRow or = null; 
                            if (!string.IsNullOrEmpty(OrderNumber))
                            {
                                orderHead = mOMSecondCompany.GetOrderById(OrderNumber, true);
                                if (delOrder != null)
                                {
                                    List<FilterDTO> filterList = getFilterList("FILTERE");
                                    if (filterList?.Count > 0)
                                    {
                                        ListProducts = mPM.GetProductList("1", "*", filterList[0].Filter);
                                        foreach (var row in delOrder.OrderRows)
                                        {
                                            if (ListProducts.Any(x => x.ProductNo == row.ProductNo))
                                            {
                                                if (!orderHead.OrderRows.Any(x => x.ProductNo == row.ProductNo && x.DeliverState != "5"))
                                                {
                                                    var addRow = new OrderRow
                                                    {
                                                        OrderNo = orderHead.OrderNo,
                                                        RowNo = 255,
                                                        ProductNo = row.ProductNo,
                                                        Amount = row.Amount==null?row.DeliveredAmount:row.Amount,
                                                        FixedCostPrice = "F",
                                                        WarehouseNo = delOrder.OrderRows.FindLast(o => o.WarehouseNo != "").WarehouseNo,
                                                        PreferedDeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now)
                                                    };
                                                    freightItems.Add(addRow);

                                                }
                                            }

                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                                or = orderHead.OrderRows.Find(o => (o.ProductNo == "9992" || o.ProductNo == "9996") && o.DeliverState != "5");


                                if (or != null)
                                {
                                    if (result.NewPrice > 0)
                                    {
                                        or.CostPrice = result.NewPrice;
                                        or.FixedCostPrice = "F";
                                    }
                                    freightItems.Add(or);
                                }
                                else if (orderHead.OrderRows.FindAll(x => (x.RowNo >= 251 && x.RowNo <= 254)).Count() < 4)
                                {
                                    or = new OrderRow
                                    {
                                        OrderNo = orderHead.OrderNo,
                                        RowNo = 255,
                                        ProductNo = "9992",
                                        Amount = 1,
                                        CostPrice = result.NewPrice,
                                        FixedCostPrice = "F",
                                        PreferedDeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now)
                                    };
                                    freightItems.Add(or);
                                }
                                else
                                {
                                    or = new OrderRow
                                    {
                                        OrderNo = orderHead.OrderNo,
                                        RowNo = 255,
                                        ProductNo = "9996",
                                        Amount = 1,  
                                        CostPrice = result.NewPrice,
                                        FixedCostPrice = "F",
                                        PreferedDeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now)
                                    };
                                    freightItems.Add(or);

                                }
                            }
                            if (PurchaseRowParam.Count > 0)
                            {
                                mPMSecondCompany.Deliver(PurchaseRowParam);
                            }
                            if (or != null)
                            {
                                mOM.UpdateOrderRowList(freightItems, userSecondCompany.GisToken);
                                orderHead = mOMSecondCompany.GetOrderById(orderHead.OrderNo, true);
                                OrderRow orUpdated = orderHead.OrderRows.Find(o => (o.ProductNo == "9992" || o.ProductNo == "9996") && o.DeliverState != "5");
                                result.FreightExceptionOrder = orUpdated.OrderNo;
                                result.FreightExceptionRowNo = orUpdated.RowNo;
                                result.IsFreightException = true;
                                decimal calculatedPrice = 0;
                                calculatedPrice = decimal.Round(CalculateFreightPrice(orderHead.OrderNo, result.NewPrice, userSecondCompany, Order.InvoiceCustomerNo).NewPrice, 2);
                                orUpdated.Price = calculatedPrice;
                                result.FreightExceptionPrice = calculatedPrice;
                                mOM.UpdateOrderRowList(new List<OrderRow> { orUpdated }, userSecondCompany.GisToken);
                                
                                //listRowParam.Add(GetDeliverRowParam(orUpdated.OrderNo, orUpdated.RowNo.ToString(), orUpdated.Amount.Value, orUpdated.WarehouseNo));
                            }
                        }

                       foreach(var row in orderHead.OrderRows)
                        {
                            if(ListProducts.Any(x=>x.ProductNo==row.ProductNo) && row.DeliverState!="5")
                            {
                                if(row.ProductNo!="9996" ||row.ProductNo!="9992")
                                    listRowParam.Add(GetDeliverRowParam(row.OrderNo, row.RowNo.ToString(), row.Amount.Value, row.WarehouseNo));
                            }
                        }



                        if (listRowParam.Count > 0)
                        {
                            var deliverResult = mOMSecondCompany.Deliver(listRowParam, userSecondCompany.GisToken);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("Error in FreightException, Trying to deliver row in Corporation 2", e);

                    }
                  
                }


            }
            catch (Exception e)
            {

                logger.Error("Error in FreightException", e);
            }

            return result; 

        }
        private List<FilterDTO> getFilterList(string prefix)
        {
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<FilterDTO> result = new List<FilterDTO>();

            try
            {
                List<BaseTable> filterlist = mBDM.GetBaseTableList(prefix);

                if (filterlist != null)
                {
                    foreach (BaseTable bt in filterlist)
                    {
                        result.Add(new FilterDTO { Id = bt.Id, Description = bt.Description, Type = bt.Type, Filter = bt.Description2, UseCache = bt.Code1 == "T" ? true : false });
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }
        public DeliverRowParam GetDeliverRowParam(string onr, string row, decimal qty, string warehouse)
        {
            DeliverRowParam result = new DeliverRowParam();
            try
            {            
                DeliverRowParam rowParam = new DeliverRowParam();

                rowParam.RowNo=GeneralFunctions.getIntFromStr(row);
                rowParam.OrderNo = onr;
                rowParam.AmountToDeliver = qty;
                rowParam.DeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now);
                if (!string.IsNullOrEmpty(warehouse))
                    rowParam.WarehouseNo = warehouse; 
                result = rowParam;

             
            }
            catch (Exception e)
            {
                logger.Error("Error in GetDeliverRowParam", e);

            }

            return result;

        }
        public PurchaseDeliverRowParam GetDeliverPurchaseRowParam(string onr, string row, decimal qty, string warehouse)
        {
            PurchaseDeliverRowParam result = new PurchaseDeliverRowParam();
            try
            {
                PurchaseDeliverRowParam rowParam = new PurchaseDeliverRowParam();

                rowParam.RowNo = GeneralFunctions.getIntFromStr(row);
                rowParam.OrderNo = onr;
                rowParam.Amount = qty;
                if (!string.IsNullOrEmpty(warehouse))
                    rowParam.WarehouseNo = warehouse;
                rowParam.DeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now);
                result = rowParam;


            }
            catch (Exception e)
            {
                logger.Error("Error in GetDeliverRowParam", e);

            }

            return result;

        }

        [HttpPost]
        [Authorize]
        public string PriceQuestion(TransportVM vm)
        {
            LogtradePriceQuestionResponse result = new LogtradePriceQuestionResponse();

            try
            {
                LogtradeModel mLM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);

                UpdateTransport(vm.Transport);

                LogtradeShipment shipment = mLM.GetShipment(new GetShipmentParam { DeliverNote = vm.Transport.DeliverNo, GarpUser = "GIS", GarpTerminal = "GS" });
                shipment.GoodsItemList = mLM.GetGarpPackageList(vm.Transport.DeliverNo);

                // CHange values that user can change in the view
                if (vm.Transport.DeliverTermId != null)
                {
                    shipment.TermsOfDeliveryCode = vm.Transport.DeliverTermId;

                }
                var list = mLM.GetPackageTypeMappingList(shipment.ProductCode);

                foreach (var package in shipment.GoodsItemList)
                {
                    package.PackageTypeCode = list.Find(x => x.Garp == package.PackageTypeCode).Logtrade;

                }
          

                result = mLM.GetShipmentPrice(shipment);
           
                if (result == null)
                {
                    result = new LogtradePriceQuestionResponse();
                    result.Price = 0;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in PriceQuestion", e);
            }

            return result.Price.ToString();
        }

        private Transport UpdateGarpTransport(string delivernote, List<LogtradeGoodsItem> pkg)
        {
            Transport result = null;

            try
            {
                if (!string.IsNullOrEmpty(delivernote) && pkg != null)
                {
                    var user = this.HttpContext.User;
                    OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(user)?.Result);
                    
                    result = mOM.GetTransportForDeliverNote(delivernote);

                    if (pkg.Count >= 1)
                    {
                        result.PackageType1 = pkg[0].PackageTypeCode;
                        result.PackageAmount1 = pkg[0].Amount;
                        result.Weight1 = pkg[0].Weight;
                        result.Volume1 = pkg[0].Volyme + pkg[0].LoadingMeters;
                        result.Length1 = pkg[0].PackageItems[0].Length;
                        result.Width1 = pkg[0].PackageItems[0].Width;
                        result.Height1 = pkg[0].PackageItems[0].Height;
                        if(pkg[0].LoadingMeters>0)
                        {
                            result.GoodsMeasurementType = "F";
                        }
                        else
                        {
                            result.GoodsMeasurementType = "V";
                        }
                     
                    }
                    if (pkg.Count >= 2)
                    {
                        result.PackageType2 = pkg[1].PackageTypeCode;
                        result.PackageAmount2 = pkg[1].Amount;
                        result.Weight2 = pkg[1].Weight;
                        result.Volume2 = pkg[1].Volyme + pkg[1].LoadingMeters;
                        result.Length2 = pkg[1].PackageItems[0].Length;
                        result.Width2 = pkg[1].PackageItems[0].Width;
                        result.Height2 = pkg[1].PackageItems[0].Height;
                        if (pkg[1].LoadingMeters > 0)
                        {
                            result.GoodsMeasurementType = "F";
                        }
                        else
                        {
                            result.GoodsMeasurementType = "V";
                        }

                    }
                    if (pkg.Count >= 3)
                    {
                        result.PackageType3 = pkg[2].PackageTypeCode;
                        result.PackageAmount3 = pkg[2].Amount;
                        result.Weight3 = pkg[2].Weight;
                        result.Volume3 = pkg[2].Volyme + pkg[2].LoadingMeters;
                        result.Length3 = pkg[2].PackageItems[0].Length;
                        result.Width3 = pkg[2].PackageItems[0].Width;
                        result.Height3 = pkg[2].PackageItems[0].Height;
                        if (pkg[2].LoadingMeters > 0)
                        {
                            result.GoodsMeasurementType = "F";
                        }
                        else
                        {
                            result.GoodsMeasurementType = "V";
                        }

                    }

                    mOM.UpdateTransport(result);
                }





            }
            catch (Exception e)
            {
                logger.Error("Error in UpdateGarpTransport", e);
            }

            return result;
        }

        [HttpPost]
        [Authorize]
        public IActionResult UpdateFreightTextRows(string onr, string rowNo, string textRowNo, string text)
        {
            OrderModel oOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            string result = string.Empty;
            try
            {
                List<OrderRow> templist = new List<OrderRow>();
                OrderRow or = oOM.GetOrderRow(onr, rowNo);

                if (or.Textrows.Exists(x=>x.TextRowNo==textRowNo))
                {
                    or.Textrows.Find(x => x.TextRowNo == textRowNo).Text = "Free-Text: " + text;
                   
                    templist.Add(or);
                    oOM.UpdateOrderRowList(templist);
                    result = text; 
                }
                else if(textRowNo=="N")
                {
                    OrderRowText orderRowText = new OrderRowText();
                    orderRowText.Text = "Free-Text: " + text;
                    orderRowText.DeliverNoteState = "0";
                    or.Textrows.Add(orderRowText);

                    templist.Add(or);
                    oOM.UpdateOrderRowList(templist);
                    result = text;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in UpdateFreightTextRows", e);
                return Content(e.Message);

            }

            return Content("OK");
        }


        [Authorize]
        public IActionResult UpdateOrderWithTransportCost(string onr, string price, string delivernote)
        {
            try
            {
                updateTransportCostToOrder(onr, GeneralFunctions.getDecimalFromStr(price), onr);
            }
            catch (Exception)
            {

            }

            return RedirectToAction("GetTransport", new { delivernote = delivernote });
        }

        private void updateTransportCostToOrder(string onr, decimal price, string reference)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                OrderHead oh = mOM.GetOrderById(onr, true);
                OrderRow or = oh.OrderRows.Find(o => (o.ProductNo == "9992" || o.ProductNo =="9996") && o.DeliverState!="5");

 

                if (or != null)
                {
                    if (price > 0)
                    {
                        or.CostPrice = price;
                        or.FixedCostPrice = "F";
                    }
                }
                else if(oh.OrderRows.FindAll(x=>(x.RowNo >= 251 && x.RowNo <= 254)).Count()<4)
                {
                    or = new OrderRow
                    {
                        OrderNo = onr,
                        RowNo = 255,
                        ProductNo = "9992",
                        Amount = 1,
                        WarehouseNo = "1",
                        CostPrice = price,
                        FixedCostPrice = "F",
                        PreferedDeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now)
                };
                }
                else
                {
                    or = new OrderRow
                    {
                        OrderNo = onr,
                        RowNo = 255,
                        ProductNo = "9996",
                        Amount = 1,
                        WarehouseNo = "1",
                        CostPrice = price,
                        FixedCostPrice = "F",
                        PreferedDeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now)
                    };

                }

                // Create textrow for reference
                var ot = new OrderRowText
                {
                    TextRowNo = "255",
                    DeliverNoteState = "1",
                    InvoiceState = "0",
                    OrderConfirmationState = "0",
                    PickListState = "0",
                    RowNo = or.RowNo.ToString(),
                    Text = "Sender references: " + reference
                };

                if (onr != reference)
                {
                    if (or.Textrows == null)
                    {
                        or.Textrows = new List<OrderRowText>();
                    }

                    or.Textrows.Add(ot);
                }

                mOM.UpdateOrderRowList(new List<OrderRow> { or });
            }
            catch (Exception e)
            {
                logger.Error("Error in UpdateTransportCostToOrder", e);
            }
        }

        [HttpPost]
        [Authorize]
        public string UpdateZipCity(string onr, string dn, string zip, string city)
        {
            
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            DeliverNoteModel mDNM = new DeliverNoteModel(_Config, _userManager.GetUserAsync(User)?.Result);

            try
            {
                OrderHead oh = mOM.GetOrderById(onr, false);
                DeliverNoteOH doh = mDNM.GetDeliverNote(dn, false);
            
                oh.DeliverZip = zip;
                oh.DeliverCity = city;
                oh.DeliverZipCity = zip + " " + city;                        
                doh.ZipCity = zip + " " + city;
                mOM.UpdateOrderHead(oh);
                mDNM.UpdateDeliverNoteHead(doh);
            }
            catch (Exception e)
            {
                logger.Error("Error in UpdateZipCity", e);

            }


            return "Ok";
        }

        private Transport UpdateTransport(Transport transport)
        {
            Transport result = null;

            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                if (transport != null)
                {
                    result = mOM.GetTransportForDeliverNote(transport.DeliverNo);

                    // Updateing the fields that user can change in view;
                    result.DeliverWayId = transport.DeliverWayId;

                    result.PackageType1 = transport.PackageType1;
                    result.PackageType2 = transport.PackageType2;
                    result.PackageType3 = transport.PackageType3;

                    result.PackageAmount1 = transport.PackageAmount1;
                    result.PackageAmount2 = transport.PackageAmount2;
                    result.PackageAmount3 = transport.PackageAmount3;

                    result.Weight1 = transport.Weight1;
                    result.Weight2 = transport.Weight2;
                    result.Weight3 = transport.Weight3;

                    result.Volume1 = transport.Volume1;
                    result.Volume2 = transport.Volume2;
                    result.Volume3 = transport.Volume3;

                    result.GoodsMeasurementType = transport.GoodsMeasurementType;

                    mOM.UpdateTransport(result);
                }


            }
            catch (Exception e)
            {
                logger.Error("Error in UpdateTransport", e);
            }

            return result;
        }

        [HttpPost]
        [Authorize]
        public IActionResult PrintGoodsLabel(string orderno, string qty)
        {
            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);
      
            ERPReport report = new ERPReport();

            try
            {
                var user = _userManager.GetUserAsync(User)?.Result;
                report = _Config.GetSection(user.GisToken + "PrintGoodsLabel").Get<ERPReport>();
                if (report == null)
                    report = _Config.GetSection(user.GisToken+"PrintGoodsLabel").Get<ERPReport>();


              
            }
            catch
            {

            }

            try
            {
                report.IndexFrom = orderno;
                report.IndexTo = orderno;
               

                report.Dialogs[2].Value = qty;

                mPM.Print(report);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            return RedirectToAction("GetTransport", new { onr = orderno });
        }
        [HttpPost]
        [Authorize]
        public IActionResult PrintGoodsMark(string delnote, string qty)
        {
            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);
          
            ERPReport report = new ERPReport();
            bool result = false;
            try
            {
                var user = _userManager.GetUserAsync(User)?.Result;
                report = _Config.GetSection(user.GisToken + "PrintGoodsMark").Get<ERPReport>();
                if(report==null)
                 report = _Config.GetSection("PrintGoodsMark").Get<ERPReport>();

                //report.DocGen = ConfigurationManager.AppSetting["PrintGoodsMark:DocGen"];
                //report.Report = ConfigurationManager.AppSetting["PrintGoodsMark:Report"];
                //report.Medium = ConfigurationManager.AppSetting["PrintGoodsMark:Medium"];
                //report.Form = ConfigurationManager.AppSetting["PrintGoodsMark:Form"];
                //report.FilePath = ConfigurationManager.AppSetting["PrintGoodsMark:FilePath"];
                //report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["PrintGoodsMark:Wait"]);


            }
            catch
            {

            }

            try
            {
                report.IndexFrom = delnote;
                report.IndexTo = delnote;


                report.Dialogs[0].Value = qty;

                result = mPM.Print(report);
                result = true;
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e);
                return Content("Error: " + e);

            }
            if(result)
            {
                    return Content("OK");
            }
            else {
                return Content("Error while trying to print label.");
            }
      
        }

        [HttpPost]
        [Authorize]
        public IActionResult UpdatePrice(string onr, string price, string costPrice = "", string exOnr = "", string exRow = "", string token = "", string exNewPrice = "")
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                DeliverNoteModel mDM = new DeliverNoteModel(_Config, _userManager.GetUserAsync(User)?.Result);
                OrderModel mOMSecondCompany = null;
                ApplicationUser userSecondCompany = new ApplicationUser();
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(exOnr))
                {
                    userSecondCompany = _userManager.GetUserAsync(User)?.Result;
                    userSecondCompany.GisToken = token;
                    mOMSecondCompany = new OrderModel(_Config, userSecondCompany);
                    OrderRow orderRowEx = mOMSecondCompany.GetOrderRow(exOnr, exRow.ToString());
                    orderRowEx.CostPrice = GeneralFunctions.getDecimalFromStr(price);
                    orderRowEx.Price = GeneralFunctions.getDecimalFromStr(exNewPrice);
                    mOMSecondCompany.UpdateOrderRowList(new List<OrderRow> { orderRowEx }, token);

                    var param = GetDeliverRowParam(orderRowEx.OrderNo, orderRowEx.RowNo.ToString(), orderRowEx.Amount.Value, "");
                    mOMSecondCompany.Deliver(new List<DeliverRowParam> { param });
                }
                var dnOrder = mDM.GetDeliverNoteByOrderId(onr, true);
                var order = mOM.GetOrderById(onr, true);

                var dnRow = dnOrder.Rows.Find(r => (r.ProductNo == "9992" || r.ProductNo == "9996"));


                var row = order.OrderRows.Find(r => (r.RowNo==dnRow?.RowNo));
                if(row==null)
                {
                    
                    row = order?.OrderRows.Find(r => (r.ProductNo == "9992" || r.ProductNo == "9996"));

                }


                if (row != null)
                {
                    //row.Price = CalculateFreightPrice(onr, GeneralFunctions.getDecimalFromStr(price));
                    row.Price = GeneralFunctions.getDecimalFromStr(price);
                    if(!string.IsNullOrEmpty(costPrice))
                    {
                        row.FixedCostPrice = "F";
                        row.CostPrice = GeneralFunctions.getDecimalFromStr(costPrice);
                    }
                   

                   

                    if (row.DeliverState != "5")
                    {
                        mOM.UpdateOrderRowList(new List<OrderRow> { row });
                        DeliverRowParam deliver = new DeliverRowParam();
                        deliver.OrderNo = row.OrderNo;
                        deliver.RowNo = row.RowNo;
                        deliver.AmountToDeliver = row.Amount.Value;
                        deliver.DeliverState = "5";
                        
                     

                        mOM.Deliver(new List<DeliverRowParam> { deliver });
                    }
                    else
                    {
                     
                        var backrow =  mOM.BackDeliver(new List<BackDeliverRowParam>() { new BackDeliverRowParam() { OrderNo = row.OrderNo, RowNo = row.RowNo, DeliverNote = dnRow.DeliverNoteNo } });
                        mOM.UpdateOrderRowList(new List<OrderRow> { row });
                        DeliverRowParam deliver = new DeliverRowParam();
                        deliver.OrderNo = row.OrderNo;
                        deliver.RowNo = row.RowNo;
                        deliver.AmountToDeliver = row.Amount.Value;
                        deliver.DeliverNote = dnRow.DeliverNoteNo;
                        deliver.DeliverState = "5";


                        mOM.Deliver(new List<DeliverRowParam> { deliver });


                    }
                }
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }

            return Content("OK");
        }


        private decimal getLbsFromKg(decimal kg)
        {
            decimal Lbs = 0;

            double kgD = Decimal.ToDouble(kg);
            double conversion = 0.45359237;
            Lbs = decimal.Round((decimal)(kgD * conversion),2);

            return Lbs;

        }
        //Calculate freight price based on customer terms
        [Authorize]
        public CalculatedFreight CalculateFreightPrice(string onr, decimal price, ApplicationUser user, string exceptionCustomer = "")
        {
            CalculatedFreight result = new CalculatedFreight();
            result.OldPrice = price;
            result.NewPrice = 0; 

            bool isFreightProduct = false;
            OrderModel mOM= new OrderModel(_Config, user);
            CustomerModel mCM = new CustomerModel(_Config, user);

            var Order = mOM.GetOrderById(onr,true);
            Customer C = mCM.GetCustomerById(Order.InvoiceCustomerNo);

            decimal fixedFreightCharge = C.Numeric_2.Value; //Fixed Freight Charge
     
            decimal exceptionSurchargePercentage = 0; 
            if (!string.IsNullOrEmpty(exceptionCustomer))
            {
                //If it is freight exception calculation, get fixed export charge from intenal customer number, Num 2
                Customer exC = mCM.GetCustomerById(exceptionCustomer);
           
                exceptionSurchargePercentage = exC.Numeric_1.HasValue ? exC.Numeric_1.Value == 0 ? 20 : exC.Numeric_1.Value:20;

            }
            exceptionSurchargePercentage = (exceptionSurchargePercentage / 100) + 1;
            decimal freightThreshold = C.Numeric_3.Value; //Freight threshold, if above no freight charge should be charged, unless there is a transportsurcharge, then all surcharges should be applied.
            bool isAboveFreightThreshold = (GeneralFunctions.getDecimalFromStr(Order.DeliveredValue) + GeneralFunctions.getDecimalFromStr(Order.NotDeliveredValue)) >= freightThreshold && freightThreshold!=0;
                        

            string freightProduct = ConfigurationManager.AppSetting["DeliverCostProduct"];
            string[] freightProductArray = string.IsNullOrEmpty(freightProduct) ? new string[1] { "I" } : freightProduct.Split('|');
            var row = Order.OrderRows.Find(r => (r.ProductNo == "9992" || r.ProductNo =="9996") && r.DeliverState!="5");

            isFreightProduct = freightProductArray.Any(d => d?.ToLower().Trim() == row.ProductNo);

            result.IsFreightFreeDay = !isSurchargeDay(C.Text_6, Order.PreferedDeliverDate);

            if(isSenderPayer(C.DeliverTermsId, user) && isFreightProduct)
            {

                result.FixedTransportSurcharge = fixedTransportSurcharge(Order.DeliverWay, Order.CurrencyValue, _userManager.GetUserAsync(User)?.Result);
              
                //if there is a freight exception, default to no additional surcharge, unless found on customer. 
                decimal surchargePercentage = (C.Numeric_1.Value == 0) && string.IsNullOrEmpty(exceptionCustomer) ? 50 : !string.IsNullOrEmpty(exceptionCustomer) && C.Numeric_1==0 ? 0:C.Numeric_1.Value;

                if (!isAboveFreightThreshold && !result.IsFreightFreeDay)
                {
                   
                    //result = row.CostPrice*((surchargePercentage/100)+1)+ fixedFreightCharge+ fixedTransportSurcharge(Order.DeliverWayId, Order.CurrencyValue);

                    result.SurchargeRate = surchargePercentage;
                    result.NewPrice = Math.Round((price * ((surchargePercentage / 100) + 1)*exceptionSurchargePercentage + fixedFreightCharge + result.FixedTransportSurcharge), 2);
                    result.IsFreightChanged = true;

                }
                else if(result.FixedTransportSurcharge>0)
                {                  
                    //result = row.CostPrice*((surchargePercentage/100)+1)+ fixedFreightCharge+ fixedTransportSurcharge(Order.DeliverWayId, Order.CurrencyValue);

                    result.SurchargeRate = surchargePercentage;
                    result.NewPrice = Math.Round((price * ((surchargePercentage / 100) + 1) * exceptionSurchargePercentage + fixedFreightCharge + result.FixedTransportSurcharge), 2);
                    result.IsFreightChanged = true;



                }
                else
                {
                    result.NewPrice = result.OldPrice;
                    result.IsFreightChanged = false;


                }

            }
            else
            {
                result.NewPrice = result.OldPrice;
                result.IsFreightChanged = false;

            }

            result.FixedFreightSurcharge = fixedFreightCharge;
            result.FreightThreshold = freightThreshold;
            result.OrderValue = (GeneralFunctions.getDecimalFromStr(Order.DeliveredValue) + GeneralFunctions.getDecimalFromStr(Order.NotDeliveredValue));
            result.isAboveFreightThreshold = isAboveFreightThreshold;

         

            return result;
        }


            decimal fixedTransportSurcharge(string dwi, string currency, ApplicationUser user)
            {
            decimal result = 0;

            try
            {
                BaseDataModel bM = new BaseDataModel(_Config, user);
                var DeliverWay = bM.GetBaseTable("03", dwi);
                string DeliverWayString = DeliverWay?.Description2?.PadRight(35).Substring(30, 5).Trim();

                decimal additionalCost = 0;

                decimal.TryParse(DeliverWayString, out additionalCost);
                decimal currencyValue = GeneralFunctions.getDecimalFromStr(currency);

                result = additionalCost / currencyValue;
            }
            catch (Exception e)
            {

             
            }

            return result;
            }
            bool isSenderPayer(string terms, ApplicationUser user)
            {
                bool result = false;

            try
            {
                BaseDataModel mBM = new BaseDataModel(_Config, user);
                var bm = mBM.GetBaseTable("02", terms);
                if (bm == null)
                {
                    result = true;
                    return result;
                }

                string checkNoFreightString = "";
                if (!string.IsNullOrEmpty(bm.Description2))
                    checkNoFreightString = bm.Description2.PadRight(3);

                checkNoFreightString = checkNoFreightString.PadRight(2).Substring(0, 2).ToLower().Trim();
                if (bm.Code7 == "X" && checkNoFreightString != "no")
                {
                    result = true;
                }

            }
            catch (Exception e)
            {

               
            }
                

                return result;

            }
         
            bool isSurchargeDay(string freeDaysString, string deliverdate)
        {
            bool result = false;

            try
            {
                    if (freeDaysString.Trim().ToLower() == "a")
                    {
                       
                        result = false;
                    }
                    else
                    {
                        string[] freeDays = string.IsNullOrEmpty(freeDaysString) ? new string[1] { "I" } : freeDaysString.Split(',');


                    // Get deliverdate to use in check
                        string deliverydate;
                        deliverydate = string.IsNullOrEmpty(deliverdate) ? DateTime.Now.ToString("yyMMdd") : deliverdate;
                        DateTime deliverdateAsDate = DateTime.ParseExact(deliverydate, "yyMMdd", CultureInfo.InvariantCulture);

                        // Get weekday of deliverydate
                        string deliverdateWeekday = deliverdateAsDate.DayOfWeek.ToString().Substring(0, 2).ToLower();

                        // Check if deliverdate is surcharge day
                        result = !freeDays.Any(d => d?.ToLower().Trim() == deliverdateWeekday);
                    }

            }
            catch (Exception e)
            {

            }

            return result;
        }

        [HttpPost]
        [Authorize]
        public string AddPackageInformationToFS(TransportVM vm, bool sendMail = false)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            LogtradeModel mLM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);
            DeliverNoteModel mDNM = new DeliverNoteModel(_Config, _userManager.GetUserAsync(User)?.Result);
            var user = _userManager.GetUserAsync(User)?.Result;

            LogtradeShipment shipment = mLM.GetShipment(new GetShipmentParam { DeliverNote = vm.Transport.DeliverNo, GarpUser = "GIS", GarpTerminal = "GS" });
            shipment.GoodsItemList = mLM.GetGarpPackageList(vm.Transport.DeliverNo);

            // CHange values that user can change in the view
            shipment.TermsOfDeliveryCode = vm.Transport.DeliverTermId;

            //var list = mLM.GetPackageTypeMappingList(shipment.ProductCode);

            //foreach (var package in shipment.GoodsItemList)
            //{
            //    package.PackageTypeCode = list.Find(x => x.Garp == package.PackageTypeCode).Logtrade;

            //}
            List<string> UOMList = null;
           
            if(vm.UOM == "UsStandard")
            {
                UOMList = new List<string>() {  "in", "lbs", "cu ft." };

            }
            else
            {
                vm.UOM = "Metric";
                UOMList = new List<string>() { "cm", "Kg", "m3" };
            }

            int rowNo = 255;
            bool rowExisist = false;
            DeliverNoteRow or = null;
            OrderHead oh = new OrderHead();
            DeliverNoteOH orderHead = new DeliverNoteOH();
            
            if (!string.IsNullOrEmpty(vm.Transport.OrderNo))
            {
                orderHead = mDNM.GetDeliverNote(vm.Transport.DeliverNo, true);                
                or = orderHead.Rows.Find(o => (o.ProductNo == "9992" || o.ProductNo == "9996"));


                if (or != null)
                {
                    rowNo = or.RowNo;
                    rowExisist = true;
                }
                else {

                    oh = mOM.GetOrderById(vm.Transport.OrderNo, true);
                    OrderRow r = new OrderRow();
                if (oh.OrderRows.FindAll(x => (x.RowNo >= 251 && x.RowNo <= 254)).Count() < 4)
                    {
                        r = new OrderRow
                        {
                            OrderNo = oh.OrderNo,
                            RowNo = 255,
                            ProductNo = "9992",
                            Amount = 1,
                            WarehouseNo = "1",
                            CostPrice = 0,
                            FixedCostPrice = "F",                            
                            PreferedDeliverDate = orderHead.PreferedDeliverDate

                                         
                    };
                       
                    }
                    else
                    {
                        r = new OrderRow
                        {
                            OrderNo = orderHead.OrderNo,
                            RowNo = 255,
                            ProductNo = "9996",
                            Amount = 1,
                            WarehouseNo = "1",
                            CostPrice = 0,
                            FixedCostPrice = "F",
                            PreferedDeliverDate = orderHead.PreferedDeliverDate
                            
                           
                        };
                  
                    }
                    mOM.UpdateOrderRowList(new List<OrderRow> { r });
                    oh = mOM.GetOrderById(vm.Transport.OrderNo, true);
                    var delRow = oh.OrderRows.Find(x => (x.ProductNo == "9992" || x.ProductNo == "9996") && x.DeliverState != "5");


                   var deliverresult = mOM.Deliver(new List<DeliverRowParam> { new DeliverRowParam { OrderNo = oh.OrderNo, RowNo = delRow.RowNo, AmountToDeliver = 1, DeliverState = "5", DeliverNote = orderHead.DeliverNoteNo,  DeliverDate = "", DeliverMaterialRow = false, HandleOperationRow = false, SetWarehouseOnOrderrow = false, WarehouseNo = "1" } });
                    orderHead = null; 
                    orderHead = mDNM.GetDeliverNote(vm.Transport.DeliverNo, true);
                    or = orderHead.Rows.Find(o => (o.ProductNo == "9992" || o.ProductNo == "9996"));
                    rowNo = or.RowNo;
                    if(or!=null)
                    {
                        rowExisist = true;
                    }
                }
           

            List<DeliverNoteRowText> deliverNoteRowTexts = new List<DeliverNoteRowText>();
                decimal netWeightShipment = 0;
                if (vm.UOM =="UsStandard")
                {
                     netWeightShipment = getLbsFromKg(vm.NetWeight);
                }
                else
                {
                    netWeightShipment = vm.NetWeight;
                }
            

            int i = 1;
            int totalamount = 0;
            decimal totalGrossWeight = 0;
            decimal totalVolume = 0;

            DeliverNoteRowText emptyrow = new DeliverNoteRowText();
            emptyrow.DeliverNoteNo = shipment.DeliverNote;
            emptyrow.RowNo = rowNo.ToString();
            emptyrow.TextRowNo = "255";
            emptyrow.InvoiceState = "0";
            emptyrow.DeliverNoteState = "1";
            emptyrow.Text = "";

            deliverNoteRowTexts.Add(emptyrow);
            DeliverNoteRowText fullshipment = new DeliverNoteRowText();
            fullshipment.DeliverNoteNo = shipment.DeliverNote;
            fullshipment.RowNo = rowNo.ToString();
            fullshipment.TextRowNo = "255";
            fullshipment.InvoiceState = "0";
            fullshipment.DeliverNoteState = "1";
            fullshipment.Text = "";
                deliverNoteRowTexts.Add(fullshipment);

            DeliverNoteRowText fullVolume = new DeliverNoteRowText();
            fullVolume.DeliverNoteNo = shipment.DeliverNote;
            fullVolume.RowNo = rowNo.ToString();
            fullVolume.TextRowNo = "255";
            fullVolume.InvoiceState = "0";
            fullVolume.DeliverNoteState = "1";
            fullVolume.Text = "";
            deliverNoteRowTexts.Add(fullVolume);

            DeliverNoteRowText fullNetWeight = new DeliverNoteRowText();
            fullNetWeight.DeliverNoteNo = shipment.DeliverNote;
            fullNetWeight.RowNo = rowNo.ToString();
            fullNetWeight.TextRowNo = "255";
            fullNetWeight.InvoiceState = "0";
            fullNetWeight.DeliverNoteState = "1";
            fullNetWeight.Text = "Total Net Weight (" + UOMList[1] + "): " + netWeightShipment;
            deliverNoteRowTexts.Add(fullNetWeight);

            DeliverNoteRowText fullGrossWeight = new DeliverNoteRowText();
            fullGrossWeight.DeliverNoteNo = shipment.DeliverNote;
            fullGrossWeight.RowNo = rowNo.ToString();
            fullGrossWeight.TextRowNo = "255";
            fullGrossWeight.InvoiceState = "0";
            fullGrossWeight.DeliverNoteState = "1";
            fullGrossWeight.Text = "";
            deliverNoteRowTexts.Add(fullGrossWeight);

            foreach (var items in shipment.GoodsItemList)
            {    
                //items.PackageTypeCode = list.Find(x => x.Logtrade == items.PackageTypeCode).Garp;
                int count = 0;
                while (count < items.Amount)
                {
                                    
                    DeliverNoteRowText itemNo = new DeliverNoteRowText();
                    itemNo.DeliverNoteNo = shipment.DeliverNote;
                    itemNo.RowNo = rowNo.ToString();
                    itemNo.TextRowNo = "255";
                    itemNo.InvoiceState = "0";                    
                    itemNo.DeliverNoteState = "1";
                    itemNo.Text = "Pallet: " + i;
                    deliverNoteRowTexts.Add(itemNo);


                    DeliverNoteRowText grossWeigth = new DeliverNoteRowText();
                    grossWeigth.DeliverNoteNo = shipment.DeliverNote;
                    grossWeigth.RowNo = rowNo.ToString();
                    grossWeigth.TextRowNo = "255";
                    grossWeigth.InvoiceState = "0";
                    grossWeigth.DeliverNoteState = "1";

                    grossWeigth.Text = "Gross weight ("+UOMList[1]+"): " + items.Weight;
                    deliverNoteRowTexts.Add(grossWeigth);

                    DeliverNoteRowText pieces = new DeliverNoteRowText();
                    pieces.DeliverNoteNo = shipment.DeliverNote;
                    pieces.RowNo = rowNo.ToString();
                    pieces.TextRowNo = "255";
                    pieces.InvoiceState = "0";
                    pieces.DeliverNoteState = "1";

                    pieces.Text = "Qty: " + items.PackageItems[0].Pieces;
                    deliverNoteRowTexts.Add(pieces);

                    DeliverNoteRowText dimension = new DeliverNoteRowText();
                    dimension.DeliverNoteNo = shipment.DeliverNote;
                    dimension.RowNo = rowNo.ToString();
                    dimension.TextRowNo = "255";
                    dimension.InvoiceState = "0";
                    dimension.DeliverNoteState = "1";
                        if (vm.UOM == "UsStandard")
                        {
                            dimension.Text = "Dim. (" + UOMList[0] + "): " + items.PackageItems[0].Length + "x" + items.PackageItems[0].Width + "x" + items.PackageItems[0].Height ;
                        }
                        else
                        {
                            dimension.Text = "Dim. (" + UOMList[0] + "): " + items.PackageItems[0].Length * 100 + "x" + items.PackageItems[0].Width * 100 + "x" + items.PackageItems[0].Height * 100;
                        }
                            deliverNoteRowTexts.Add(dimension);

                    DeliverNoteRowText lines = new DeliverNoteRowText();
                    lines.DeliverNoteNo = shipment.DeliverNote;
                    lines.RowNo = rowNo.ToString();
                    lines.TextRowNo = "255";
                    lines.InvoiceState = "0";
                    lines.DeliverNoteState = "1";
                    lines.Text = "----------";
                    deliverNoteRowTexts.Add(lines);

                    totalamount ++;
                    totalGrossWeight += items.Weight;
                    totalVolume += items.Volyme;
                    count++;
                    i++;
                }
            }
            deliverNoteRowTexts[1].Text = "Number of pallets/boxes: " + totalamount;

                    deliverNoteRowTexts[2].Text = "Total Volume ("+ UOMList[2] +"): " + totalVolume;
       
      
            deliverNoteRowTexts[4].Text = "Total Gross Weight ("+ UOMList[1]+"): " + totalGrossWeight;
                  

            var ts = mOM.GetTransportForDeliverNote(shipment.DeliverNote);
            ts.PackageAmount1 = totalamount;
            ts.Weight1 = totalGrossWeight;
            ts.Volume1 = totalVolume;

            mOM.UpdateTransport(ts);
            or.TextRows = new List<DeliverNoteRowText>();
            or.TextRows = deliverNoteRowTexts;

            if(rowExisist)
            {
               mDNM.UpdateDeliverNoteRowTextList(deliverNoteRowTexts);

            }
            else
            {
                orderHead.Rows.Add(or);
                mDNM.UpdateDeliverNoteHead(orderHead);
            }


            //mDNM.UpdateDeliverNoteRowTextList(deliverNoteRowTexts);

            if (sendMail)
            {
                var sent = SendMail(shipment.OrderNo, shipment.DeliverNote, deliverNoteRowTexts);
            }
            return "Ok";
            }
            else
            {
                return "No Ordernumber";
            }
        }

    

        public bool SendMail(string onr, string dn, List<DeliverNoteRowText> textrows)
        {
            bool result = false;

            try
            {
                SendMailParam param = new SendMailParam();
                string HtmlRows = "<br>";
                foreach (var item in textrows)
                {
                    HtmlRows += item.Text + "<br>";
                }
                param.FromEmail = ConfigurationManager.AppSetting["MailReplyAddress"];
                param.Subject = "Order: "+ onr + ", Följesedel: "+ dn +", Redo att bokas för transport";
                param.To = new List<SendMailReceipent> { new SendMailReceipent { Email = ConfigurationManager.AppSetting["MailToAddress"], Name = "Order" } };
                param.MessageHtml = "<html><body><p>Hej,</p><p>Följande order "+ onr +" har nu kompletterats med frakt information, och det finns nu tillgängligt på följesedelen i Garp.</p><p>Nedan är informationen ang. sändningen:"+HtmlRows+"</p></body></html>";
                MailFunction mM = new MailFunction(_Config, _userManager.GetUserAsync(User)?.Result);
                result = mM.CreateEmail(param);
            }
            catch (Exception e)
            {
                result = false; 
                
            }



            return result;  
        }


        [Authorize]
        public IActionResult AddPackage(PackageItem pkg)
        {
            LogtradeModel logM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);
            ViewBag.PackageQty += pkg.Quantity; 
            LogtradeGoodsItem lgPkg = new LogtradeGoodsItem
            {
                PackageTypeCode = pkg.PackageType,
                Amount = pkg.Quantity,
                Weight = GeneralFunctions.getDecimalFromStr(pkg.Weight),
                Volyme = GeneralFunctions.getDecimalFromStr(pkg.Volume),               
                LoadingMeters = GeneralFunctions.getDecimalFromStr(pkg.LoadingMeters) 

            }; 
            lgPkg.PackageItems = new List<LogtradePackageItem>();

            lgPkg.PackageItems.Add(new LogtradePackageItem
            {
                Height = GeneralFunctions.getDecimalFromStr(pkg.Height),
                Width = GeneralFunctions.getDecimalFromStr(pkg.Width),
                Length = GeneralFunctions.getDecimalFromStr(pkg.Length),
                Pieces = pkg.Pieces.HasValue ? pkg.Pieces.Value:0
            });


               logM.AddGarpPackage(lgPkg, pkg.DeliverNoteNo);


            // addToTempDataGoodsItemList(pkg);
            return ViewComponent("TAPackageSelector", new { pkg = pkg, delivernote = pkg.DeliverNoteNo, controller = "Transport", add_action = "AddPackage", copy_action = "CopyPackage", update_action = "UpdatePackage", delete_action = "DeletePackage" });
        }
        [HttpPost]
        [Authorize]
        public IActionResult CopyPackage(string delivernote, string id)
        {
           
            LogtradeModel logM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);


            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
        
            LogtradeGoodsItem copyGI = logM.GetGarpPackageList(delivernote + id).FirstOrDefault();

            logM.AddGarpPackage(copyGI, delivernote);


            // addToTempDataGoodsItemList(pkg);
            return ViewComponent("TAPackageSelector", new { delivernote = delivernote, controller = "Transport", add_action = "AddPackage", copy_action = "CopyPackage", update_action = "UpdatePackage", delete_action = "DeletePackage" });
        }

        [HttpPost]
        [Authorize]
        public IActionResult Refresh(string delivernote)
        {
            return ViewComponent("TAPackageSelector", new { delivernote = delivernote, controller = "Transport", add_action = "AddPackage", copy_action = "CopyPackage", update_action = "UpdatePackage", delete_action = "DeletePackage" });
        }

        [HttpPost]
        [Authorize]
        public IActionResult UpdatePackage(PackageItem pkg)
        {
            string result = "";
            LogtradeModel logM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);
            LogtradeGoodsItem lgPkg = new LogtradeGoodsItem
            {
                PackageTypeCode = pkg.PackageType,
                GarpId = pkg.ID,
                Amount = pkg.Quantity,
                Weight = GeneralFunctions.getDecimalFromStr(pkg.Weight),
                Volyme = GeneralFunctions.getDecimalFromStr(pkg.Volume),
                LoadingMeters = GeneralFunctions.getDecimalFromStr(pkg.LoadingMeters)


            };
            lgPkg.PackageItems = new List<LogtradePackageItem>();
            lgPkg.PackageItems.Add(new LogtradePackageItem
            {
                Height = GeneralFunctions.getDecimalFromStr(pkg.Height),
                Width = GeneralFunctions.getDecimalFromStr(pkg.Width),
                Length = GeneralFunctions.getDecimalFromStr(pkg.Length),
                Pieces = pkg.Pieces.HasValue ? pkg.Pieces.Value : 0
            });

            logM.UpdateGarpPackage(pkg.DeliverNoteNo, lgPkg);

        
            // addToTempDataGoodsItemList(pkg);
            return ViewComponent("TAPackageSelector", new { delivernote = pkg.DeliverNoteNo, controller = "Transport", add_action = "AddPackage", copy_action = "CopyPackage", update_action = "UpdatePackage", delete_action = "DeletePackage" });
        }




        [HttpPost]
        [Authorize]
        public IActionResult DeletePackage(string delivernote, int id)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                LogtradeModel logM = new LogtradeModel(_Config, _userManager.GetUserAsync(User)?.Result);
                logM.RemoveGarpPackage(id.ToString(), delivernote);
                /*

                var transport = mOM.GetTransportForDeliverNote(delivernote);

                if (transport != null)
                {
                    if (id == 1)
                    {
                        transport.PackageType1 = "";
                        transport.PackageAmount1 = 0;
                        transport.Weight1 = 0;
                        transport.Volume1 = 0;
                        transport.Height1 = 0;
                        transport.Length1 = 0;
                        transport.Width1 = 0;
                        transport.GoodsMeasurementType = "";
                    }
                    else if (id == 2)
                    {
                        transport.PackageType2 = "";
                        transport.PackageAmount2 = 0;
                        transport.Weight2 = 0;
                        transport.Volume2 = 0;
                        transport.Height2 = 0;
                        transport.Length2 = 0;
                        transport.Width2 = 0;
                        transport.GoodsMeasurementType = "";
                    }
                    else if (id == 3)
                    {
                        transport.PackageType3 = "";
                        transport.PackageAmount3 = 0;
                        transport.Weight3 = 0;
                        transport.Volume3 = 0;
                        transport.Height3 = 0;
                        transport.Length3 = 0;
                        transport.Width3 = 0;
                        transport.GoodsMeasurementType = "";
                    }

                    removeFromTempDataGoodsItemList(delivernote, id);

                    mOM.UpdateTransport(transport);
                } */

            }
            catch (Exception r)
            {

            }


            return ViewComponent("TAPackageSelector", new { delivernote = delivernote, controller = "Transport", add_action = "AddPackage", update_action = "UpdatePackage", delete_action = "DeletePackage" });
        }

        //private void updateDeliverNoteLabelTexts(Transport transport)
        //{
        //    try
        //    {
        //        List<DeliverNoteRowText> textList = new List<DeliverNoteRowText>();
        //        var delNote = mDNM.GetDeliverNote(transport.DeliverNo, true);

        //        foreach (var row in delNote.Rows)
        //        {
        //            decimal totalPallets = transport.PackageAmount1.Value + transport.PackageAmount2.Value + transport.PackageAmount3.Value;

        //            // Add Summary of total packages
        //            DeliverNoteRowText text = new DeliverNoteRowText();
        //            text.DeliverNoteNo = transport.DeliverNo;
        //            text.RowNo = row.RowNo.ToString();
        //            text.TextRowNo = "255";
        //            text.Text = "Total Pallets: " + totalPallets;
        //            textList.Add(text);

        //            if (transport.PackageAmount1.Value > 0)
        //            {
        //                for (int i = 0; i < transport.PackageAmount1.Value; i++)
        //                {
        //                    DeliverNoteRowText p1_1 = new DeliverNoteRowText();
        //                    p1_1.DeliverNoteNo = transport.DeliverNo;
        //                    p1_1.RowNo = row.RowNo.ToString();
        //                    p1_1.TextRowNo = "255";
        //                    p1_1.Text = "Pallet: " + i;
        //                    textList.Add(p1_1);

        //                    DeliverNoteRowText p1_2 = new DeliverNoteRowText();
        //                    p1_2.DeliverNoteNo = transport.DeliverNo;
        //                    p1_2.RowNo = row.RowNo.ToString();
        //                    p1_2.TextRowNo = "255";
        //                    p1_2.Text = "Gross weight: " + transport.Weight1.Value;
        //                    textList.Add(p1_2);

        //                    DeliverNoteRowText p1_3 = new DeliverNoteRowText();
        //                    p1_3.DeliverNoteNo = transport.DeliverNo;
        //                    p1_3.RowNo = row.RowNo.ToString();
        //                    p1_3.TextRowNo = "255";
        //                    p1_3.Text = "Net weight: " + transport.NetWeight1;
        //                    textList.Add(p1_3);

        //                    DeliverNoteRowText p1_4 = new DeliverNoteRowText();
        //                    p1_4.DeliverNoteNo = transport.DeliverNo;
        //                    p1_4.RowNo = row.RowNo.ToString();
        //                    p1_4.TextRowNo = "255";
        //                    p1_4.Text = "Qty: " + transport.Qty1;
        //                    textList.Add(p1_4);

        //                    DeliverNoteRowText p1_5 = new DeliverNoteRowText();
        //                    p1_5.DeliverNoteNo = transport.DeliverNo;
        //                    p1_5.RowNo = row.RowNo.ToString();
        //                    p1_5.TextRowNo = "255";
        //                    p1_5.Text = "Dim cm: " + transport.Length1.ToString().Trim() + "x" + transport.Width1.ToString().Trim() + "x" + transport.Height1.ToString().Trim();
        //                    textList.Add(p1_5);

        //                    DeliverNoteRowText p1_6 = new DeliverNoteRowText();
        //                    p1_6.DeliverNoteNo = transport.DeliverNo;
        //                    p1_6.RowNo = row.RowNo.ToString();
        //                    p1_6.TextRowNo = "255";
        //                    p1_6.Text = "----------";
        //                    textList.Add(p1_6);
        //                }
        //            }
        //        }

        //        mDNM.UpdateDeliverNoteRowTextList(textList);
        //    }
        //    catch (Exception e)
        //    {
        //    }
        //}

        private List<PackageItem> getTempDataGoodsItemList(string delivernote)
        {
            List<PackageItem> result = null;

            try
            {
                if (_memoryCache.TryGetValue(delivernote, out List<PackageItem> packageList))
                {
                    result = packageList;
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        private void addToTempDataGoodsItemList(PackageItem pkg)
        {
            try
            {
                var cacheKey = pkg.DeliverNoteNo;
                if (_memoryCache.TryGetValue(cacheKey, out List<PackageItem> packageList))
                {
                    pkg.ID = (packageList.Count + 1).ToString(); 

                    packageList.Add(pkg);
                    _memoryCache.Set(cacheKey, packageList, _cacheExpiryOptions);
                }
                else
                {
                    packageList = new List<PackageItem>();
                    pkg.ID = "1";
                    packageList.Add(pkg);
                    _memoryCache.Set(cacheKey, new List<PackageItem> { pkg }, _cacheExpiryOptions);
                }

            }
            catch (Exception)
            {
            }
        }

        private void removeFromTempDataGoodsItemList(string delivernote, int id)
        {
            try
            {
                --id;

                if (_memoryCache.TryGetValue(delivernote, out List<PackageItem> packageList))
                {
                    if (packageList.Count >= id)
                    {
                        packageList.RemoveAt(id);
                    }
                    _memoryCache.Set(delivernote, packageList, _cacheExpiryOptions);
                }
            }
            catch (Exception)
            {
            }
        }
    }

    static class ConfigurationManager
    {
        public static IConfiguration AppSetting { get; }
        static ConfigurationManager()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }
    }

    public class CalculatedFreight
    {
        public bool IsFreightChanged { get; set; } = false;
        public decimal FreightThreshold { get; set; } = 0;
        public bool isAboveFreightThreshold { get; set; } = false;
        public decimal OrderValue{ get; set; } = 0;
        public bool IsFreightFreeDay { get; set; } = false;

        public decimal FixedFreightSurcharge { get; set; } = 0;
        public decimal FixedTransportSurcharge { get; set; } = 0;
        public decimal SurchargeRate { get; set; } = 0;
        public decimal OldPrice { get; set; } = 0;
        public decimal NewCostPrice { get; set; } = 0;
        public decimal NewPrice { get; set; } = 0;
        public bool? IsFreightException { get; set; } = false;
        public decimal? FreightExceptionPrice { get; set; } = 0;
        public string FreightExceptionOrder { get; set; } = "";
        public int? FreightExceptionRowNo { get; set; } = 0;
        public FreightExceptions? FreightExceptions { get; set; }

    }

}
