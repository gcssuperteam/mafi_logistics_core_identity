﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Controllers
{
    public class ProductionOverView : Controller
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private OrderModel mOM;
        //private ProductionModel mPOM;
        //private LogisticModel mLOM;
        //private BaseDataModel mBDM;
        //private PrintingModel mPM;
        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;

        public ProductionOverView(IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            //mOM = new OrderModel(config);
            //mPOM = new ProductionModel(config);
            //mLOM = new LogisticModel(config);
            //mBDM = new BaseDataModel(config);
            //mPM = new PrintingModel(config);
            _userManager = userManager;
            _Config = config;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult ProductionOverview()
        {
            OrderViewVM result = new OrderViewVM();
            List<OrderHead> orderList = new List<OrderHead>();
            FilterDTO filterDto = null;
            PickListParam param = new PickListParam();
            List<BaseTable> delTermsList = null;
            var user = _userManager.GetUserAsync(User)?.Result;

            try
            {
                LogisticModel mLOM = new LogisticModel(_Config, user);

                param.IncludeDeliverCustomer = false;
                param.IncludeDeliveredRows = false;
                param.Index = 1;
                param.IndexCount = 0;
                param.OrderSerie = "";
                param.IndexFilter = "";
                param.MaxCount = 0;
                param.ReverseReading = true;
                param.WithRows = true;
                

                filterDto = getFilterList("FILTERP");

                param.Filter = filterDto.Filter;

                if (filterDto.UseCache == true)
                {
                    param.Cache = new CacheParam { CacheId = user.GisToken + "_production", ReadFrom = true, UpdateTo = false };
                }

                orderList = mLOM.GetOrderListWithAdditionalOrder(param);
           
                foreach (OrderHead oh in orderList)
                { 
                
                    foreach (OrderRow or in oh.OrderRows)
                    {
                       
                        if (or.MTF.CompareTo("0") > 0 && or.MTF!="5")
                        {
                            OrderViewRow ovr = new OrderViewRow();

                            ovr.CustomerNo = oh.DeliverCustomerNo;
                            ovr.CustomerName = oh.DeliverCustomerName;
                            ovr.OrderNo = oh.OrderNo;
                            ovr.ProductNo = or.ProductNo;
                            ovr.ProductDescription = or.ProductDescription;
                            ovr.RowNo = or.RowNo;
                            ovr.Amount = or.Amount.Value;
                            ovr.DeliverDate = or.PreferedDeliverDate;
                                                       result.Orderrows.Add(ovr);
                        }

                    }
                }
                
                result.Orderrows = result.Orderrows.OrderBy(o => o.DeliverDate).ToList();
            }
            catch (Exception e)
            {
                logger.Error("Error in OrderProduction()", e);
            }

            return View(result);
        }
        [Authorize]
        public IActionResult ProductionDetails(string id, string row)
        {
            MaterialPickVM result = new MaterialPickVM();

            try
            {
                ProductionModel mPM = new ProductionModel(_Config, _userManager.GetUserAsync(User)?.Result);
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                result.OH = mPM.GetMaterialOrder(id, row);
                OrderRow or = mOM.GetOrderRow(id, row);

                // Get warehouse from orderrow of produced product (used in selection of exteneded warehouselist)
                result.ManufacturedWarehouse = or.WarehouseNo;
                result.ManufacturedAmount = result.OH.Amount - result.OH.DeliveredAmount;

                result.MaterialRowList = result.OH.MaterialRows.OrderBy(o => o.PreferedDeliverDate).ToList();

                result.Succeeded = true;
            }
            catch (Exception e)
            {
                
            }

            return PartialView(result);
        }
        [HttpPost]
        [Authorize]

        public ContentResult PrintProductionsList(string id)
        {
            ERPReport report = new ERPReport();
            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            try
            {
                report = _Config.GetSection("ProductionList").Get<ERPReport>();
                //report.DocGen = ConfigurationManager.AppSetting["Picklist:DocGen"];
                //report.Report = ConfigurationManager.AppSetting["Picklist:Report"];
                //report.Medium = ConfigurationManager.AppSetting["Picklist:Medium"];
                //report.Form = ConfigurationManager.AppSetting["Picklist:Form"];
                //report.FilePath = ConfigurationManager.AppSetting["Picklist:FilePath"];
                //report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["Picklist:Wait"]);
                //report.IndexFrom = id;
                //report.IndexTo = id;
            }
            catch
            {
                report.DocGen = "265";
                report.Report = "33";
                report.Medium = "P";
                report.Form = "A4SF";
            }

            report.IndexFrom = id;
            report.IndexTo = id;

            if (mPM.Print(report))
            {
                try
                {

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return Content("Plocklista utskriven på order " + id);
        }

        private string parseFilterConstants(string filter)
        {
            int count = 0;

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter.Contains("#DAT"))
                {
                    while (filter.Contains("#DAT"))
                    {
                        int pos = filter.IndexOf("#DAT");
                        string whole = filter.Substring(pos, 6);
                        string date = DateTime.Now.AddDays(int.Parse(whole.Substring(4))).ToString("yyMMdd");
                        filter = filter.Replace(whole, date);
                        count++;

                        // If this high count something went wrong
                        if (count > 20)
                        {
                            break;
                        }
                    }
                }
            }

            return filter;
        }

        private FilterDTO getFilterList(string prefix)
        {
            FilterDTO result = new FilterDTO();

            try
            {
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);

                List<BaseTable> filterlist = mBDM.GetBaseTableList(prefix);

                if (filterlist != null)
                {
                    foreach (BaseTable bt in filterlist)
                    {
                        // First post we read all values, al other we reda descriptions to add only filters
                        if (string.IsNullOrEmpty(result.Id))
                        {
                            result = new FilterDTO { Id = string.IsNullOrEmpty(bt.Id) ? "#" : bt.Id, Description = bt.Description, Type = bt.Type, Filter = bt.Description2, UseCache = bt.Code1 == "T" ? true : false };
                        }
                        else
                        {
                            result.Filter += bt.Description.Trim() + bt.Description2.Trim();
                        }
                    }
                }

                result.Filter = parseFilterConstants(result.Filter);
            }
            catch (Exception)
            {

            }

            return result;
        }
    }
}
