using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.Web.CodeGeneration.Utils;
//using NuGet.Frameworks;

namespace Babc_logistic_core.Controllers
{
    public class OrderController : Controller
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string mGeneralIndexSearchCount = "0";
        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;
        
        public OrderController(IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            try
            {
                _Config = config;
                _userManager = userManager;

            }
            catch (Exception)
            {
            }
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Index(string search_value, string search_type)
        {
            List<OrderHead> lst = new List<OrderHead>();

            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            if (string.IsNullOrEmpty(search_value))
            {
                return View();
            }

            if (search_type.Equals("Datum"))
            {
                lst = mOM.GetOrderByIdxList("1", "*", "LDT>=" + search_value, false, 20, true);
            }
            else if (search_type.Equals("Kund"))
            {
                lst = mOM.GetOrderByIdxList("2", search_value, "LEF<=4", false, 50, false);
            }
            else if (search_type.Equals("Säljarkod"))
            {
                lst = mOM.GetOrderByIdxList("4", search_value, "*", false, 50, false);
            }

            List<OrderHead> sortedList = lst.OrderByDescending(o => o.OrderNo).ToList();

            ViewBag.search_value = search_value;
            ViewBag.search_type = search_type;

            return View(sortedList);
        }

        [Authorize]
        public IActionResult PickList(string search_value, string search_value2, string search_type)
        {
            var user = _userManager.GetUserAsync(User)?.Result;

            OrderModel mOM = new OrderModel(_Config, user);
            BaseDataModel mBDM = new BaseDataModel(_Config, user);
            LogisticModel mLOM = new LogisticModel(_Config, user);

            List<OrderHead> lst = new List<OrderHead>();
            StringBuilder filter = new StringBuilder();
            List<OrderHead> tempLst = new List<OrderHead>();
            List<string> additionalLst = new List<string>();
            List<string> checkLst = new List<string>();

            PickListParam olp = new PickListParam();
            OrderHead ohda = new OrderHead();

            // When landing on the page the first time
            if (string.IsNullOrEmpty(search_type))
                return View();

            if (search_type.Equals("supplie"))
            {
                try
                {
                    List<FilterDTO> filterList = getFilterList("FILTERS");
                    olp.Filter = parseFilterConstants(filterList[0].Filter);

                    if (filterList[0].UseCache == true)
                    {
                        olp.Cache = new CacheParam { CacheId = user.GisToken + "_supplie", ReadFrom = true, UpdateTo = false, CheckConsistency = true };
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error setting filter", e);
                }
            }
            else if (search_type.Equals("pre"))
            {
                try
                {
                    List<FilterDTO> filterList = getFilterList("FILTERO");
                    olp.Filter = parseFilterConstants(filterList[0].Filter);

                    if (filterList[0].UseCache == true)
                    {
                        olp.Cache = new CacheParam { CacheId = user.GisToken + "_pre", ReadFrom = true, UpdateTo = false };
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error setting filter", e);
                }
            }
            else if (search_type.Equals("other"))
            {
                try
                {
                    List<FilterDTO> filterList = getFilterList("FILTERD");
                    olp.Filter = parseFilterConstants(filterList[0].Filter);

                    if (filterList[0].UseCache == true)
                    {
                        olp.Cache = new CacheParam { CacheId = user.GisToken + "_other", ReadFrom = true, UpdateTo = false };
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Error setting filter", e);
                }
            }

            olp.Index = 1;
            olp.IndexFilter = "*";
            olp.IndexCount = int.Parse(mGeneralIndexSearchCount);
            olp.OrderSerie = "A";
            olp.IncludeDeliveredRows = false;
            olp.WithRows = true;
            olp.MaxCount = 100;
            olp.ReverseReading = true;
            olp.IncludeProduct = true;

            try
            {
                List<FilterDTO> filterList = getFilterList("FILTERA");
                olp.AdditionalFilter = parseFilterConstants(filterList[0].Filter);
            }
            catch (Exception e)
            {
                logger.Error("Error setting filter", e);
            }

            List<OrderHead> temp = new List<OrderHead>();

            // Additionlorders or ordinary search
            if (search_type.Equals("customer"))
            {
                olp.Index = 2;
                olp.IndexFilter = search_value.PadRight(10) + ";" + search_value.PadRight(10);
                //olp.IndexFilter = search_value;
                olp.Filter = olp.AdditionalFilter;
                olp.IndexCount = 0;

                lst = mLOM.GetOrderListWithAdditionalOrder(olp);

                // Mafi unique, filter out order depening on next deliverdate
                List<OrderHead> tempAdditional = new List<OrderHead>();
                foreach (OrderHead oh in lst)
                {
                   

                    if (oh.OrderRows.Count > 0)
                    {
                        OrderRow orDelDate = oh.OrderRows.Where(f => f.DeliverState != "5").OrderBy(r => r.PreferedDeliverDate)?.First();

                        if (orDelDate != null)
                        {
                            if (orDelDate.OrderNo.Trim() == oh.OrderNo.Trim())
                            {
                                oh.PreferedDeliverDate = orDelDate.PreferedDeliverDate;             //Skall vi ändra datumformat igen?
                            }
                        }
                    }

                    // search_value 2 == original orders PreferedDeliverDate
                    if (oh.PreferedDeliverDate == search_value2)
                    {
                        temp.Add(oh);
                    }
                }
            }
            else
            {
                lst = mLOM.GetOrderListWithAdditionalOrder(olp);
                BaseTable btWH = mBDM.GetBaseTable("WELOGG", "WH");
                if (lst != null)
                {
                    foreach (OrderHead oh in lst)
                    {
                        if (oh.OrderNo == "144328")
                        {
                            Console.WriteLine();

                        }
                        // Mark order that has materialplanned rows
                        oh.HasMaterialPlannedRows = (oh.OrderRows.Where(r => r.MTF.CompareTo("0") > 0)?.ToList().Count > 0);

                        oh.IsWholeOrderDeliverable = true;
                        oh.NonDeliverableRows = 0;

                        try
                        {
                            oh.OrderRows.RemoveAll(r => string.IsNullOrEmpty(r.ProductNo));
                        }
                        catch (Exception e)
                        {
                            logger.Debug("Error : " + e.Message);
                        }

                        decimal amountTvk = 0;
                        foreach (OrderRow or in oh.OrderRows)
                        {


                            if (btWH?.Id == "WH")
                            {
                                if (btWH.Code1 == "T") // Warehouse accoring to Description is mandatory
                                {
                                    // If OR warehouse not match madatory warehouse from settings
                                    if (or.WarehouseNo.Trim() != btWH.Description.Trim())
                                    {
                                        oh.NonDeliverableRows++;
                                        oh.IsWholeOrderDeliverable = false;

                                    }
                                    // If warehouse not have enough amount to supplie this row
                                    else if (or.Product?.WarehouseList?.Find(w => w.Id == btWH.Description.Trim())?.Stock < or.Amount.Value && or.Product.StockUpdateType != "9")
                                    {
                                        oh.NonDeliverableRows++;
                                        oh.IsWholeOrderDeliverable = false;
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        // If warehouse not have enough amount to supplie this row
                                        if (or.Product.WarehouseList.Find(w => w.Id == btWH.Description.Trim())?.Stock < or.Amount.Value && or.Product.StockUpdateType != "9")
                                        {
                                            oh.NonDeliverableRows++;
                                            oh.IsWholeOrderDeliverable = false;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        logger.Error("Error checking stock on warehouse, order and row was " + or.OrderNo + "-" + or.RowNo, e);
                                    }
                                }
                            }
                            else
                            {
                                if (or.Product != null)
                                {
                                    if (or.Amount.Value <= or.Product.Stock || or.Product.StockUpdateType == "9" || or.MTF.CompareTo("0") > 0)
                                        oh.IsWholeOrderDeliverable = true;
                                    else
                                    {
                                        oh.IsWholeOrderDeliverable = false;
                                        oh.NonDeliverableRows++;
                                    }

                                    if (or.Product.OriginType?.Equals("3") == true)
                                    {
                                        amountTvk += or.Amount.Value;
                                    }
                                }
                                else
                                {
                                    oh.NonDeliverableRows++;
                                }
                            }
                        }

                        try
                        {
                            OrderRow orDelDate = oh.OrderRows.Where(f => f.DeliverState != "5").OrderBy(r => r.PreferedDeliverDate).First();
                            //string s = Newtonsoft.Json.JsonConvert.SerializeObject(orDelDate);
                            //logger.Debug("Row with first deliverdate (JSON): " + s);

                            if (orDelDate != null)
                            {
                                if (orDelDate.OrderNo.Trim() == oh.OrderNo.Trim())
                                {
                                    oh.PreferedDeliverDate = orDelDate.PreferedDeliverDate;             //Skall vi ändra datumformat igen?
                                }
                            }

                        }
                        catch (Exception e)
                        {
                            logger.Debug("Error : " + e.Message);
                        }

                        if (amountTvk > 100)
                        {
                            oh.isLargeOrder = true;
                        }
                        else
                        {
                            oh.isLargeOrder = false;
                        }

                        //if NonDeliverableRows and total orderrows is the same (like orderrows with "wrong" warehouseno), we drop this order.
                        if (oh.OrderRows.Count == oh.NonDeliverableRows && oh.OrderRows.Count > 0)
                        {
                            if (btWH != null)
                            {
                                if (oh.OrderRows[0].WarehouseNo != btWH.Id)
                                {
                                    continue;
                                }
                            }
                        }

                        if (search_type.Equals("customer"))         // To check on additionalOrders so DeliverAddress match the original order, otherwise we drop this...
                        {
                            if (ohda.DeliverAddress1 != oh.DeliverAddress1)
                            {
                                continue;
                            }
                        }

                        if (oh.OrderNo == "135460")
                        {
                            Console.WriteLine();

                        }
                        // ONly add if any deliverble row exists
                        if (oh?.OrderRows.Count(r => r.RowNo <= 250 && r.DeliverState != "5") > 0)
                        {
                            temp.Add(oh);
                        }

                    }
                    //temp = CheckTodayOrders(lst);

                }

                // Mafi unique AdditionalOrderCheck
                foreach (OrderHead oh in temp)
                {
                    try
                    {
                        int count = lst.Count(o => o.DeliverCustomerNo == oh.DeliverCustomerNo && o.PreferedDeliverDate == oh.PreferedDeliverDate);

                        if (count > 1)
                        {
                            oh.AdditionalOrders = true;
                        }
                        else
                        {
                            oh.AdditionalOrders = false;
                        }
                    }
                    catch { }
                }
            }


            //ViewBag.additionalOrders = additionalLst;
            ViewBag.search_value = search_value;
            ViewBag.search_type = search_type;

            // Sortorder of picklist
            List<OrderHead> sortedList = temp.OrderBy(o => o.PreferedDeliverDate).ThenBy(o => o.OrderNo).ToList();

            return View(sortedList);
        }




        private bool CheckPrioOrder(string orderid)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            bool isPrio = false;
            List<OrderRowText> result = mOM.GetOrderRowTextList(orderid, "0");
            if (result.Count > 0)
            {
                string txt = result.FirstOrDefault().Text;
                if (!string.IsNullOrWhiteSpace(txt))
                {
                    if (txt.Substring(0, 1).Equals("#"))
                        isPrio = true;
                }
            }

            return isPrio;


        }


        [Authorize]
        public IActionResult GetStartedOrderList()
        {
            List<OrderHead> lst = new List<OrderHead>();
            
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);


            string filter = "LEF<=4;PLF==P;FAF!=5;OTY==1;BLT>=" + DateTime.Now.AddMonths(-5).ToString("yyMMdd");
            lst = mOM.GetOrderPickList("1", "*", mGeneralIndexSearchCount, filter, "A", true, true, 100, true);

            List<OrderHead> temp = new List<OrderHead>();

            foreach (OrderHead oh in lst)
            {
                if (oh.OrderRows.Count > 0)
                {
                    BaseTable ls = mBDM.GetBaseTable("03", oh.DeliverWay);
                    if (ls != null)
                        oh.DeliverWay = ls.Description;

                    temp.Add(oh);
                }
            }

            List<OrderHead> sortedList = temp.OrderByDescending(o => o.OrderNo).ToList();

            return View(sortedList);
        }

        [Authorize]
        public ActionResult GetDeliveredOrderWithoutTransport()
        {
            List<OrderHead> lst = new List<OrderHead>();
            
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            string filter = "PLF==5;FAF!=5;OTY==1;LSE<=89;BLT>=" + DateTime.Now.AddDays(-7).ToString("yyMMdd");
            lst = mOM.GetOrderForDeliverNoteList("*", 1000, filter, 50, true);

            List<OrderHead> temp = new List<OrderHead>();

            //foreach (OrderHead oh in lst)
            //{
            //    if (oh.OrderRows.Count > 0)
            //    {
            //        BaseTable ls = mBDM.GetBaseTable("", "03", oh.DeliverWay);
            //        if (ls != null)
            //            oh.DeliverWay = ls.Description;

            //        temp.Add(oh);
            //    }
            //}

            List<OrderHead> sortedList = new List<OrderHead>();

            if (lst != null)
                sortedList = lst.OrderByDescending(o => o.OrderNo).ToList();

            return View(sortedList);
        }

        [Authorize]
        public IActionResult Details(string id)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
            CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);
            ManufacturingModel mMM = new ManufacturingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<OrderHead> lst = mOM.GetOrderPickList("1", id, "0", "*", "A", true, false, 1, false);
            OrderHead oh = null;
            Dictionary<string, List<MaterialRow>> materials = new Dictionary<string, List<MaterialRow>>();

            try
            {

                if (lst.Count > 0)
                    oh = lst[0];
                else
                    oh = null;

                if (oh != null)
                {
                    BaseTable ls = mBDM.GetBaseTable("03", oh.DeliverWay);
                    if (ls != null)
                        oh.DeliverWay = ls.Description;

                    BaseTable lv = mBDM.GetBaseTable("02", oh.DeliverTerms);
                    if (lv != null)
                        oh.DeliverTermsDescription = lv.Description;

                    Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);

                    ViewBag.Customer = customer;

                }

                if (oh?.OrderRows == null)
                    oh.OrderRows = new List<OrderRow>();

                // Remove all rows without product
                oh.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

                //Remove all rows with products in "wrong" Warehouseno if "UseOnlyThis" is checked.
                BaseTable btWH = mBDM.GetBaseTable("WELOGG", "WH");


                // Get warehouse stock and set on product level
                if (btWH?.Id == "WH")
                {
                    string WarehouseNo = btWH?.Description.Trim();
                    bool UseOnlyThis = btWH.Code1.Equals("T") ? true : false;

                    if (UseOnlyThis)
                    {
                        oh.OrderRows.RemoveAll(o => (o.WarehouseNo != WarehouseNo));
                    }

                    foreach (OrderRow or in oh.OrderRows)
                    {
                        decimal? wh = or.Product.WarehouseList.Find(w => w.Id == btWH.Description.Trim())?.Stock;

                        if (wh.HasValue)
                        {
                            or.Product.Stock = wh.Value;
                        }
                        else
                        {
                            or.Product.Stock = 0;
                        }

                        if (or.Product.StockUpdateType.CompareTo("5") >= 0)
                        {
                            or.AmountToDeliver = or.Amount - or.DeliveredAmount;
                        }
                    }
                }

                foreach (OrderRow or in oh.OrderRows)
                {
                    if (or.MDF.Equals("1") || or.MDF.Equals("2"))
                    {
                        List<MaterialRow> mr = mMM.getMaterialRowList(or.OrderNo, or.RowNo.ToString());
                        materials.Add(or.OrderNo + or.RowNo.ToString(), mr);
                    }
                }

                logger.Debug("Details materials :" + materials.Count);
                ViewBag.Materials = materials;

                //var sortedRows = (from OrderRow r in oh.OrderRows
                //                  orderby r.Product.WarehouseNo ascending
                //                  select r).ToList();
                // oh.OrderRows = sortedRows;

                List<OrderRow> sortedList = oh.OrderRows.OrderBy(o => o.PreferedDeliverDate)
                                .ThenBy(o => o.Product.Code1)
                                .ThenBy(o => o.RowNo)
                                .ToList();

                oh.OrderRows = sortedList;
                oh.isPrioOrder = CheckPrioOrder(oh.OrderNo);

                return View(oh);
            }
            catch (Exception e)
            {
                logger.Debug("Error in Details" + e.Message);

                return View();
            }
        }

        [Authorize]
        public IActionResult HistoricDetails(string onr, string delnote)
        {

            OrderHead oh;
            OrderHead plainOH = null;

            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
                GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);
                CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);

                oh = mOM.GetOrderForDeliverNote(delnote);

                plainOH = mOM.GetOrderById(oh.OrderNo, false);

                // If nor delivernote was sent (happens sometimes, if only one row is delivered as an example. We then need to get lastdelivernote from OH
                if (string.IsNullOrEmpty(delnote))
                {
                    delnote = plainOH.LastDeliverNote;
                }

                // If oh is null, then delivernote really dont exits
                if (oh != null)
                {
                    Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);
                    ViewBag.Customer = customer;
                    oh.OrderRows = oh.OrderRows == null ? new List<OrderRow>() : oh.OrderRows;

                   
                    return View(oh);
                }
                else
                {
                    return RedirectToAction("PickOrder", new { id = oh.OrderNo, sign = oh.Season });
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("PickOrder", new { id = onr, sign = plainOH?.Season });
            }
        }


        [Authorize]
        public IActionResult PickOrder(string id, string? sign, bool force, bool isFutureOrder)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
            GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);
            CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);

            WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
            Babc_logistic_core.GIS.Model.ProductModel mPRM = new Babc_logistic_core.GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);
            //var warehouseList = FindBatch(id, "2210616", "2210616");
            List<OrderHead> lst = mOM.GetOrderPickList("1", id, "0", "*", "A", true, true, 1, false);
            OrderViewModel ovm = new OrderViewModel();

            ovm.Rows = new List<CompactOrderRow>();
            ovm.Order = new OrderHead();
            if(isFutureOrder==null)
            {
                isFutureOrder= false;
            }
            ViewBag.isFutureOrder = isFutureOrder.ToString();
            ViewBag.sign = sign;
          
            try
            {
                if (lst.Count > 0)
                {
                    ovm.Order = lst[0];
                    if(ovm.Order.PickListState=="F")
                    {
                        isFutureOrder= true;
                    }
                    ViewBag.CustomerInfo = GetCustomerInfo(ovm.Order.DeliverCustomerNo);
                    if (string.IsNullOrEmpty(ovm.Order.Season) || ovm.Order.Season == sign || force == true)
                    {
                        sign = sign?.ToUpper();
                        // Check if it is FutureOrder
                        if (isFutureOrder)
                        {
                            // Set signature and mark order as "under pick, future order"
                            ovm.Order.PickListState = "F";
                        }
                        else
                        {
                            // Set signature and mark order as "under pick"
                            ovm.Order.PickListState = "P";
                        }
                        ovm.Order.Season = sign;
                        mOM.UpdateOrderHead(ovm.Order);

                        // Add orderrow with signature, if th row not exists yet, and signature exists as a product.
                        if (ovm.Order.OrderRows.Find(s => s.ProductNo == sign) == null)
                        {
                            Product signProduct = mPRM.GetProductById(sign);
                          
                            try
                            {
                                if (!string.IsNullOrEmpty(signProduct?.ProductNo))
                                {
                                    List<OrderRow> orlist = new List<OrderRow>();
                                    orlist.Add(new OrderRow
                                    {
                                        ProductNo = sign,
                                        Amount = 0,
                                        OrderNo = ovm.Order.OrderNo,
                                        RowNo = 255,
                                        WarehouseNo = "1"
                                    });

                                    mOM.UpdateOrderRowList(orlist);
                                    ovm.Order.OrderRows = mOM.GetOrderRowByOrderIdList(ovm.Order.OrderNo);
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Error("Error creating kst row with signature", e);
                            }
                        }

                        BaseTable ls = mBDM.GetBaseTable("03", ovm.Order.DeliverWay);
                        if (ls != null)
                            ovm.Order.DeliverWay = ls.Description;

                        BaseTable lv = mBDM.GetBaseTable("02", ovm.Order.DeliverTerms);
                        if (lv != null)
                            ovm.Order.DeliverTermsDescription = lv.Description;

                        // Logtexter
                        var logList = mBDM.GetBaseTableList("LOGTEXT");
                        if (logList != null)
                        {
                            ovm.LogTexts = new List<string>();

                            foreach (var log in logList)
                            {
                                ovm.LogTexts.Add(log.Description);
                            }
                        }

                        // Remove all rows without product
                        ovm.Order.OrderRows.RemoveAll(o => string.IsNullOrEmpty(o.ProductNo));

                        try
                        {
                            var sortedRows = (from OrderRow r in ovm.Order.OrderRows
                                              orderby r.Product.WarehouseNo ascending
                                              select r).ToList();

                            ovm.Order.OrderRows = sortedRows;
                        }
                        catch (Exception e) { }

                        //hämta kundfält för PS info
                        Customer customer = mCM.GetCustomerById(ovm.Order.DeliverCustomerNo);
                        try
                        {
                            ViewBag.Customer = customer;
                            //oh.isPrioOrder = CheckPrioOrder(oh.OrderNo);
                        }
                        catch (Exception e)
                        {
                            logger.Error("ViewbagError", e);

                        }

                        List<FilterDTO> filterList = getFilterList("FILTERE");
                        if (filterList?.Count > 0)
                        {
                            ovm.SelectableProducts = mPRM.GetProductList("1", "*", filterList[0].Filter);
                        }
                        else
                        {
                            ovm.SelectableProducts = new List<Product>();
                        }

                        List<OrderRow> tempOr = new List<OrderRow>();
                        foreach (OrderRow or in ovm.Order.OrderRows)
                        {
                            tempOr.Add(or);

                            if (or.Product.StockUpdateType.CompareTo("5") >= 0)
                            {
                                or.AmountToDeliver = or.Amount - or.DeliveredAmount;
                            }

                            // Check if product is manually added or not
                            if (ovm.SelectableProducts?.Find(p => p.ProductNo == or.ProductNo) != null)
                            {
                                or.ManuallyAdded = true;
                            }
                            else
                            {
                                or.ManuallyAdded = false;
                            }

                            try
                            {
                                // Check if logtext is added today or not
                                var deliverText = or.Textrows.Find(t => t.InvoiceState == "L" && t.Text.Contains("(" + DateTime.Today.ToString("yyMMdd") + $"/"));
                                if (deliverText != null)
                                {
                                    var text = or.Textrows.Last(t => t.InvoiceState == "K");
                                    or.DeliverLogText = text?.Text;
                                }
                                else
                                {
                                    or.DeliverLogText = "";
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }

                        List<OrderRow> sortedList = tempOr.OrderBy(o => o.PreferedDeliverDate)
                            .ThenBy(o => o.Product.Code1)
                            .ThenBy(o => o.RowNo)
                            .ToList();

                        //ovm.Order.OrderRows = tempOr;
                        ovm.Order.OrderRows = sortedList;

                        // Find any comment manually added
                        var logComment = ovm.Order.OrderRows.Find(or => or.Textrows.Find(t => t.DeliverNoteState == "K" && t.InvoiceState == "I") != null);

                        // Check if row has been delivered today, to se if we should show logtext window
                        var logAmount = ovm.Order.OrderRows.Find(or => or.Textrows.Find(t => t.DeliverNoteState == "S" && t.InvoiceState == "I" && t.Text.Substring(t.Text.IndexOf("LDat:") + 6, 6) == DateTime.Now.ToString("yyMMdd")) != null);


                        // *** CHECK EARLY DELIVER ****
                        bool earlyDeliverAllowed = false;
                        try
                        {
                            var ogc = mGTM.GetTableValueByIndex(new GetTableValueRequest
                            {
                                Field = "CH7",
                                Index = 1,
                                Key = ovm.Order.OrderNo,
                                TableName = "OGC"
                            });

                            if (ogc.Result.FieldValue.Substring(6, 1) == "J")
                            {
                                earlyDeliverAllowed = true;
                            }

                        }
                        catch { }

                        if (logComment != null && logAmount != null)
                        {
                            ovm.LogtextAddedToday = true;
                        }
                        else
                        {
                            if (GeneralFunctions.getDateFromStr(ovm.Order.PreferedDeliverDate) > DateTime.Now && earlyDeliverAllowed)
                            {
                                ovm.LogtextAddedToday = true;
                            }
                            else
                            {
                                ovm.LogtextAddedToday = false;
                            }

                        }

                        //*** END EARLY DELIVER ***

                        foreach (OrderRow or in ovm.Order.OrderRows)
                        {
                            ovm.Rows.Add(getCompactOrderRow(or, customer.Code_3, customer.CustomerNo, ovm.Order.Text2));
                        }
                    }
                    else
                    {
                        return RedirectToAction("OrderIsAlreadyUnderPicking", "Order", new { order = ovm.Order.OrderNo, sign = ovm.Order.Season, forwardpath = "/Order/PickOrder?id=" + ovm.Order.OrderNo + "&sign=" + sign + "&force=true", backpath = "/Order/PickList" });
                    }
                }
                else
                    ovm.Order = null;
            }
            catch (Exception e)
            {
                logger.Error("Error in PickOrder", e);
            }



            return View(ovm);
        }



 

        [Authorize]
        public string GetCustomerInfo(string id)
        {
            string result = "";
            try
            {
              CustomerModel C = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);
              string[] customerinfo = C.GetCustomerInfo(id);
                StringBuilder sb = new StringBuilder();
                foreach(string s in customerinfo)
                    {
                        if (s != "[]")
                        {
                            
                            sb.Append(s.Substring(1, s.Length - 2));
                            sb.Replace("\"", "");
                            sb.Replace(",", " ");

                    }
                    }
                result = sb.ToString();
            }

            catch (Exception e)
            {
               
            }
            ViewBag.CustomerInfo = result;
            return result;
        }

            [Authorize]
        public IActionResult CancelPickOrder(string onr, string? sign)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            List<OrderHead> lst = mOM.GetOrderPickList("1", onr, "0", "*", "A", true, false, 1, false);
            OrderHead oh = null;
            Babc_logistic_core.GIS.Model.ProductModel mPRM = new Babc_logistic_core.GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

            if (lst.Count > 0)
            {
                oh = lst[0];
                oh.PickListState = "1";
       

                if (oh.OrderRows.Exists(s => s.ProductNo == sign))
                {
                    Product signProduct = mPRM.GetProductById(sign);

                    try
                    {
                        if (!string.IsNullOrEmpty(signProduct?.ProductNo))
                        {

                            mOM.DeleteOrderRow(oh.OrderRows.Find(x => x.ProductNo == sign));
                  
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("Error deleting kst row with signature", e);
                    }
                }

                
                mOM.UpdateOrderHead(oh);
            }
            else
                oh = null;

            return RedirectToAction("Picklist", new { search_type = "supplie", search_value = GeneralFunctions.getStrFromDate(DateTime.Now) });
        }


        //[HttpPost]
        //public ActionResult DeliverOrder(List<OrderRow> rows)
        //{
        //    List<DeliverRowParam> deliver = new List<DeliverRowParam>();
        //    //OrderRow or = mOM.GetOrderRow(order, row);
        //    string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        //    decimal? dAmount = null;

        //    try
        //    {
        //        if (decSep == ".")
        //        {
        //            dAmount = decimal.Parse(amount.Replace(",", "."));
        //        }
        //        else
        //        {
        //            dAmount = decimal.Parse(amount.Replace(".", ","));
        //        }
        //    }
        //    catch { }

        //    try
        //    {
        //        if (dAmount.HasValue)
        //        {
        //            if (dAmount.Value != 0)
        //            {
        //                //or.DeliverState = state;
        //                //if (dAmount.HasValue)
        //                //    or.AmountToDeliver = dAmount.Value;

        //                DeliverRowParam param = new DeliverRowParam()
        //                {
        //                    AmountToDeliver = dAmount.Value,
        //                    DeliverDate = "",
        //                    DeliverMaterialRow = false,
        //                    DeliverNote = "",
        //                    DeliverState = state,
        //                    HandleOperationRow = false,
        //                    OrderNo = order,
        //                    RowNo = int.Parse(row),
        //                    WarehouseNo = ""
        //                };

        //                deliver.Add(param);
        //            }
        //            else
        //            {
        //                if (state == "5")
        //                {
        //                    DeliverRowParam param = new DeliverRowParam()
        //                    {
        //                        AmountToDeliver = dAmount.Value,
        //                        DeliverDate = "",
        //                        DeliverMaterialRow = false,
        //                        DeliverNote = "",
        //                        DeliverState = state,
        //                        HandleOperationRow = false,
        //                        OrderNo = order,
        //                        RowNo = int.Parse(row),
        //                        WarehouseNo = ""
        //                    };

        //                    deliver.Add(param);
        //                }
        //                else
        //                {
        //                    return "FAILED";
        //                }
        //            }

        //            DeliverResult result = mOM.Deliver(deliver);

        //            if (result.Succeeded)
        //            {
        //                return result.DeliverNo;
        //            }
        //            else
        //            {
        //                return "FAILED - " + result.InternalDeliverMessage;
        //            }
        //        }
        //        else
        //        {
        //            return "FAILED";
        //        }
        //    }
        //    catch
        //    {
        //        return "FAILED";
        //    }
        //}
     
        [Authorize]
        public IActionResult ViewDeliveredOrder(string delnote, string onr)
        {
            OrderHead oh;
            OrderHead plainOH = null;

            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
                GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);
                CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);

                oh = mOM.GetOrderForDeliverNote(delnote);

                plainOH = mOM.GetOrderById(oh.OrderNo, false);

                // If nor delivernote was sent (happens sometimes, if only one row is delivered as an example. We then need to get lastdelivernote from OH
                if (string.IsNullOrEmpty(delnote))
                {
                    delnote = plainOH.LastDeliverNote;
                }

                // If oh is null, then delivernote really dont exits
                if (oh != null)
                {
                    Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);
                    ViewBag.Customer = customer;
                    oh.OrderRows = oh.OrderRows == null ? new List<OrderRow>() : oh.OrderRows;

                    UpdateTableIndexObject update = new UpdateTableIndexObject();

                    // ** OM INTE BET.VILLKOR HAR V FLAGGA ***
                    BaseTable btPayment = mBDM.GetBaseTable("01", plainOH.PaymentTerms);

                    // Change invoice flag ONLY if paymentterms invoiceflag is NOT "V" and PickState is NOT "F" (F is set for pre-pick/future orders)
                    if (btPayment?.Code2.ToUpper().CompareTo("V") != 0 && oh.PickListState != "F")
                    {
                        update.TableName = "HKA";
                        update.Key = delnote.PadRight(6) + "  1";
                        update.IndexFields = new List<FieldObject> { new FieldObject { FieldName = "HNR", FieldValue = delnote } };
                        update.Fields = new List<FieldObject> { new FieldObject { FieldName = "FAF", FieldValue = "1" } };
                        mGTM.UpdateTableByIndex(update, "false");
                    }

                    // If picklist state is P (ongoing) then we change it back to not ongoing
                    if (plainOH.PickListState == "P")
                    {
                        plainOH.PickListState = "1";
                        mOM.UpdateOrderHead(plainOH);
                    }

                    return View(oh);
                }
                else
                {
                    return RedirectToAction("PickOrder", new { id = oh.OrderNo, sign = oh.Season });
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("PickOrder", new { id = onr, sign = plainOH?.Season });
            }
        }

        [Authorize]
        public IActionResult PrintDeliverNote(string id)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
                BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);
                GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);
                CustomerModel mCM = new CustomerModel(_Config, _userManager.GetUserAsync(User)?.Result);

                OrderHead oh = mOM.GetOrderForDeliverNote(id);

                // If oh is null, then delivernote really dont exits
                if (!string.IsNullOrEmpty(oh?.OrderNo))
                {
                    OrderHead plainOH = mOM.GetOrderById(oh.OrderNo, false);
                    Customer customer = mCM.GetCustomerById(oh.DeliverCustomerNo);
                    ViewBag.Customer = customer;

                    // ** OM INTE BET.VILLKOR HAR V FLAGGA ***
                    BaseTable btPayment = mBDM.GetBaseTable("01", plainOH?.PaymentTerms);

                    // Change invoice flag ONLY if paymentterms invoiceflag is NOT "V" and PickState is NOT "F" (F is set for pre-pick/future orders)
                    if (btPayment?.Code2.ToUpper().CompareTo("V") != 0 && plainOH.PickListState != "F")
                    {
                        UpdateTableIndexObject update = new UpdateTableIndexObject();

                        update.TableName = "HKA";
                        update.Key = id.PadRight(6) + "  1";
                        update.IndexFields = new List<FieldObject> { new FieldObject { FieldName = "HNR", FieldValue = id } };
                        update.Fields = new List<FieldObject> { new FieldObject { FieldName = "FAF", FieldValue = "1" } };
                        mGTM.UpdateTableByIndex(update, "false");
                    }

                    // If picklist state is P (ongoing) then we change it back to not ongoing
                    if (plainOH.PickListState == "P")
                    {
                        OrderHead tmpOH = new OrderHead() { OrderNo = oh.OrderNo, PickListState = "1" };
                        mOM.UpdateOrderHead(tmpOH);
                    }
                }
            }
            catch
            {

            }


            ERPReport report = new ERPReport();
            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);
      
            try
            {
                var user = _userManager.GetUserAsync(User)?.Result;
                report = _Config.GetSection(user.GisToken + "DeliverNote").Get<ERPReport>();
                if (report == null)
                    report = _Config.GetSection("DeliverNote").Get<ERPReport>();
                //report.DocGen = ConfigurationManager.AppSetting["DeliverNote:DocGen"];
                //report.Report = ConfigurationManager.AppSetting["DeliverNote:Report"];
                //report.Medium = ConfigurationManager.AppSetting["DeliverNote:Medium"];
                //report.Form = ConfigurationManager.AppSetting["DeliverNote:Form"];
                //report.FilePath = ConfigurationManager.AppSetting["DeliverNote:FilePath"];
                //report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["DeliverNote:Wait"]);
            }
            catch
            {
                report.DocGen = "296";
                report.Report = "M1";
                report.Medium = "P";
                report.Form = "FS";
                
            }

            try
            {
                
                report.IndexFrom = id;
                report.IndexTo = id;

                mPM.Print(report);
            }
            catch (Exception)
            {

            }

            return RedirectToAction("Picklist", new { search_type = "supplie", search_value = GeneralFunctions.getStrFromDate(DateTime.Now) });
        }


        //public IActionResult PrintLabel00(string orderno, string qty)
        //{

        //    PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

        //    ERPReport report = new ERPReport();

        //    try
        //    {
        //        report = _Config.GetSection("PrintGoodsLabel_LA").Get<ERPReport>();
        //    }
        //    catch
        //    {

        //    }

        //    try
        //    {
        //        report.DocGen = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:DocGen"];
        //        report.Report = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Report"];
        //        report.Medium = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Medium"];
        //        report.Form = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Form"];
        //        report.FilePath = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:FilePath"];
        //        report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Wait"]);
                
               
        //        report.IndexFrom = orderno;
        //        report.IndexTo = orderno;
        //        int i = 0;
        //        report.Dialogs[i].Value = qty;
                
                

        //        i = 0;



        //        mPM.Print(report);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);

        //    }

        //    return Content("OK");
        //}


        public IActionResult PrintGoodsLabelLA(string orderno, string rowno, string printerque, string qtyOfLabels, string qty, string origin, string IncludeItemNo, string company, string sign)
        {

            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            ERPReport report = new ERPReport();

            try
            {
                var user = _userManager.GetUserAsync(User)?.Result;
                report = _Config.GetSection(user.GisToken + "PrintGoodsLabel_LA").Get<ERPReport>();
                if (report == null)
                       report = _Config.GetSection("PrintGoodsLabel_LA").Get<ERPReport>();
            }
            catch
            {

            }

            try
            {
                //report.DocGen = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:DocGen"];
                //report.Report = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Report"];
                //report.Medium = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Medium"];
                //report.Form = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Form"];
                //report.FilePath = ConfigurationManager.AppSetting["PrintGoodsLabel_LA:FilePath"];
                //report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["PrintGoodsLabel_LA:Wait"]);

                if(string.IsNullOrEmpty(printerque))
                {
                    printerque = "1";
                }

                report.IndexFrom = orderno;
                report.IndexTo = orderno;
                int i = 0;
                
                if (company == "SE")
                {
                    report.Dialogs[i].Value = printerque;
                    i++;
                }

                report.Dialogs[i].Value = rowno;
                i++;
                report.Dialogs[i].Value = origin;
                i++;
                report.Dialogs[i].Value = qtyOfLabels;
                i++;
                report.Dialogs[i].Value = qty;
                i++;
                report.Dialogs[i].Value = IncludeItemNo;

                i = 0;



                mPM.Print(report);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            

            return RedirectToAction("PickOrder", new { id = orderno, sign = sign });
        }
        public IActionResult PrintGoodsMarkCustomer(string orderno, string text2, string customerSlice, string productNo, string qty)
        {

            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            ERPReport report = new ERPReport();
            if(string.IsNullOrEmpty(text2))
            {
                text2 = "";
            }
            try
            {
                var user = _userManager.GetUserAsync(User)?.Result;
                report = _Config.GetSection(user.GisToken + "PrintGoodsMarkCustomer").Get<ERPReport>();
                if (report == null)
                    report = _Config.GetSection("PrintGoodsMarkCustomer").Get<ERPReport>();
            }
            catch
            {

            }

            try
            {
                //report.DocGen = ConfigurationManager.AppSetting["PrintGoodsMarkCustomer:DocGen"];
                //report.Report = ConfigurationManager.AppSetting["PrintGoodsMarkCustomer:Report"];
                //report.Medium = ConfigurationManager.AppSetting["PrintGoodsMarkCustomer:Medium"];
                //report.Form = ConfigurationManager.AppSetting["PrintGoodsMarkCustomer:Form"];
                //report.FilePath = ConfigurationManager.AppSetting["PrintGoodsMarkCustomer:FilePath"];
                //report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["PrintGoodsMarkCustomer:Wait"]);


                report.IndexFrom = productNo;
                report.IndexTo = productNo;
                int i = 0;


                report.Dialogs[i].Value = orderno;
                i++;
                report.Dialogs[i].Value = text2.PadRight(40).Substring(0,20).Trim();
                i++;
                report.Dialogs[i].Value = text2.PadRight(40).Substring(20, 10).Trim();
                i++;
                report.Dialogs[i].Value = customerSlice;
                i++;
                report.Dialogs[i].Value = productNo;
                i++;
                report.Dialogs[i].Value = qty;
                i = 0;



              mPM.Print(report);
            
            return Content("OK");
   
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Content("Error: " + e);

            }
           
        }
        [HttpPost]
        [Authorize]
        public ActionResult DeliverOrder(List<OrderRow> rows)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            DeliverResult result = null;
            if (rows != null)
            {
                List<DeliverRowParam> rowList = new List<DeliverRowParam>();

                foreach (OrderRow or in rows)
                {
                    rowList.Add(new DeliverRowParam { OrderNo = or.OrderNo, RowNo = or.RowNo, AmountToDeliver = or.AmountToDeliver.Value, DeliverMaterialRow = false, HandleOperationRow = false, WarehouseNo = or.WarehouseNo, SetDeliverFlagFromPaymentTerms = true });
                }
                result = mOM.Deliver(rowList);
            }

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public IActionResult DeliverOrderRow(string order, string row, string state, string amount, string warehouseno, string logtext)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<DeliverRowParam> deliver = new List<DeliverRowParam>();

            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            decimal? dAmount = GeneralFunctions.getDecimalFromStr(amount);
            var orderRow = mOM.GetOrderRow(order, row);
            var oh = mOM.GetOrderById(order, false);

            try
            {
                if (dAmount.HasValue)
                {
                    if (dAmount.Value != 0)
                    {
                        DeliverRowParam param = new DeliverRowParam()
                        {
                            AmountToDeliver = dAmount.Value,
                            DeliverDate = "",
                            DeliverMaterialRow = false,
                            DeliverNote = "",
                            DeliverState = state,
                            HandleOperationRow = false,
                            OrderNo = order,
                            RowNo = int.Parse(row),
                            WarehouseNo = warehouseno,
                            SetWarehouseOnOrderrow = false
                        };

                        deliver.Add(param);
                    }
                    else
                    {
                        if (state == "5" || orderRow.Product.ProductType == "/")
                        {
                            DeliverRowParam param = new DeliverRowParam()
                            {
                                AmountToDeliver = dAmount.Value,
                                DeliverDate = "",
                                DeliverMaterialRow = false,
                                DeliverNote = "",
                                DeliverState = state,
                                HandleOperationRow = false,
                                OrderNo = order,
                                RowNo = int.Parse(row),
                                WarehouseNo = warehouseno,
                                SetWarehouseOnOrderrow = false
                            };

                            deliver.Add(param);
                        }
                        else
                        {
                            return Content("# The amount is missing! You must choose an amount greater than 0!");
                        }
                    }

                    DeliverResult result = mOM.Deliver(deliver);

                    if (result.Succeeded)
                    {
                        setReservationCode(order, row);

                        // Check if early deliver is OK with customer (J in OGC:CH7)
                        bool isEarlyDeliverOk = false;
                        if (oh.Freight7 == "J")
                        {
                            if (oh.PreferedDeliverDate.CompareTo(DateTime.Now.ToString("yyMMdd")) < 0)
                            {
                                isEarlyDeliverOk = true;
                            }
                        }

                        updateLogtextOnOrderRow(order, row, GeneralFunctions.getDecimalFromStr(amount), orderRow.PreferedDeliverDate, DateTime.Now.ToString("yyMMdd"), orderRow.PreferedDeliverDate, oh.Season, logtext, isEarlyDeliverOk);

                        try
                        {
                            UpdateTableIndexObject update = new UpdateTableIndexObject();

                            update.TableName = "HKA";
                            update.Key = result.DeliverNo.PadRight(6) + "  1";
                            update.IndexFields = new List<FieldObject> { new FieldObject { FieldName = "HNR", FieldValue = result.DeliverNo } };
                            update.Fields = new List<FieldObject> { new FieldObject { FieldName = "FAF", FieldValue = "V" } };

                            mGTM.UpdateTableByIndex(update, "false");
                        }
                        catch (Exception e)
                        {

                        }

                        OrderRow or = mOM.GetOrderRow(order, row);
                        return ViewComponent("OrderRowSlice", new { or = getCompactOrderRow(or) });
                    }
                    else
                    {
                        return Content("# Error:" + result.InternalDeliverMessage);
                    }
                }
                else
                {
                    return Content("# Amount is missing, ju must choose a amount greater than 0!");
                }
            }
            catch (Exception e)
            {
                return Content("# Error: " + e.Message);
            }

        }

        [HttpPost]
        [Authorize]
        public IActionResult DeleteOrderRow(string order, string row)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            string result = "";

            try
            {
                var delRow = mOM.GetOrderRow(order, row);

                if (delRow != null)
                {
                    var delResult = mOM.DeleteOrderRow(delRow);

                    if (delResult.Succeeded != true)
                    {
                        result = "# Row was not delivered! Message was: " + delResult.ErrorMessage;
                    }

                }
            }
            catch (Exception e)
            {
                result = "# Error: " + e.Message;
            }

            return Content(result);
        }

        [Authorize]
        public IActionResult PickOrderRow(string productno, string onr, string row, string amount, string warehouse)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<DeliverRowParam> deliver = new List<DeliverRowParam>();
            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            decimal dAmount = GeneralFunctions.getDecimalFromStr(amount);

            try
            {
                if (dAmount > 0)
                {
                    //or.DeliverState = state;
                    //if (dAmount.HasValue)
                    //    or.AmountToDeliver = dAmount.Value;

                    DeliverRowParam param = new DeliverRowParam()
                    {
                        AmountToDeliver = dAmount,
                        DeliverDate = "",
                        DeliverMaterialRow = false,
                        DeliverNote = "",
                        HandleOperationRow = false,
                        OrderNo = onr,
                        RowNo = int.Parse(row),
                        WarehouseNo = "",
                        User = ""
                    };

                    deliver.Add(param);
                }

                DeliverResult result = mOM.Deliver(deliver);
            }
            catch
            {

            }

            return RedirectToAction("PickOrder", new { id = onr, sign = "" });
        }

        
   
        public IActionResult FindBatch(string batch, List<DeliverBatchParam> list = null, string onr = "")
        {           
            string result = "";
            DeliverBatchParam deliverBatchParam = new DeliverBatchParam();
            List<DeliverBatchParam> listdeliverbatch = new List<DeliverBatchParam>();
            List<WarehouseNumber> whList = new List<WarehouseNumber>();
            //searchBatch(batch);
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);  
            OrderHead oh = new OrderHead();
            OrderRow or = new OrderRow();
            decimal dQtyPicked = 0;
            decimal dBatchQtyPicked = 0;
            WarehouseNumber batchwarehouse = new WarehouseNumber();
            StringBuilder sb = new StringBuilder();
            string tempCookieJson = null;
            try
            {
                if(list!=null && list.Count()>0)
                {
                    onr = list[0].OrderNo;
                }
                oh = mOM.GetOrderById(onr, true);
                if (oh != null && oh?.OrderRows.Count()>0)
                {
                    foreach (OrderRow row in oh.OrderRows)
                    {
                        if (row.Product?.WarehouseList != null || row.Product?.WarehouseList.Count() > 0)
                        {
                            foreach (var wh in row.Product?.WarehouseList)
                            {
                                if (wh.Stock > 0)
                                {
                                    whList.Add(wh);
                                }
                            }
                        }
                    }              
                    batchwarehouse = whList.Find(x => x.Id == batch);
                    if (batchwarehouse != null)
                    {
                        if (oh.OrderRows.Exists(x => x?.ProductNo == batchwarehouse.ProductNo && x?.DeliverState!="5"))
                        {
                            or = oh?.OrderRows.Find(x => x?.ProductNo == batchwarehouse.ProductNo && x?.DeliverState != "5");
                            
                        }
                        else
                        {
                            return Content("Error, could not find matching Row or Product");
                        }
                        if (list == null || list.Count() == 0)
                        {
                            if (HttpContext.Request.Cookies.ContainsKey(onr + or.RowNo + "-JSON"))
                            {
                                tempCookieJson = HttpContext.Request.Cookies[onr + or.RowNo + "-JSON"];
                                listdeliverbatch = JsonConvert.DeserializeObject<List<DeliverBatchParam>>(tempCookieJson);
                            }
                        }
                        else
                        {
                            listdeliverbatch = list;
                        }
                        decimal picked = 0;
                        foreach(var delbatch in listdeliverbatch)
                        {
                            if(delbatch.Qty>(delbatch.RowQty-picked))
                            {
                                delbatch.Qty = delbatch.RowQty-picked; 
                            }
                            picked += delbatch.Qty;
                        }
                        dQtyPicked = listdeliverbatch.Where(x => x.Product == or.ProductNo).Sum(x => x.Qty);
                        dBatchQtyPicked = listdeliverbatch.Where(x => x.Product == or.ProductNo && x.Batch==batch).Sum(x => x.Qty);

                        decimal dAmount = 0;
                        decimal rowAmount = 0;
                        if (or.AmountToDeliver.HasValue)
                        {
                            rowAmount = or.AmountToDeliver.Value;
                        }
                        else
                        {
                            rowAmount = or.Amount.Value;
                        }




                        if (or.AmountToDeliver.HasValue)
                        {

                            if ((batchwarehouse.Stock-dBatchQtyPicked) < (or.AmountToDeliver.Value - dQtyPicked))
                            {
                                dAmount = batchwarehouse.Stock-dBatchQtyPicked;

                            }
                            else
                            {
                                dAmount = (or.AmountToDeliver.Value - dQtyPicked);

                            }
                        }
                        else
                        {
                            if ((batchwarehouse.Stock - dBatchQtyPicked) < (or.Amount.Value - dQtyPicked))
                            {
                                dAmount = batchwarehouse.Stock-dBatchQtyPicked;

                            }
                            else
                            {
                                dAmount = or.Amount.Value - dQtyPicked;

                            }
                        }

                        dAmount = dAmount > 0 ? dAmount : 0;
                        dAmount = dAmount > or.AmountToDeliver.Value ? or.AmountToDeliver.Value : dAmount;

                        if (HttpContext.Request.Cookies.ContainsKey(onr + or.RowNo + "-JSON"))
                        {
                         
                            listdeliverbatch.RemoveAll(x => x.Qty == 0);                     
                            deliverBatchParam.Id = listdeliverbatch.Count();
                            deliverBatchParam.OrderNo = oh.OrderNo;
                            deliverBatchParam.RowNo = or.RowNo.ToString();
                            deliverBatchParam.Batch = batch;
                            deliverBatchParam.BatchStock = batchwarehouse.Stock;
                            deliverBatchParam.Product = or.ProductNo;
                            deliverBatchParam.ProductDescription = or.ProductDescription;
                            deliverBatchParam.RowQty = or.AmountToDeliver.Value;
                            deliverBatchParam.Qty = dAmount;
                            deliverBatchParam.Rest = (or.AmountToDeliver.Value - dAmount) > 0 ? or.AmountToDeliver.Value - dAmount : 0 ;
                            listdeliverbatch.Add(deliverBatchParam);

          
                           
                            int i = 0;
                            foreach (var delbatch in listdeliverbatch)
                            {
                                delbatch.Id = i;
                                i++;
                            }
                            var json = JsonConvert.SerializeObject(listdeliverbatch);
                            CookieOptions cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddMinutes(60);
                            HttpContext.Response.Cookies.Append(onr + or.RowNo + "-JSON", json, cookieOptions);

                        }
                        else
                        {
                            dAmount = dAmount > 0 ? dAmount : 0;
                            dAmount = dAmount > or.AmountToDeliver.Value ? or.AmountToDeliver.Value : dAmount;

                            listdeliverbatch.RemoveAll(x => x.Qty == 0);
                            deliverBatchParam.Id = 0;
                            deliverBatchParam.OrderNo = oh.OrderNo;
                            deliverBatchParam.RowNo = or.RowNo.ToString();
                            deliverBatchParam.Batch = batch;
                            deliverBatchParam.BatchStock = batchwarehouse.Stock;
                            deliverBatchParam.Product = or.ProductNo;
                            deliverBatchParam.ProductDescription = or.ProductDescription;
                            deliverBatchParam.RowQty = or.AmountToDeliver.Value;
                            deliverBatchParam.Qty = dAmount;
                            deliverBatchParam.Rest = (or.AmountToDeliver.Value - dAmount) > 0 ? (or.AmountToDeliver.Value - dAmount) : 0;
                            listdeliverbatch.Add(deliverBatchParam);

                         
                            int i = 0;
                            foreach (var delbatch in listdeliverbatch)
                            {
                                delbatch.Id = i;
                                i++;
                            }
                            var json = JsonConvert.SerializeObject(listdeliverbatch);
                            CookieOptions cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddMinutes(60);
                            HttpContext.Response.Cookies.Append(onr + or.RowNo + "-JSON", json, cookieOptions);


                        }

                        string resultJson = buildHTML(listdeliverbatch);
                        result = resultJson;
                    }
                    else
                    {
                        return Content("Error, could not find any batch for that value");
                    }
                }
                else
                {
                    return Content("Error, could not find any records for that value");
                }
            }
            catch (Exception e)
            {
                return Content("Error: " + e);
            }
          //Response.Cookies.Delete(onr + or.RowNo);
            return Content(result);
        }


        public IActionResult searchBatch(string batch)
        {
            string result = "";
            WarehouseNumber warehouseNumber = new WarehouseNumber();
            try
            {
                WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
                warehouseNumber = mWM.GetBatch(batch, "*");
                if (warehouseNumber != null)
                {
                    ProductLocationParam param = new ProductLocationParam();
                    param.GeneralFilter = "*";
                    param.IndexFilter = warehouseNumber.ProductNo;
                    param.StockOnly = false;
                    //var list = mWM.GetWarehouseLocationList("*", false);
                }
            }
            catch (Exception e)
            {


            }

            return Content(result);
        }
        public IActionResult openBatchPickList(string onr, string rowNo)
        {
            string result = "";
            List<DeliverBatchParam> listdeliverbatch = new List<DeliverBatchParam>();
            string tempCookieJson = "";
            try
            {

                if (HttpContext.Request.Cookies.ContainsKey(onr + rowNo + "-JSON"))
                {
                    tempCookieJson = HttpContext.Request.Cookies[onr + rowNo + "-JSON"];
                    listdeliverbatch = JsonConvert.DeserializeObject<List<DeliverBatchParam>>(tempCookieJson);
                    if (listdeliverbatch.Count > 0)
                    {
                        result = buildHTML(listdeliverbatch);
                    }
                    else
                    {
                        result = "No batches has been added yet.";
                    }
                }
                else
                {
                    result = "Couldn't find any started batch picklist";
                }
               
            }
            catch (Exception e)
            {


            }


            return Content(result);
        }
        public void AddBatch(List<DeliverBatchParam> list, string batch)
        {

            FindBatch(batch, list);
        
        }
        public string buildHTML(List<DeliverBatchParam> list)
        {
            string result = "";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Div class='logistics-page-header' style='font-weight:bold'>Order: " + list[0].OrderNo + "-" + list[0].RowNo + " Product: " + list[0].Product + " " + list[0].ProductDescription + " Qty: " + list[0].RowQty + "</div>");
            sb.AppendLine("<table class='table table-responsive-md'><tr><th width='120'><input type='hidden' id='Row' value='" + list[0].RowNo +"' Batch</th><th width='120'>Batch Qty</th><th width='120'>Qty to Pick</th><th width='120'>Qty Rest</th></tr>");
            decimal alreadyPicked = 0;
            foreach (var row in list)
            {
                string styleInput = "";
                if(row.Qty==0)
                {
                    styleInput = ";background-color:yellow";
                }
                sb.AppendLine("<tr id='batchrow[" + row.Id + "]'>");
                sb.Append("<td>" + row.Batch + "</td>");
                sb.Append("<td>" + row.BatchStock + "</td><td>");
                sb.Append("<input type='text' class='form-control' asp-for='Qty' name='["+row.Id+"].Qty' style='width:75px" + styleInput + "' value=" + row.Qty + ">");
                sb.Append("<input type='hidden' asp-for='Id' name='["+row.Id+"].Id' value='" + row.Id + "'>");
                sb.Append("<input type='hidden' asp-for='Product' name='["+row.Id+"].Product' value='" + row.Product + "'>");
                sb.Append("<input type='hidden' asp-for='ProductDescription' name='["+row.Id+"].ProductDescription' value='" + row.ProductDescription + "'>");
                sb.Append("<input type='hidden' asp-for='BatchStock' name='["+row.Id+"].BatchStock' value='" + row.BatchStock + "'>");
                sb.Append("<input type='hidden' asp-for='RowQty' name='["+row.Id+"].RowQty' value='" + row.RowQty + "'>");
                sb.Append("<input type='hidden' asp-for='Rest' name='["+row.Id+"].Rest' value='" + row.Rest + "'>");
                sb.Append("<input type='hidden' asp-for='OrderNo' name='["+row.Id+"].OrderNo' value='" + row.OrderNo + "'>");
                sb.Append("<input type='hidden' asp-for='RowNo' name='["+row.Id+"].RowNo' value='" + row.RowNo + "'>");
                sb.Append("<input type='hidden' asp-for='Batch' name='["+row.Id+"].Batch' value='" + row.Batch + "'></td>");
                sb.Append("<td>" + (row.RowQty - row.Qty - alreadyPicked) + "</td>");
                sb.Append("</tr>");
                alreadyPicked += row.Qty;
            }                             

        sb.AppendLine("</table>");
            result = sb.ToString();
            return result; 
        }


   
  
        public IActionResult PickBatch(List<DeliverBatchParam> batches, string sign = "")
        {
            WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<WarehouseNumber> warehouseList = new List<WarehouseNumber>();

            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
            List<DeliverRowParam> deliver = new List<DeliverRowParam>();

            try
            {
                if (batches.Count > 0)
                {
                    foreach (var batch in batches)
                    {
                        if (batch.Qty > 0)
                        {
                            DeliverRowParam param = new DeliverRowParam()
                            {
                                AmountToDeliver = GeneralFunctions.getDecimalFromStr(batch.Qty.ToString()),
                                DeliverDate = "",
                                DeliverMaterialRow = false,
                                DeliverNote = "",
                                HandleOperationRow = false,
                                OrderNo = batch.OrderNo,
                                RowNo = int.Parse(batch.RowNo),
                                WarehouseNo = batch.Batch,
                                SetWarehouseOnOrderrow = false,
                                User = sign,

                            };

                            deliver.Add(param);
                        }

                    }

                    DeliverResult result = mOM.Deliver(deliver);
                    if (result.Succeeded)
                    {
                        Response.Cookies.Delete(batches[0].OrderNo + batches[0].RowNo + "-JSON");
                    }
                }
            }
            catch
            {

            }

            return Content("OK");
        }

        public void SaveBatch(List<DeliverBatchParam> batches)
        {
            batches.RemoveAll(x => x.Qty == 0);
            int i = 0;
            decimal picked = 0;
            foreach(var batch in batches)
            {
                batch.Id = i;
                batch.Rest = batch.RowQty-batch.Qty - picked;
                picked += batch.Qty;
                i++;
            }

            var json = JsonConvert.SerializeObject(batches);
            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddMinutes(60);           
            HttpContext.Response.Cookies.Append(batches[0].OrderNo + batches[0].RowNo + "-JSON", json, cookieOptions);

        }
        public void ClearBatch(List<DeliverBatchParam> batches)
        {

            Response.Cookies.Delete(batches[0].OrderNo + batches[0].RowNo + "-JSON");
        
        }
        [Authorize]
        public string DeliverOrderRows(string order, List<DeliverRowParam> rows, string state, string warehouse)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            string decSep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            try
            {
                foreach (var row in rows)
                {
                    if (decSep == ".")
                    {
                        row.AmountToDeliver = decimal.Parse(row.AmountToDeliver.ToString().Replace(",", "."));
                    }
                    else
                    {
                        row.AmountToDeliver = decimal.Parse(row.AmountToDeliver.ToString().Replace(".", ","));
                    }

                    row.DeliverDate = "";
                    row.DeliverMaterialRow = false;
                    row.DeliverNote = "";
                    row.DeliverState = state;
                    row.HandleOperationRow = false;
                    row.OrderNo = order;
                    row.WarehouseNo = "";
                    row.WarehouseNo = warehouse;
                }

            }
            catch { }

            try
            {
                DeliverResult result = mOM.Deliver(rows);
                return result.DeliverNo;

            }
            catch
            {
                return "FAILED";
            }


        }


        [Authorize]
        public IActionResult BackDeliverOnOrderRow(string order, string row, string state)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<BackDeliverRowParam> deliver = new List<BackDeliverRowParam>();

            try
            {
                deliver.Add(new BackDeliverRowParam { BackDeliverOnMaterialRow = false, DeliverNote = null, OrderNo = order, RowNo = int.Parse(row) });

                DeliverResult result = mOM.BackDeliver(deliver);

                OrderRow or = mOM.GetOrderRow(order, row);
                return ViewComponent("OrderRowSlice", new { or = getCompactOrderRow(or) });
            }
            catch
            {
                return null;
            }
        }

        [Authorize]
        public string BackDeliverOnOrderRows(string order, int[] rowIds)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                List<BackDeliverRowParam> deliver = new List<BackDeliverRowParam>();
                foreach (var row in rowIds)
                {
                    deliver.Add(new BackDeliverRowParam { OrderNo = order, RowNo = row, DeliverNote = null, BackDeliverOnMaterialRow = false });
                }
                DeliverResult result = mOM.BackDeliver(deliver);
                return result.Succeeded.ToString();
            }
            catch
            {
                return "FAILED";
            }
        }

        [Authorize]
        public ActionResult ViewRows(string id)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<OrderRow> lst = mOM.GetOrderRowByOrderIdList(id);

            return View(lst);
        }

  
        [Authorize]
        public ActionResult AddOrderRow(string anr, decimal amount, string onr, string sign)
        {
            OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<OrderRow> lst = new List<OrderRow>();
            OrderRow or = new OrderRow();

            try
            {
                if (!string.IsNullOrEmpty(anr))
                {
                    List<OrderRow> orList = mOM.GetOrderRowByOrderIdList(onr);

                    or.OrderNo = onr;
                    or.ProductNo = anr;
                    or.Amount = amount;
                    or.RowNo = 255;
                   or.PreferedDeliverDate = GeneralFunctions.getStrFromDate(DateTime.Now);
                    or.WarehouseNo = orList.FindLast(o => o.WarehouseNo != "").WarehouseNo;

                    lst.Add(or);

                    mOM.UpdateOrderRowList(lst);
                }

                if (string.IsNullOrEmpty(sign))
                {
                    sign = "rockon";
                }
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("PickOrder", "Order", new { id = onr, sign = sign, force = false });
        }

        [Authorize]
        public ActionResult OrderIsAlreadyUnderPicking(string order, string sign, string forwardpath, string backpath)
        {
            ViewBag.Order = order;
            ViewBag.Signature = sign;
            ViewBag.ForwardPath = forwardpath;
            ViewBag.BackPath = backpath;

            return View();
        }

        //public JsonResult GetProducts(string id)
        //{
        //    try
        //    {
        //        LogtradeForwarder lf = mLM.GetForwarder("", id, true);

        //        if (lf != null)
        //        {
        //            if (lf.Products != null)
        //            {
        //                return Json(lf.Products);
        //            }
        //        }

        //    }
        //    catch
        //    {
        //    }

        //    return null;

        //}

        //public JsonResult GetTermsOfDeliveryList(string id)
        //{
        //    try
        //    {
        //        List<LogtradeTermOfDelivery> result = mLM.GetTermsOfDeliveryList("", id);

        //        if (result != null)
        //        {
        //            return Json(result);
        //        }

        //    }
        //    catch
        //    {
        //    }

        //    return null;

        //}

        //public JsonResult GetShipmentsServiceList(string id)
        //{
        //    try
        //    {
        //        List<LogtradeShipmentService> result = mLM.GetShipmentServiceList("", id);

        //        if (result != null)
        //        {
        //            return Json(result);
        //        }

        //    }
        //    catch
        //    {
        //    }

        //    return null;

        //}

        //public List<LogtradeShipmentService> GetShipmentsServiceObjectsList(string id)
        //{
        //    try
        //    {
        //        List<LogtradeShipmentService> result = mLM.GetShipmentServiceList("", id);

        //        if (result != null)
        //        {
        //            return result;
        //        }

        //    }
        //    catch
        //    {
        //    }

        //    return null;

        //}

        //public JsonResult GetProductPackageTypes(string id)
        //{
        //    try
        //    {
        //        List<LogtradePackageType> lst = mLM.GetProductPackageTypeList("", id, true);

        //        if (lst != null)
        //        {
        //            return Json(lst);
        //        }
        //    }
        //    catch
        //    {
        //    }

        //    return null;
        //}
        //public List<LogtradePackageType> GetProductPackageTypesList(string id)
        //{
        //    try
        //    {
        //        List<LogtradePackageType> lst = mLM.GetProductPackageTypeList("", id, true);

        //        if (lst != null)
        //        {
        //            return lst;
        //        }
        //    }
        //    catch
        //    {
        //    }

        //    return null;
        //}

        private string parseFilterConstants(string filter)
        {
            int count = 0;

            if (filter.Contains("#DAT"))
            {
                while (filter.Contains("#DAT"))
                {

                    int pos = filter.IndexOf("#DAT");
                    string whole = filter.Substring(pos, 6);
                    string date = DateTime.Now.AddDays(int.Parse(whole.Substring(4))).ToString("yyMMdd");
                    filter = filter.Replace(whole, date);
                    count++;

                    // If this high count something went wrong
                    if (count > 20)
                    {
                        break;
                    }
                }
            }

            return filter;
        }

        private List<FilterDTO> getFilterList(string prefix)
        {
            BaseDataModel mBDM = new BaseDataModel(_Config, _userManager.GetUserAsync(User)?.Result);

            List<FilterDTO> result = new List<FilterDTO>();

            try
            {
                List<BaseTable> filterlist = mBDM.GetBaseTableList(prefix);

                if (filterlist != null)
                {
                    foreach (BaseTable bt in filterlist)
                    {
                        result.Add(new FilterDTO { Id = bt.Id, Description = bt.Description, Type = bt.Type, Filter = bt.Description2, UseCache = bt.Code1 == "T" ? true : false });
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }

        [Authorize]
        public string ValidateSignature(string sign)
        {
            UserModel mUM = new UserModel(_Config, _userManager.GetUserAsync(User)?.Result);

            Console.WriteLine("ValidateSignature: " + sign);

            if (!string.IsNullOrEmpty(sign))
            {
                User user = mUM.GetUser("", sign.ToUpper());

                if (user.UserName != null)
                    return "true";
                else
                    return "false";
            }
            else
                return "FAILED";

        }

        private int getIntValue(string value)
        {
            if (value == "A")
                return 10;
            else if (value == "B")
                return 11;
            else if (value == "C")
                return 12;
            else if (value == "D")
                return 13;
            else
                return int.Parse(value);
        }

        private void updateLogtextOnOrderRow(string order, string row, decimal amountDelivered, string levtid, string levdate, string originalLevdate, string who, string cause, bool changedByCustomer)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                List<OrderRowText> txtList = new List<OrderRowText>();

                /*
                 * Check if deliver is in time, if so we abort
                 */

                if (levdate.CompareTo(originalLevdate) == 0)
                {
                    return;
                }

                /*
                    Leveranstext - Loggtext ändrar leveranstid
                */
                if (!string.IsNullOrEmpty(cause))
                {
                    OrderRowText txtLevWrongDayLog = new OrderRowText();

                    txtLevWrongDayLog.OrderNo = order;
                    txtLevWrongDayLog.RowNo = row;
                    txtLevWrongDayLog.TextRowNo = "255";
                    if (changedByCustomer)
                    {
                        txtLevWrongDayLog.Text = $"Levtid ändrad från {originalLevdate} till {levdate} Av Kund ({levdate}/{who})";
                    }
                    else
                    {
                        txtLevWrongDayLog.Text = $"Levtid ändrad från {originalLevdate} till {levdate} Av Oss ({levdate}/{who})";
                    }

                    txtLevWrongDayLog.InvoiceState = "L";
                    txtLevWrongDayLog.DeliverNoteState = "S";
                    txtLevWrongDayLog.OrderConfirmationState = "0";
                    txtLevWrongDayLog.PickListState = "0";

                    txtList.Add(txtLevWrongDayLog);
                }

                /*
                    Leveranstext - Orsak till felleverans
                */
                if (!string.IsNullOrEmpty(cause))
                {

                    OrderRowText txtLevWrongDay = new OrderRowText();

                    txtLevWrongDay.OrderNo = order;
                    txtLevWrongDay.RowNo = row;
                    txtLevWrongDay.TextRowNo = "255";
                    txtLevWrongDay.Text = $"{cause}";
                    txtLevWrongDay.InvoiceState = "I";
                    txtLevWrongDay.DeliverNoteState = "K";
                    txtLevWrongDay.OrderConfirmationState = "0";
                    txtLevWrongDay.PickListState = "0";

                    txtList.Add(txtLevWrongDay);
                }

                /*
                    Leveranstext - Skall med på alla rader
                */
                OrderRowText txtLev = new OrderRowText();

                txtLev.OrderNo = order;
                txtLev.RowNo = row;
                txtLev.TextRowNo = "255";
                txtLev.Text = $"Utlev Antal:{amountDelivered} LTid: {levtid} LDat: {levdate} ({levdate}/{who}) ";
                txtLev.InvoiceState = "I";
                txtLev.DeliverNoteState = "S";
                txtLev.OrderConfirmationState = "0";
                txtLev.PickListState = "0";

                txtList.Add(txtLev);

                /*
                    Sänd alla texter till GIS 
                */
                if (txtList.Count > 0)
                {
                    mOM.UpdateOrderRowTextList(txtList);
                }



            }
            catch (Exception e)
            {

            }

        }

        private void setReservationCode(string order, string row)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);

                OrderRow orderrow = mOM.GetOrderRow(order, row);

                if (orderrow.DeliverState != "5")
                {
                    orderrow.ReservedId = "";

                    mOM.UpdateOrderRowList(new List<OrderRow> { orderrow });
                }
            }
            catch (Exception e)
            {

            }

        }


        [HttpPost]
        [Authorize]
        public IActionResult SearchOrder(string ordernumber)
        {
            try
            {
                OrderModel mOM = new OrderModel(_Config, _userManager.GetUserAsync(User)?.Result);
               DeliverNoteModel mDN = new DeliverNoteModel(_Config, _userManager.GetUserAsync(User)?.Result);
                var order = mOM.GetOrderById(ordernumber, true);
                              
   
 

                if(order==null)
                {
                    return Content("No orders found!");
                }
                string delivernotes = "";
                //if(order.DeliverState =="5")
                //{
                    var list = mDN.GetDeliverNoteByOrderIdxList("2", ordernumber, "*", "true", "50", "false");
                if(list.Count()==0)
                {
                    return Content("OK");
                }     
                else if(list.Count()==1)
                {
                    return Content("GoToDN:"+list[0].DeliverNoteNo);
                }
                else
                {
                    foreach (var item in list)
                    {
                        delivernotes += "|" + item.DeliverNoteNo;
                    }
                    return Content(delivernotes);
                }

            }

            catch (Exception e)
            {
             return Content(e.Message);
            }
            return Content("OK");
        }

        private CompactOrderRow getCompactOrderRow(OrderRow or, string label="", string customer = "", string ohText2 = "")
        {
            CompactOrderRow result = new CompactOrderRow();

            try
            {
                result.OrderNo = or.OrderNo;
                result.RowNo = or.RowNo;
                result.ProductNo = or.ProductNo;
                result.ProductDescription = or.ProductDescription;
                result.ProductDescription2 = or.ProductDescription2;
                result.Amount = or.Amount.Value;
                result.DeliveredAmount = or.DeliveredAmount.Value;
                result.AmountToDeliver = or.AmountToDeliver.Value;
                result.DeliverState = or.DeliverState;
                result.Unit = or.Unit;
                result.RowType = or.RowType;
                result.MDF = or.MDF;
                result.PreferedDeliverDate = or.PreferedDeliverDate;
                result.WarehouseNo = or.WarehouseNo;
                result.ProductStockUpdateType = or.Product?.StockUpdateType;
                result.ManuallyAdded = or.ManuallyAdded;
                result.WarehouseType = or.Product.WarehouseType;
                result.MaterialPlanFlag = or.MTF;
                result.Origin = or.Product.CountryOfOrigin;
                result.ProductText = or.Product.ProductText;
                result.PrintLabel = label;
                result.CustomerNo = customer;
                result.OhText2 = ohText2;
                
                if (or.Textrows != null)
                {
                    result.TextRows = or.Textrows;
                }

                result.WarehouseList = new List<CompactWarehouse>();
                foreach (var wh in or.Product.WarehouseList)
                {
                    if (wh.WarehouseLocation.StartsWith(or.WarehouseNo) && wh.Stock > 0)
                    {
                        result.WarehouseList.Add(new CompactWarehouse
                        {
                            Id = wh.Id,
                            Location = wh.WarehouseLocation,
                            Amount = wh.Stock,
                            LastDate = wh.Description2,
                            LastDateAsDate = GeneralFunctions.getDateFromStr(wh.Description2),
                            SaleableCode = wh.SaleableCode
                        });
                    }
                }

                if (result.WarehouseList?.Count > 0)
                {
                    result.SelectedWarehouse = result.WarehouseList.Where(s => s.Amount > 0).OrderBy(d => d.LastDateAsDate).First();
                }

            }
            catch (Exception e)
            {
                logger.Error("Error in getCompactOrderRow()", e);
            }

            return result;
        }
    }

    static class ConfigurationManager
    {
        public static IConfiguration AppSetting { get; }
        static ConfigurationManager()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }
    }
    public class ViewHistoricDn
    {
        public string OrderNo { get; set; }
        public string DeliverNote { get; set; }
    }
    public class DeliverBatchParam
    {
        public int Id { get; set; } 
        public string? OrderNo { get; set; }
        public string? RowNo { get; set; }
        public string? Product { get; set; }
        public string? ProductDescription { get; set; }
        public string? Batch { get; set; }
        public decimal BatchStock { get; set; }
        public decimal RowQty { get; set; }
        public decimal Qty { get; set; }
        public decimal Rest { get; set; }
    }
}





