﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Babc_logistic_core.Controllers
{
    public class InventoryController : Controller
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly InventoryContext _context;

        //private GIS.Model.ProductModel mPM;
        private IConfiguration _Config;
        private UserManager<ApplicationUser> _userManager;

        public InventoryController(IConfiguration config, InventoryContext context, UserManager<ApplicationUser> userManager)
        {
            //mPM = new GIS.Model.ProductModel(config, user);
            _Config = config;
            _userManager = userManager;

            _context = context;
        }

        // GET: Inventory
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult OverView()
        {
            InventoryOverviewVM result = new InventoryOverviewVM();

            try
            {
                result.ProductNo = "";
                result.Warehouse = "";
                result.InventorySetList = new List<InventorySet>();

                result.InventorySetList.Add(new InventorySet { ID = 1, Description = "Lista 1" });
                result.InventorySetList.Add(new InventorySet { ID = 2, Description = "Lista 2" });
                result.InventorySetList.Add(new InventorySet { ID = 3, Description = "Lista 3" });
                result.InventorySetList.Add(new InventorySet { ID = 4, Description = "Lista 4" });
                result.InventorySetList.Add(new InventorySet { ID = 5, Description = "Lista 5" });
            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        [Authorize]
        public IActionResult CreateInventorySet(CreateInventorySetVM param)
        {
            CreateInventorySetVM result = new CreateInventorySetVM();

            try
            {
                if(!string.IsNullOrEmpty(param.WarehousNo))
                {
                    result.Result = new InventorySet();
                    result.Description = param.Description;

                    //List<Product> prodList = mPM.GetProductList("1", "*", "FRY!=z");

                    //foreach(Product product in prodList)
                    //{
                    //    WarehouseNumber wn = product.WarehouseList.Find(w => w.Id == param.WarehousNo);
                    //    if((wn != null))
                    //    {
                    //        result.Result.CreatedDate = DateTime.Now;
                    //        result.Result.ID = 0;
                    //        result.Result.InventoryList = new List<Inventory>();

                    //        Inventory inv = new Inventory();
                    //        inv.Date = GeneralFunctions.getStrFromDate(DateTime.Now);
                    //        inv.OriginalAmount = wn.Stock;
                    //        inv.InventoryDone = false;
                    //        inv.ProductNo = product.ProductNo;
                    //        inv.VIPAtTime = GeneralFunctions.getDecimalFromStr(wn.VIP);
                    //        inv.WarehouseNo = wn.Id;

                    //        result.Result.InventoryList.Add(inv);
                    //    }
                    //}

                    InventorySet addedSet = InventoryLib.AddInventorySet(result.Result);

                }
            }
            catch (Exception e)
            {

            }

            return View(result);

        }

        [HttpPost]
        [Authorize]
        public IActionResult OverView(InventoryOverviewVM param)
        {
            InventoryOverviewVM result = param;
   
            try
            {
                result.CurrentInventorySet = InventoryLib.GetInventorySet(param.CurrentInventorySetId);
             
                }
            catch (Exception e)
            {
                logger.Error("Error in OverView [POST]", e);
            }

            return View(result);
        }


        [Authorize]
        public string ReadBackup(string filename)
        {
            List<InventoryDetail> lst = new List<InventoryDetail>();
            string result = "";

            try
            {
                //using (StreamReader _testData = new StreamReader(Server.MapPath("~/" + filename), true))
                //{
                //    string json = _testData.ReadToEnd();

                //    lst = JsonConvert.DeserializeObject<List<InventoryDetail>>(json);

                //    Cache.DataCache.AddToCache<List<InventoryDetail>>("inventory", 10, lst);
                //}
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        [Authorize]
        public string FindProduct(string search)
        {
            Product result = new Product();

            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                var p = mPM.GetProductById(search);

                if (p?.ProductNo != null)
                {
                    result = p;
                }
                else
                {
                    result = new Product { ProductNo = null, Description = "" };
                }
            }
            catch (Exception e)
            {

            }

            return JsonConvert.SerializeObject(result);
        }

        [Authorize]
        public IActionResult SumInventory()
        {
            List<InventorySumVM> result = new List<InventorySumVM>();

            try
            {
                //List<InventoryDetail> lst = Cache.DataCache.GetFromCache<InventoryDetail>("inventory");
                //var groupedList = lst.GroupBy(inventory => inventory.ProductNo);

                //foreach (var group in groupedList)
                //{
                //    InventorySumVM sum = new InventorySumVM();

                //    sum.Amount = group.Sum(s => s.Amount);
                //    sum.ProductNo = group.Key;
                //    sum.Warehouse = group.First().WarehouseNo;
                //    sum.InventoryDate = group.Last().Date;

                //    sum.ContainingInventories = group.ToList();

                //    result.Add(sum);
                //}
            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        [Authorize]
        public System.Web.Mvc.FileContentResult ExecuteInventory(int setid)
        {
            InventoryFileResult result = new InventoryFileResult();

            try
            {
                GIS.Model.ProductModel mPM = new GIS.Model.ProductModel(_Config, _userManager.GetUserAsync(User)?.Result);

                var inventorys = InventoryLib.GetAllInventorOnSet(setid);

                InventoryFileParam inventory = new InventoryFileParam();
                foreach (var sum in inventorys)
                {
                    inventory.Inventorys.Add(new InventoryParam
                    {
                        ProductNo = sum.ProductNo,
                        WarehouseNo = sum.WarehouseNo,
                        Amount = sum.Amount,
                        Date = sum.Date,
                        Time = sum.Time,
                        PlaceName = "",
                        PlaceNo = "",
                        Note = sum.Note,
                        User = "GIS",
                        Terminal = "G1",
                        Rating = "",
                    });
                }

                result = mPM.CreateInventoryFile(inventory);
            }
            catch (Exception e)
            {
                logger.Error("Error in ExecuteInventory", e);
            }

            byte[] byteArr = Encoding.UTF8.GetBytes(result.InventoryFile);
            string mimeType = "text/plain";

            return new System.Web.Mvc.FileContentResult(byteArr, mimeType)
            {
                FileDownloadName = "excelfromByte1.xlsx"
            };
           
        }

        [Authorize]
        public string Delete(string id)
        {
            string result = "";

            try
            {
                //List<InventoryParam> lst = Cache.DataCache.GetFromCache<InventoryParam>("inventory");

                //var itemToRemove = lst.Where(i => i.Note == id).FirstOrDefault();

                //if (itemToRemove != null)
                //{
                //    lst.Remove(itemToRemove);
                //}

                //Cache.DataCache.AddToCache<List<InventoryParam>>("inventory", 10, lst);
            }
            catch (Exception e)
            {
                result = "false";
            }

            return result;
        }

        private Tuple<bool, string> CheckWarehouse(Product product, InventoryOverviewVM param)
        {
            Tuple<bool, string> result = new Tuple<bool, string>(false, "");

            //checked inventory on product if pressent
            if (product.WarehouseList?.Count > 0)
            {
                // Does the user send a warehouse?
                if (!string.IsNullOrEmpty(param.Warehouse))
                {
                    if (product.WarehouseList.Find(w => w.Id == param.Warehouse) != null)
                    {
                        result = new Tuple<bool, string>(true, "");
                    }
                    else
                    {
                        result = new Tuple<bool, string>(false, "Lagernr: " + param.Warehouse + " finns inte på artikel " + product.ProductNo);
                    }
                }
                else
                {
                    result = new Tuple<bool, string>(false, "Lagernr får ej vara blankt på artikel: " + product.ProductNo);
                }
            }
            else // Product is NOT a MultipleWarehouse product
            {
                if (!string.IsNullOrEmpty(param.Warehouse))
                {
                    result = new Tuple<bool, string>(false, "Lagernr: " + param.Warehouse + " skickades in men " + product.ProductNo + " är ingen flerlagerartikel");
                }
            }

            return result;
        }
        [HttpPost]
        [Authorize]
        public IActionResult searchBatch(string batch)
        {
            string result = "";
            WarehouseNumber warehouseNumber = new WarehouseNumber();
            try
            {
                WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
                warehouseNumber = mWM.GetBatch(batch, "*");
                if (warehouseNumber != null)
                {
                    ProductLocationParam param = new ProductLocationParam();
                    param.GeneralFilter = "*";
                    param.IndexFilter = warehouseNumber.ProductNo;
                    param.StockOnly = false;
                    //var list = mWM.GetWarehouseLocationList("*", false);
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(warehouseNumber.ProductNo + "|");
                stringBuilder.Append(warehouseNumber.WarehouseLocation + "|");
                stringBuilder.Append(warehouseNumber.Description + "|");
                stringBuilder.Append(warehouseNumber.Description2 + "|");
                stringBuilder.Append(warehouseNumber.Stock + "|");
                result = stringBuilder.ToString();


            }
            catch (Exception e)
            {


            }

            return Content(result);
        }
        [HttpPost]
        [Authorize]
        public IActionResult MoveBatch(string productNo, string fromWarehouseNo, string fromPlaceNo, string toWarehouseNo, string toPlaceNo, string amount, string batchAmount, bool moveWarehouse)
        {
            WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
            string whNo = "";
            try
            {
                if(string.IsNullOrEmpty(toPlaceNo))
                {
                    toPlaceNo = fromPlaceNo;
                }
                if (GeneralFunctions.getDecimalFromStr(amount)!= GeneralFunctions.getDecimalFromStr(batchAmount))
                {
                    whNo = "-" + SplitBatch(fromWarehouseNo, amount, toPlaceNo);
                    return Content(whNo);
                }
                else
                {            
                    var warehouseNumber = mWM.GetBatch(fromWarehouseNo, "*");
   
                    if (warehouseNumber != null)
                    {
                        var dAmount = GeneralFunctions.getDecimalFromStr(amount);
                        if (warehouseNumber.Stock == dAmount)
                        {
                            warehouseNumber.WarehouseLocation = toPlaceNo;
                            var result = mWM.UpdateWarehouse(warehouseNumber);
                            PrintPalletLabel(warehouseNumber.Id, warehouseNumber.WarehouseLocation, warehouseNumber.Stock.ToString());
                        }

                    }
                }
            }
            catch (Exception)
            {

               
            }
           // GetLocationList();
            return Content("OK");
        }

        [HttpPost]
        [Authorize]
        public string SplitBatch(string fromWarehouseNo, string amount, string toPlace)
        {
            WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);
            GenericTableModel mGTM = new GenericTableModel(_Config, _userManager.GetUserAsync(User)?.Result);
            string result = "";
            try
            {
                // var test = mWM.GetWarehouseLocationList("*", true);
               
                var warehouseNumber = mWM.GetBatch(fromWarehouseNo, "*");
                // findBatch(productNo);
                var newWH = mWM.GetNextWarehouseNumber("2");
                
                if (warehouseNumber != null && !string.IsNullOrEmpty(newWH)) 
                {   var dAmount = GeneralFunctions.getDecimalFromStr(amount);
                    WarehouseNumber newWHnumber = new WarehouseNumber
                    {
                        Id=newWH,
                        Account= warehouseNumber.Account,
                        BatchNo=warehouseNumber.BatchNo,
                        Company=warehouseNumber.Company,
                        CreationDate=GeneralFunctions.getStrFromDate(DateTime.Now),
                        Description=warehouseNumber.Description,
                        Description2 = warehouseNumber.Description2,
                        LeadTime=warehouseNumber.LeadTime,
                        OrderPoint=warehouseNumber.OrderPoint,
                        WarehouseLocation=toPlace,                    
                        ProductNo=warehouseNumber.ProductNo,        
                        SaleableCode=warehouseNumber.SaleableCode,
                        SekIdxFlg=warehouseNumber.SekIdxFlg,
                        //Stock= dAmount,
                        VIP=warehouseNumber.VIP,
                        AmountDecimalCount=warehouseNumber.AmountDecimalCount,
                        PriceDecimalCount=warehouseNumber.PriceDecimalCount
                        
                        
                    };


                    var oldStock = warehouseNumber.Stock - dAmount;
                    


                    UpdateResult addResult = mWM.AddWarehouse(newWHnumber);
                    result = addResult.WarehouseNo;
                    if (addResult != null)
                    {
                        try
                        {
                            UpdateTableIndexObject additionalFields = new UpdateTableIndexObject();
                            additionalFields.Fields = new List<FieldObject>();
                            additionalFields.TableName = "AGL";
                            additionalFields.IndexFields = new List<FieldObject> { new FieldObject { FieldName = "ANR", FieldValue = warehouseNumber.ProductNo } }; ;
                            additionalFields.Key = warehouseNumber.ProductNo.PadRight(13) + result.PadRight(7);
                            List<string> fields = new List<string> { "_FKO", "_HNR", "_PKO", "_PKL", "_TKO" };
                            foreach (string field in fields)
                            {
                            


                                var value = mGTM.GetTableValueByIndex(new GetTableValueRequest
                                {
                                    Field = field,
                                    Index = 1,
                                    Key = warehouseNumber.ProductNo.PadRight(13) + warehouseNumber.Id.PadRight(7),
                                    TableName = "AGL"
                                });

                                FieldObject addFields = new FieldObject();
                                addFields.FieldName = field;
                                addFields.FieldValue = value.Result.FieldValue;
                                additionalFields.Fields.Add(addFields);
                            }

                            mGTM.UpdateTableByIndex(additionalFields, "false");

                        }
                        catch { }

                        var success = mWM.UpdateWarehouse(warehouseNumber);
                        PrintPalletLabel(addResult.WarehouseNo, warehouseNumber.WarehouseLocation, amount);
                        PrintPalletLabel(warehouseNumber.Id, warehouseNumber.WarehouseLocation, oldStock.ToString());
                    }

                    mWM.MoveProduct(warehouseNumber.ProductNo,warehouseNumber.Id, newWHnumber.Id, amount);    
                    

                }

            }
            catch (Exception)
            {


            }
            return result;
        }

        [HttpPost]
        [Authorize]
        public IActionResult GetLocationList()
        {
            return ViewComponent("InventoryLocationList");
        }

        [HttpPost]
        [Authorize]
        public IActionResult PrintPalletLabel(string batch, string location, string qty)
        {
            PrintingModel mPM = new PrintingModel(_Config, _userManager.GetUserAsync(User)?.Result);

            ERPReport report = new ERPReport();
            bool result = false;
            try
            {
                report = _Config.GetSection("PrintPalletLabel").Get<ERPReport>();

                report.DocGen = ConfigurationManager.AppSetting["PrintPalletLabel:DocGen"];
                report.Report = ConfigurationManager.AppSetting["PrintPalletLabel:Report"];
                report.Medium = ConfigurationManager.AppSetting["PrintPalletLabel:Medium"];
                report.Form = ConfigurationManager.AppSetting["PrintPalletLabel:Form"];
                report.FilePath = ConfigurationManager.AppSetting["PrintPalletLabel:FilePath"];
                report.Wait = Convert.ToBoolean(ConfigurationManager.AppSetting["PrintPalletLabel:Wait"]);


            }
            catch
            {

            }

            try
            {
                report.IndexFrom = batch;
                report.IndexTo = batch;


                report.Dialogs[0].Value = batch;
                report.Dialogs[1].Value = location;
                report.Dialogs[2].Value = qty;

                result = mPM.Print(report);
                result = true;
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return Content("Error: " + e);

            }
            if (result)
            {
                return Content("OK");
            }
            else
            {
                return Content("Error while trying to print label.");
            }

        }


        public IActionResult findBatch(string ProductNo)
        {
            string result = "";
            WarehouseNumber warehouseNumber = new WarehouseNumber();
            List<ProductLocation> list = new List<ProductLocation>();
            try
            {
                WarehouseModel mWM = new WarehouseModel(_Config, _userManager.GetUserAsync(User)?.Result);

                if(warehouseNumber!=null)
                {
                    ProductLocationParam param = new ProductLocationParam();
                    param.GeneralFilter = "*";
                    param.IndexFilter = ProductNo;
                    param.StockOnly = false;
                    list = mWM.GetProductLocationList(param);
                    list.RemoveAll(x => x.Id.Length < 2);
                }

                StringBuilder stringBuilder = new StringBuilder();

                foreach(var wh in list)
                {
                    stringBuilder.Append(wh.WarehouseNoSingle + "|");
                    stringBuilder.Append(wh.Id + "|");
                    stringBuilder.Append(wh.Stock + "||");          
                }
   
                result = stringBuilder.ToString();


            }
            catch (Exception e)
            {


            }

            return Content(result);
        }
    }
}

