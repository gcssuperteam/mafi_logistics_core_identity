﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Babc_logistic_core.Database
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    : base(options)
        {
        }
    }

    public class ApplicationUser : IdentityUser
    {
        public string GarpUser { get; set; }
        public string GarpPassword { get; set; }
        public string GisToken { get; set; }
    }
}
