﻿using Babc_logistic_core.Database;
using Babc_logistic_core.Database.GIS.DTO;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using BABC_Logistics_core.GIS.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.Model
{
    public class LogtradeModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LogtradeModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {
        }

        public string TestLogtradeConnection()
        {
            string rest = "", result = "";

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/TestLogtradeConnection/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<string>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public LogtradeForwarder GetForwarder(string forwardercode, bool new_read)
        {
            string rest = "";
            LogtradeForwarder forwarder = new LogtradeForwarder();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetForwarder/" + GISToken + "/" + forwardercode + "/" + new_read.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                forwarder = JsonConvert.DeserializeObject<LogtradeForwarder>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return forwarder;
        }

        public List<LogtradeForwarder> GetForwarderList(bool new_read)
        {
            string rest = "";
            List<LogtradeForwarder> lst = new List<LogtradeForwarder>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetForwarderList/" + GISToken + "/" + new_read.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeForwarder>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradePackageType> GetProductPackageTypeList(string productid, bool custom)
        {
            string rest = "";
            List<LogtradePackageType> lst = new List<LogtradePackageType>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetProductPackageTypeList/" + GISToken + "/" + productid + "/" + custom.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradePackageType>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradePackageTypeMapping> GetPackageTypeMappingList(string productNo)
        {
            string rest = "";
            List<LogtradePackageTypeMapping> lst = new List<LogtradePackageTypeMapping>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/nShiftSvc/REST/GetPackageTypeMappingList/" + GISToken + "/" + productNo;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradePackageTypeMapping>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeProductMapping> GetProductMappingList()
        {
            string rest = "";
            List<LogtradeProductMapping> lst = new List<LogtradeProductMapping>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetProductMappingList/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeProductMapping>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeUserMapping> GetUserMappingList()
        {
            string rest = "";
            List<LogtradeUserMapping> lst = new List<LogtradeUserMapping>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetUserMappingList/" + GISToken; 

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeUserMapping>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeShipmentService> GetShipmentServiceList(string productid)
        {
            string rest = "";
            List<LogtradeShipmentService> lst = new List<LogtradeShipmentService>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetShipmentServiceList/" + GISToken; 

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeShipmentService>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeTermOfDelivery> GetTermsOfDeliveryList(string productid)
        {
            string rest = "";
            List<LogtradeTermOfDelivery> lst = new List<LogtradeTermOfDelivery>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetTermsOfDeliveryList/" + GISToken; 

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeTermOfDelivery>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeSenderAddress> GetSenderAddressList()
        {
            string rest = "";
            List<LogtradeSenderAddress> lst = new List<LogtradeSenderAddress>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetSenderAddressList/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252); //System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeSenderAddress>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public LogtradeShipment GetShipment(GetShipmentParam param)
        {
            string rest = "";
            LogtradeShipment lst = new LogtradeShipment();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetShipment/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    lst = JsonConvert.DeserializeObject<SingleResult<LogtradeShipment>>(result).Result;
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public LogtradeShipmentResponse CreateShipment(LogtradeShipment shipment)
        {
            string rest = "";
            LogtradeShipmentResponse result = new LogtradeShipmentResponse();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/CreateShipment/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(shipment));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string text = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<LogtradeShipmentResponse>(text);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public LogtradePriceQuestionResponse GetShipmentPrice(LogtradeShipment shipment)
        {
            string rest = "";
            LogtradePriceQuestionResponse result = new LogtradePriceQuestionResponse();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetShipmentPrice/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(shipment));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<LogtradePriceQuestionResponse>(json);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }











        public List<LogtradeGoodsItem> GetGarpPackageList(string delivernote)
        {
            string rest = "";
            List<LogtradeGoodsItem> lst = new List<LogtradeGoodsItem>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/GetGarpPackageList/" + GISToken + "/" + delivernote;


                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeGoodsItem>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }




        public SingleResult<bool> AddGarpPackage(LogtradeGoodsItem pkg, string delivernote)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/AddGarpPackage/" + GISToken + @"/" + delivernote;


                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(pkg));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string text = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(text);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public SingleResult<bool> UpdateGarpPackage(string delivernote, LogtradeGoodsItem pkg)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/UpdateGarpPackage/" + GISToken + @"/" + delivernote;


                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(pkg));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string text = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(text);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public SingleResult<bool> RemoveGarpPackage(string garpid, string delivernote)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogtradeSvc/REST/RemoveGarpPackage/" + GISToken + @"/" + garpid + @"/" + delivernote;


                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject($"garpid: {garpid}, delivernote: {delivernote}"));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string text = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(text);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }










    }
}
