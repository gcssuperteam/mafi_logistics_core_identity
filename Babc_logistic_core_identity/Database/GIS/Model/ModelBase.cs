﻿using Babc_logistic_core.Database;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.Model
{
    public class ModelBase
    {
        public string GISBaseAddress { get; set; }
        public string GISToken { get; set; }

        private readonly IConfiguration _config;
        private ApplicationUser _user;

        public ModelBase(IConfiguration configuration, ApplicationUser user)
        {
            _config = configuration;
            _user = user;

            try
            {
                GISBaseAddress = _config.GetValue<string>("GIS:BaseAddress");
                GISToken = user.GisToken; //Configuration.GetValue<string>("GIS:Token");
            }
            catch (Exception e)
            {

            }

        }

        public ModelBase()
        {
        }

        private string getAddress()
        {
            string result = "";

            return result;
        }
    }
}
