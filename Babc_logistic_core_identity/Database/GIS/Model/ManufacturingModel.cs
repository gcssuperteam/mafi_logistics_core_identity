﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.Model
{
    public class ManufacturingModel : ModelBase
    {

        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ManufacturingModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {

        }

        public List<MaterialRow> getMaterialRowList(string onr, string row)
        {
            string rest = "";
            List<MaterialRow> lst = new List<MaterialRow>();

            try
            {
                rest = GISBaseAddress.Trim('/') + "/ManufacturingSvc/REST/GetMaterialRowList/" + GISToken + "/" + onr + "/" + row;
                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<MaterialRow>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }
    }
}
