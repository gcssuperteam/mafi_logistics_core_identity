﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.Model
{
    public class CustomerModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CustomerModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {

        }

        public List<Customer> GetCustomerList(string index, string indexfilter, string general_filter_string)
        {
            string rest = "";
            List<Customer> lst = new List<Customer>();

            try
            {
                rest = GISBaseAddress.Trim('/') + "/CustomerSvc/REST/GetCustomerList/" + GISToken + "/" + index + "/" + indexfilter + "/" + general_filter_string;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<Customer>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public Customer GetCustomerById(string id)
        {
            string rest = "";
            Customer customer = new Customer();

            rest = GISBaseAddress.Trim('/') + "/CustomerSvc/REST/GetCustomer/" + GISToken + "/" + id;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                customer = JsonConvert.DeserializeObject<Customer>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return customer;
        }

        public bool AddCustomer(Customer customer)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.Trim('/') + "/CustomerSvc/REST/AddCustomer/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(customer));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return false;
            }
        }

        public bool UpdateCustomer(Customer customer)
        {
            string rest = "";
            try
            {
                rest = GISBaseAddress.Trim('/') + "/CustomerSvc/REST/UpdateCustomer/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(customer));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return false;
            }
        }

        public bool DeleteCustomer(Customer customer)
        {
            string rest = "";

            rest = GISBaseAddress.Trim('/') + "/CustomerSvc/REST/DeleteCustomer/" + GISToken;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(customer));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return false;
            }
        }
        public string[] GetCustomerInfo(string customerid)
        {
            string rest = "";
            string response = "";
            try
            {

                if (!string.IsNullOrEmpty(customerid))
                {
                    rest = GISBaseAddress.Trim('/') + "/CustomerSvc/REST/GetCustomerInfo/" + GISToken + @"/" + customerid; ;
                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    response = responseStream.ReadToEnd();
                    responseStream.Close();


                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return new[] { e.Message };
            }
            return new[] { response };
        }
    }
}
