﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using System.Text.Json;
using System.Text;
using Microsoft.Extensions.Configuration;
using Babc_logistic_core.Database;
using Babc_logistic_core.Database.GIS.DTO;
using Newtonsoft.Json;

namespace Babc_logistic_core.GIS.Model
{
    public class MailModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    
        public MailModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {
           
        }

        public bool SendMail(SendMailParam sendMail)
        {
            string rest = "";
                      
            mLog.Debug("CALL TO SendMail: " +rest);

            try
            {
                //rest = getAddress("/REST/Print");
                rest = GISBaseAddress.TrimEnd('/') + "/MailSvc/REST/SendMail/";


                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(sendMail));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    return Convert.ToBoolean(result);
                    // do something with the results

                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return false;
            }



        }


       
    }
}
