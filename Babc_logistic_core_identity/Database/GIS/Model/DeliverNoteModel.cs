﻿using Babc_logistic_core.Database;
using Babc_logistic_core.Database.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.Model
{
    public class DeliverNoteModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DeliverNoteModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {

        }

        public void UpdateDeliverNoteRowTextList(List<DeliverNoteRowText> rows)
        {
            string rest = "";

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/DeliverNoteSvc/REST/UpdateDeliverNoteRowTextList/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(rows));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        public void UpdateDeliverNoteHead(DeliverNoteOH delivernote)
        {
            string rest = "";

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/DeliverNoteSvc/REST/UpdateDeliveryNoteHead/" + GISToken;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(delivernote));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        public DeliverNoteOH GetDeliverNote(string deliverno, bool withRows)
        {
            string rest = "";
            DeliverNoteOH result = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/DeliverNoteSvc/REST/GetDeliverNoteById/" + GISToken + "/" + deliverno + "/" + withRows.ToString();
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "GET";
                request.ContentType = "application/json; charset=utf-8";

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string s = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<DeliverNoteOH>(s);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }


        public DeliverNoteOH GetDeliverNoteByOrderId(string orderno, bool withRows)
        {
            string rest = "";
            DeliverNoteOH result = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/DeliverNoteSvc/REST/GetDeliverNoteByOrderId/" + GISToken + "/" + orderno + "/" + withRows.ToString();
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "GET";
                request.ContentType = "application/json; charset=utf-8";

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string s = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<DeliverNoteOH>(s);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }
        public List<DeliverNoteOH> GetDeliverNoteByOrderIdxList(string idx, string idxfilter, string filter, string with_rows, string max_count, string reverse_reading)
        {
            string rest = "";
            List<DeliverNoteOH> result = null;

            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/DeliverNoteSvc/REST/GetDeliverNoteByOrderIdxList/" + GISToken + "/" + idx + "/" + idxfilter + "/"+ filter + "/" + with_rows + "/" + max_count + "/" + reverse_reading;
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "GET";
                request.ContentType = "application/json; charset=utf-8";

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string s = reader.ReadToEnd();

                    result = JsonConvert.DeserializeObject<List<DeliverNoteOH>>(s);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }


    }
}
