﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using System.Text.Json;
using System.Text;
using Microsoft.Extensions.Configuration;
using Babc_logistic_core.Database;

namespace Babc_logistic_core.GIS.Model
{
    public class PrintingModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string GISPrintBaseAddress { get; set; } 
        public PrintingModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {
            GISPrintBaseAddress = config.GetValue<string>("GIS:PrintBaseAddress");
        }

        public bool Print(ERPReport report)
        {
            string rest = "";

            string json = JsonSerializer.Serialize(report);// JsonConvert.SerializeObject(report);
            mLog.Debug("CALL TO Print with JSON: " + json);

            try
            {
                //rest = getAddress("/REST/Print");
                rest = GISPrintBaseAddress.TrimEnd('/') + "/PrintingSvc/REST/Print/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(report));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    return Convert.ToBoolean(result);
                    // do something with the results

                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return false;
            }
        }

    }
}
