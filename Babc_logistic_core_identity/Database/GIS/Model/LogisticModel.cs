﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.Model
{
    public class LogisticModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LogisticModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {

        }

        public List<OrderHead> GetOrderList(OrderListParam param)
        {
            List<OrderHead> result = new List<OrderHead>();

            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/Logistic/REST/GetOrderList/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonSerializer.Deserialize<List<OrderHead>>(json);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public List<OrderHead> GetOrderListWithAdditionalOrder(PickListParam param)
        {
            List<OrderHead> result = new List<OrderHead>();

            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/LogisticSvc/REST/GetOrderPickListWithAdditionalOrder/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonSerializer.Deserialize<List<OrderHead>>(json);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public List<MaterialOrderMinimal> GetMaterialPickList(MaterialPickListParam param)
        {
            List<MaterialOrderMinimal> result = new List<MaterialOrderMinimal>();

            string rest = "";
            try
            {
                rest = GISBaseAddress.TrimEnd('/') + "/Logistic/REST/GetMaterialPicklist/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonSerializer.Serialize(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonSerializer.Deserialize<List<MaterialOrderMinimal>>(json);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        private string getAddress(string rest_function)
        {
            string result = "";

            return result;

        }
    }
}
