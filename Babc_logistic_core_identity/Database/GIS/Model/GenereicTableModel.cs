﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.Model
{
    public class GenericTableModel : ModelBase
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GenericTableModel(IConfiguration config, ApplicationUser user) : base(config, user)
        {
        }

        public SingleResult<FieldObject> GetTableValueByIndex(GetTableValueRequest tabreq)
        {
            SingleResult<FieldObject> result = new SingleResult<FieldObject>();
            string rest = "";

            try
            {
                rest = GISBaseAddress.Trim('/') + "/GenericTableSvc/REST/GetTableValueByIndex/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(tabreq));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<SingleResult<FieldObject>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in GetTableValueByIndex", e);
            }

            return result;
        }

        public GeneralTableUpdateResult UpdateTableByIndex(UpdateTableIndexObject updateObj, string insert)
        {
            GeneralTableUpdateResult result = new GeneralTableUpdateResult();
            string rest = "";

            try
            {
                rest = GISBaseAddress.Trim('/') + "/GenericTableSvc/REST/UpdateTableByIndex/" + GISToken + @"/" + insert;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<GeneralTableUpdateResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByIndex", e);
            }

            return result;
        }

        public List<GeneralTableUpdateResult> UpdateTableByIndexList(List<UpdateTableIndexObject> updateObj, string insert)
        {
            List<GeneralTableUpdateResult> result = new List<GeneralTableUpdateResult>();
            string rest = "";

            try
            {
                rest = GISBaseAddress.Trim('/') + "/GenericTableSvc/REST/UpdateTableByIndexList/" + GISToken + @"/" + insert;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Timeout = Timeout.Infinite;

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<List<GeneralTableUpdateResult>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByIndex", e);
            }

            return result;
        }

        public List<GeneralTableUpdateResult> UpdateTableByIndexListAsync(List<UpdateTableIndexObject> updateObj, string insert)
        {
            List<GeneralTableUpdateResult> result = new List<GeneralTableUpdateResult>();
            string rest = "";

            try
            {
                rest = GISBaseAddress.Trim('/') + "/GenericTableSvc/REST/UpdateTableByIndexList/" + GISToken + @"/" + insert;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Timeout = Timeout.Infinite;

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<List<GeneralTableUpdateResult>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByIndex", e);
            }

            return result;
        }

        public UpdateResult UpdateTableByRange(UpdateTableRangeObject updateObj)
        {
            UpdateResult result = new UpdateResult();
            string rest = "";

            try
            {
                rest = GISBaseAddress.Trim('/') + "/GenericTableSvc/REST/UpdateTableByRange/" + GISToken;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UpdateResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByRange", e);
            }

            return result;
        }

    }
}
