﻿using BABC_Logistics_core.Views.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace BABC_Logistics_core.GIS.DTO
{
    public partial class LogtradeForwarder
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public List<LogtradeProduct> Products { get; set; }
    }

    public partial class LogtradeProduct
    {
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ForwarderCode { get; set; }
    }

    public partial class LogtradeTermOfDelivery
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public partial class LogtradeShipment
    {
        public string ProductCode { get; set; }
        public string ForwarderCode { get; set; }
        public string SenderAddressId { get; set; }
        public string SenderReference { get; set; }
        public string ConsignmentId { get; set; }
        public string RecipientName { get; set; }
        public string RecipientForwarderCustomerCode { get; set; }
        public string RecipientAddress1 { get; set; }
        public string RecipientAddress2 { get; set; }
        public string RecipientAddress3 { get; set; }
        public string RecipientZip { get; set; }
        public string RecipientCity { get; set; }
        public string RecipientZipCity { get; set; }
        public string RecipientReference { get; set; }
        public string RecipientPhone { get; set; }
        public string RecipientEmail { get; set; }
        public string RecipientFax { get; set; }
        public string RecipientContactPerson { get; set; }
        public string RecipientCountryCode { get; set; }
        public string DeliverNote { get; set; }
        public string OrderNo { get; set; }
        public string TermsOfDeliveryCode { get; set; }
        public string TermsOfDeliveryLocation { get; set; }
        public string OurReference { get; set; }
        public string GANNo { get; set; }
        public string DeliveryCustomerName { get; set; }
        public string DeliveryCustomerAddress1 { get; set; }
        public string DeliveryCustomerAddress2 { get; set; }
        public string DeliveryCustomerAddress3 { get; set; }
        public string DeliveryCustomerZip { get; set; }
        public string DeliveryCustomerCity { get; set; }
        public string DeliveryCustomerCountryCode { get; set; }
        public string DeliveryCustomerContact { get; set; }
        public string DeliveryCustomerContactPhone { get; set; }
        public decimal DeclaredValueField { get; set; }
        public string DeclaredCurrencyCodeField { get; set; }
        public PaymentType PaymentTypeField { get; set; }
        public LogtradeAuthorization Authorization { get; set; }
        public List<LogtradeGoodsItem> GoodsItemList { get; set; }
        public List<LogtradeShipmentService> ShipmentServiceList { get; set; }
        public string ShipmentTemplate { get; set; }
        public string GarpUserId { get; set; }
        public string GarpTerminalId { get; set; }
        public string PrintQueueId { get; set; }
        public string ShipmentAction { get; set; } // Save, Prepare, Release
        public string ShipmentInstruction { get; set; }
        public string ShipmentFreeText { get; set; }

    }

    public partial class LogtradeAuthorization
    {
        public string User { get; set; }
        public string Password { get; set; }
    }

    public partial class LogtradeSenderAddress
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public partial class LogtradeGoodsItem
    {
        public string GarpId { get; set; }
        public string PackageTypeCode { get; set; }
        public string PackageTypeDescription { get; set; }
        public int Amount { get; set; }
        public decimal Weight { get; set; }
        public decimal Volyme { get; set; }
        public decimal LoadingMeters { get; set; }
        public string GoodsDescription { get; set; }
        public string ShippingMarks { get; set; }
        public bool ShowEurPalletsQuestion { get; set; }
        public List<LogtradePackageItem> PackageItems { get; set; }
    }

    public class LogtradePackageItem
    {
        public decimal GrossWeight { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Circumference { get; set; }
        public int Pieces { get; set; }
    }

    public partial class LogtradePackageType
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public partial class LogtradeShipmentService
    {
        public LogtradeShipmentService()
        {
            PropertyDescriptions = new List<LogtradeProperty>();
        }

        public string ServiceName { get; set; }
        public string Description { get; set; }
        public List<LogtradeProperty> PropertyDescriptions { get; set; }
    }

    public partial class LogtradeProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string Caption { get; set; }
        public bool ShowQuestion { get; set; }
    }

    public class LogtradePackageTypeMapping
    {
        public string Logtrade { get; set; }
        public string Garp { get; set; }
    }

    public class LogtradeProductMapping
    {
        public string GarpDeliverWay { get; set; }
        public string Forwarder { get; set; }
        public string Product { get; set; }
    }

    public class LogtradeUserMapping
    {
        public string ShipmentTemplate { get; set; }
        public string SenderAddressId { get; set; }
        public string LogtradeUserId { get; set; }
        public string LogtradePassword { get; set; }
        public string PrintQueueId { get; set; }
        public string GarpUserId { get; set; }
        public string GarpTerminalId { get; set; }
    }

    public class LogtradeSettings
    {
        public List<LogtradePackageTypeMapping> PackageMappings { get; set; }
        public List<LogtradeProductMapping> ProductMapping { get; set; }
        public List<LogtradeUserMapping> UserMapping { get; set; }
    }

    public partial class LogtradeShipmentResponse
    {
        public string ShipmentNumber { get; set; }
        public LogtradeShipment UsedLogtradeShipment { get; set; }
        public string Status { get; set; }
        public string DeliverNo { get; set; }
        public string OrderNo { get; set; }
        public decimal Price { get; set; }
        public List<string> Messages { get; set; }
        public CalculatedFreight CalculatedFreight { get; set; }
    }

    public partial class LogtradePriceQuestionResponse
    {
        public string Currency { get; set; }
        public decimal Price { get; set; }
        public string ProductCode { get; set; }
        //        public string DeliverNo { get; set; }
        public bool Succeeded { get; set; }
        public LogtradeShipment BasedOnThisShipment { get; set; }

        public List<string> Messages { get; set; }
    }

    public class PriceQuestionResult
    {
        public LogtradePriceQuestionResponse priceData { get; set; }
    }


    //public class priceData
    //{
    //    public string price { get; set; }
    //    public string additionalSurcharges { get; set; }
    //    public string priceWithAdditionalSurcharges { get; set; }
    //}

    public enum PaymentType
    {

        AnyPayment,
        Sender,
        Recipient,
        ThirdParty,
        UnknownParty,
    }

}
