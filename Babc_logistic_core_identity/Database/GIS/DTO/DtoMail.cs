﻿using System.Collections.Generic;

namespace Babc_logistic_core.Database.GIS.DTO
{

        public partial class SendMailParam
        {
            public string FromEmail { get; set; }
            public string FromName { get; set; }
            public List<SendMailReceipent> To { get; set; }
            public string Subject { get; set; }
            public string MessageText { get; set; }
            public string MessageHtml { get; set; }
            public List<SendMailReceipent> Cc { get; set; }
            public List<SendMailReceipent> Bcc { get; set; }
            public List<SendMailAttachment> Attachments { get; set; }
        }
        public partial class SendMailMultipleParam
        {
            public string FromEmail { get; set; }
            public string FromName { get; set; }
            public List<SendMailReceipent> To { get; set; }
            public string Subject { get; set; }
            public string MessageText { get; set; }
            public string MessageHtml { get; set; }
            public List<SendMailReceipent> Cc { get; set; }
            public List<SendMailReceipent> Bcc { get; set; }
            public List<SendMailAttachment> Attachments { get; set; }
        }
        /// <summary>
        /// Max filesize is 15mb and needs to be base64 ecnoded
        /// </summary>
        public partial class SendMailAttachment
        {
            public string ContentType { get; set; }
            public string Filename { get; set; }
            public string Base64 { get; set; }
        }
        public partial class SendMailReceipent
        {
            public string Email { get; set; }
            public string Name { get; set; }
        }

    
}
