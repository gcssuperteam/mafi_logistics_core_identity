﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class User
    {
        public int Id { get; set; }
        public string GarpServerAddress { get; set; }
        public string GarpServerPort { get; set; }
        public string MainDbDir { get; set; }
        public string UserName { get; set; }
        public string UserRoleName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string CurrentSite { get; set; }
        public string CurrentCompany { get; set; }
        public string RoleId { get; set; }
    }
}
