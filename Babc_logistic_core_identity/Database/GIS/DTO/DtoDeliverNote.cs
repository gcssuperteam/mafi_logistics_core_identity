﻿using Babc_logistic_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Database.GIS.DTO
{

    public partial class DeliverNoteOH
    {
        public string DeliverNoteNo { get; set; }
        public string SeqNo { get; set; } // Innomnr
        public string CustomerNo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCity { get; set; }
        public string Country { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string OrderType { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string DeliveryState { get; set; }
        public string X1F { get; set; }
        public string InvoiceState { get; set; }
        public string PaymentTerms { get; set; }
        public string DeliverWay { get; set; }
        public string TermsOfDeliveryCode { get; set; }
        public string OrderDate { get; set; }
        public string OurReference { get; set; }
        public string YourReference { get; set; }
        public string SellerId { get; set; }
        public string LastDeliverDate { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string PriceListId { get; set; }
        public string CurrencyId { get; set; }
        public string CompanyId { get; set; }
        public string AccountNo { get; set; }
        public string CostPlaceId { get; set; }
        public string ProjectId { get; set; }
        public string ExtraId { get; set; }
        public string DiscountId { get; set; }
        public string VatId { get; set; }
        public string CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByTerminal { get; set; }
        public string OrderNo { get; set; }
        public string InvoiceNo { get; set; }
        public string DeliveryNoteState { get; set; }
        public string CurrencyValue { get; set; }
        public string TransportTime { get; set; }
        public string DiscountPercentage { get; set; }
        public SellerDTO Seller { get; set; }
        public string OurReferenceName { get; set; }
        public List<DeliverNoteRow> Rows { get; set; }
        public List<DeliverNoteRowText> ZeroTexts { get; set; }
        public string ShippingNotification { get; set; }
        public string ShippingNotificationType { get; set; }
        public string ShippingContactName { get; set; }
        public string DeliverInstruction { get; set; }
        public string Freight3 { get; set; }
        public string Freight15 { get; set; }
        public string YourOrderContactId { get; set; }
        public string YourDeliverContactId { get; set; }
        public string YourInvoiceContactId { get; set; }
    }

    public partial class DeliverNoteRow
    {
        public string DeliverNoteNo { get; set; }
        public int RowNo { get; set; }
        public string OrderNo { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string AmountState { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string Unit { get; set; }
        public string DeliverState { get; set; }
        public decimal DeliveredAmount { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string DiscountPercentage { get; set; }
        public bool UseOHDiscount { get; set; }
        public decimal NetPrice { get; set; }
        public decimal RowValue { get; set; }
        public string VatId { get; set; }
        public string AccountId { get; set; }
        public string DeliveryNoteState { get; set; }
        public string InvoiceState { get; set; }
        public decimal? NX1 { get; set; }

        public decimal? DIM { get; set; }
        public Product Product { get; set; }
        public List<DeliverNoteRowText> TextRows { get; set; }
    }

    public partial class DeliverNoteRowText
    {
        public string DeliverNoteNo { get; set; }
        public string RowNo { get; set; }
        public string InomNo { get; set; }
        public string TextRowNo { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string DeliverNoteState { get; set; }
        public string InvoiceState { get; set; }
        public string Text { get; set; }

    }
}
