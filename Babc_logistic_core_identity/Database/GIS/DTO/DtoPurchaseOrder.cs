﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class PurchaseOrderHead
    {
        public string OrderNo { get; set; }
        public string Date { get; set; }
        public string DeliverAddress1 { get; set; }                 // Denna rad ner till CountryCode finns inte i PurchaseOrderSocket, antar att värdena hämtas från en annan tabell
        public string DeliverAddress2 { get; set; }
        public string DeliverZipCity { get; set; }
        public string CountryCode { get; set; }
        public string OrderType { get; set; }
        public string YoureReference { get; set; }
        public string OurReferenceId { get; set; }
        public string OurReferenceName { get; set; }                // Finns inte i PurchaseOrderSocket. Antar att den hämtas med hjälp av värdet i föregående fält.
        public string SellerId { get; set; }                        // Finns inte i PurchaseOrderSocket
        public string SellerName { get; set; }                      // Finns inte i PurchaseOrderSocket
        public string DeliverTerms { get; set; }
        public string DeliverWay { get; set; }
        public string DeliverWayText { get; set; }                  // Finns inte i PurchaseOrderSocket. Hämtas väl mha föregående värde.
        public string CalculatedDeliverDate { get; set; }           // Finns inte. Beräknas?
        public string InvoiceAddress1 { get; set; }                 // Finns inte. Hämtas från annan tabell?
        public string InvoiceAddress2 { get; set; }                 // Finns inte. Hämtas från annan tabell?
        public string InvoiceZipCity { get; set; }                  // Finns inte. Hämtas från annan tabell?
        public string DeliverState { get; set; }
        public string PaymentTerms { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyValue { get; set; }
        public string AccountId { get; set; }
        public string VatId { get; set; }
        public string Season { get; set; }
        public string SupplierNo { get; set; }
        public string SupplierName { get; set; }                    // Finns inte. Annan tabell?
        public string InvoiceCustomerNo { get; set; }               // Finns inte. Annan tabell?
        public string OrderConfirmationState { get; set; }          // Finns inte. Beräknas?
        public string GoodsReceiptState { get; set; }
        public string X1F { get; set; }
        public string LastDeliverDate { get; set; }
        public string PriceListId { get; set; }
        public string CompanyId { get; set; }
        public string Stopped { get; set; }
        public string ReservedId { get; set; }
        public string NotDeliveredValue { get; set; }
        public string DeliveredValue { get; set; }
        public string TransportTime { get; set; }
        public string DeliverPath { get; set; }                     // Finns inte.
        public string DeliverPlace { get; set; }                    // Finns inte.
        public string Order_ttmm { get; set; }
        public string AccountNo { get; set; }
        public string CostPlaceId { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }                     // Finns inte. Hämtas nog mha ovan värde.
        public string ExtraId { get; set; }
        public string DiscountId { get; set; }
        public string DiscountPercentage { get; set; }
        public string LastDeliverNote { get; set; }                 // Finns inte, alla fram tom Text4
        public string LastDeliverNoteState { get; set; }
        public string ShipmentNo { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalVolyme { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        public string Purchaser { get; set; }
        public string PurchaseState { get; set; }
        public bool ConnectedOrder { get; set; }                    // Finns inte.

        public SellerDTO Seller { get; set; }                       // Finns inte.
        public List<OrderRow> OrderRows { get; set; }               // Finns inte.
    }
}
