﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class OrderHead
    {
        private bool? _IsWholeOrderDeliverable = null;
        private bool? _DoesOrderContainsMeasure = null;

        public bool? IsWholeOrderDeliverable
        {
            get { return _IsWholeOrderDeliverable; }
            set
            {
                if (_IsWholeOrderDeliverable == value) return;
                _IsWholeOrderDeliverable = value;

            }
        }
       
        public bool? DoesOrderContainsMeasure
        {
            get { return _DoesOrderContainsMeasure; }
            set
            {
                if (_DoesOrderContainsMeasure == null)
                {
                    _DoesOrderContainsMeasure = value;
                }
            }
        }
        public int NonDeliverableRows { get; set; }
        public List<string> TypeOfMeasure { get; set; }
        public string CustomerCategoryName { get; set; }
        public string CustomerCategoryCode { get; set; }
        public bool IsMeasureDone { get; set; }
        public int DeliverableRowsCount { get; set; }
        public bool Relations { get; set; }
        public decimal NoOfPieces { get; set; }
        public string CustomerCode3 { get; set; }
        public bool IsRowsWithinDates { get; set; }
        public bool isExport { get; set; }
        public bool isWorkOrder { get; set; }
        public bool isLargeOrder { get; set; }
        public bool IncludeInPickList { get; set; }
        public bool isPrioOrder { get; set; }
        public bool HasMaterialPlannedRows { get; set; }
        public string DeliverWayId { get; set; }
        public bool HasAdditional { get; set; }     //Fattades, så tillagd 190502
    }
    
    public partial class OrderHead
    {
        

        public string OrderNo { get; set; }
        public string Date { get; set; }
        public string DeliverAddress1 { get; set; }  
        public string DeliverAddress2 { get; set; }
        public string DeliverAddress3 { get; set; }
        public string DeliverZipCity { get; set; }
        public string DeliverZip { get; set; }
        public string DeliverCity { get; set; }
        public string CountryCode { get; set; }
        public string OrderType { get; set; }
        public string YoureReference { get; set; }
        public string OurReferenceId { get; set; }
        public string OurReferenceName { get; set; }        
        public string SellerId { get; set; }
        public string SellerName { get; set; } 
        public string DeliverTerms { get; set; }
        public string DeliverTermsDescription { get; set; } 
        public string DeliverWay { get; set; }
        public string DeliverWayDescription { get; set; }
        public string DeliverState { get; set; }
        public string PaymentTerms { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyValue { get; set; }
        public string VatId { get; set; }
        public string Season { get; set; }
        public string DeliverCustomerNo { get; set; }  
        public string DeliverCustomerName { get; set; }
        public string DeliverCustomerPhone { get; set; }
        public string DeliverCustomerCode { get; set; }
        public string DeliverCustomerLockId { get; set; }
        public string DeliverCustomerType { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string X1F { get; set; }
        public string LastDeliverDate { get; set; }
        public string PriceListId { get; set; }
        public string CompanyId { get; set; }
        public string Stopped { get; set; }
        public string ReservedId { get; set; }
        public string NotDeliveredValue { get; set; }
        public string DeliveredValue { get; set; }
        public string TransportTime { get; set; }
        public string DeliverPath { get; set; }
        public string DeliverPlace { get; set; }
        public string Order_ttmm { get; set; }
        public string AccountNo { get; set; }
        public string CostPlaceId { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }   
        public string ExtraId { get; set; }
        public string DiscountId { get; set; }
        public string DiscountPercentage { get; set; }
        public string WaitingAt { get; set; }
        public string LastDeliverNote { get; set; } 
        public string LastDeliverNoteState { get; set; }
        public string ShipmentNo { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalVolyme { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        public string ContinueOnOrder { get; set; }
        public string ContinueFromOrder { get; set; }
        public bool AdditionalOrders { get; set; }
        public string[] CustomerInfo { get; set; }
        public bool? IsDeliveryAddressUnique { get; set; }
        public Customer DeliverCustomer { get; set; }
        public SellerDTO Seller { get; set; }
        public List<OrderRow> OrderRows { get; set; }
        public List<OrderRowText> OrderRowZeroTexts { get; set; }
        public string InvoiceCustomerName { get; set; }
        public string InvoiceCustomerAddress1 { get; set; }
        public string InvoiceCustomerAddress2 { get; set; }
        public string InvoiceCustomerAddress3 { get; set; }
        public string InvoiceCustomerZipCode { get; set; }
        public string InvoiceCustomerCity { get; set; }
        public string InvoiceCustomerCountryCode { get; set; }
        public string InvoiceCustomerVATId { get; set; }
        public string ShippingNotification { get; set; }
        public string ShippingNotificationType { get; set; }
        public string Freight7 { get; set; }

    }
   
    public partial class OrderRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string AccountId { get; set; }
        public string CostPlaceId { get; set; }
        public string ProjectId { get; set; }
        public string ExtraId { get; set; }
        public string Unit { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string MTF { get; set; }
        public string BLF { get; set; }
        public string MDF { get; set; }
        public string BDF { get; set; }
        public string X1F { get; set; }
        public string X2F { get; set; }
        public decimal? NX1 { get; set; }
        public decimal? DIM { get; set; }
        public string MKT { get; set; }
        public string DeliverState { get; set; }
        public string PurchaseState { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string FixedCostPrice { get; set; }
        public string RowType { get; set; }
        public string AmountState { get; set; }
        public bool UseOHDiscount { get; set; }
        public string DiscountId { get; set; }
        public string DiscountPercentage { get; set; }
        public string Origin { get; set; }
        public string VatId { get; set; }
        public string ReservedId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Price { get; set; }
        public decimal? DeliveredAmount { get; set; }
        public decimal? DiscardAmount { get; set; }
        public decimal? GrossPrice { get; set; }
        public decimal? CostPrice { get; set; }
        public string StatisticState { get; set; }
        public int? PriceDecimalCount { get; set; }
        public int? AmountDecimalCount { get; set; }
        public string BRA { get; set; }
        public string HRF { get; set; }
        public string KTR { get; set; }
        public string PackageProduct { get; set; }
        public decimal? AmountToDeliver { get; set; }
        public Product Product { get; set; }
        public List<OrderRowText> Textrows { get; set; }
        public List<MaterialRow> MaterialRows { get; set; } 
        public decimal RowValue { get; set; }
        public decimal NetPrice { get; set; }
        public bool ManuallyAdded { get; set; }
        public string DeliverLogText { get; set; }
    }

    public partial class OrderRowText
    {
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string TextRowNo { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string DeliverNoteState { get; set; }
        public string InvoiceState { get; set; }
        public string Text { get; set; }
    }

    public partial class Transport
    {
        public string DeliverNo { get; set; }
        public string OrderNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string ShipmentNo { get; set; }
        public string PackageId { get; set; }
        /// <summary>
        /// This is KID field in HKE ("KolliID")
        /// </summary>
        public string SenderReference { get; set; }
        public string OptionalInfo { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string DeliverMark { get; set; }
        public string DeliverMarkOH { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalVolume { get; set; }
        public decimal TotalFlak { get; set; }
        public decimal PackageCount { get; set; }
        public string ModeOfTransportNo { get; set; }
        public string DeliverWayId { get; set; }
        public string DeliverWayDescription { get; set; }
        public string DeliverTermId { get; set; }
        public string DeliverTermDescription { get; set; }
        public string InternalDeliverMessage { get; set; }
        public string DeliverAddress1 { get; set; }
        public string DeliverAddress2 { get; set; }
        public string DeliverZipCity { get; set; }
        public string DeliverCountry { get; set; }
        public string PackageType1 { get; set; }
        public string PackageType2 { get; set; }
        public string PackageType3 { get; set; }
        public decimal? PackageAmount1 { get; set; }
        public decimal? PackageAmount2 { get; set; }
        public decimal? PackageAmount3 { get; set; }
        public decimal? Volume1 { get; set; }
        public decimal? Volume2 { get; set; }
        public decimal? Volume3 { get; set; }
        public decimal? Weight1 { get; set; }
        public decimal? Weight2 { get; set; }
        public decimal? Weight3 { get; set; }
        public string DeliverAnnounce { get; set; }
        public string DeliverAnnounceType { get; set; }
        public string DeliverAnnouncePhone { get; set; }
        public string OurReference { get; set; }
        public string YoureReferenceName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string GoodsMeasurementType { get; set; }
        public string GoodsMark { get; set; }
        public string InvoiceState { get; set; }
        public string DeliveryNoteState { get; set; }
        public string DeliveryState { get; set; }
        public string X1F { get; set; }
        public string TransportTime { get; set; }
        public string TransportDocumentState { get; set; }
        public string PrinterUsername { get; set; }
        public string PrinterTerminal { get; set; }
        
       


        public List<ModeOfTransport> ModeOfTransportList { get; set; }
        public List<TransportTerm> TransportTermList { get; set; }
    }

    public partial class ModeOfTransport
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public partial class TransportTerm
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
    
}
