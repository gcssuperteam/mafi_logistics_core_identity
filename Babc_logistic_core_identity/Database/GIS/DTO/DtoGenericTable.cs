﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class FieldObject
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }

    public partial class GetTableValueRequest
    {
        public string TableName { get; set; }
        public string Field { get; set; }
        public int Index { get; set; }
        public string Key { get; set; }
    }

    public partial class GeneralTableUpdateResult
    {
        public string Table { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Message { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class UpdateTableIndexObject
    {
        public string TableName { get; set; }
        public List<FieldObject> Fields { get; set; }
        public List<FieldObject> IndexFields { get; set; }
        public string Key { get; set; }
    }

    public partial class UpdateTableRangeObject
    {
        public string TableName { get; set; }
        public List<FieldObject> Fields { get; set; }
        public string RangeFrom { get; set; }
        public string RangeTo { get; set; }
    }


}
