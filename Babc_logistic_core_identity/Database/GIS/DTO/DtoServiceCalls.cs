﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class DeliverResult
    {
        public bool Succeeded { get; set; }
        public string OrderNo { get; set; }
        public string DeliverNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string InternalDeliverMessage { get; set; }
        public string OHDeliverState { get; set; }

        public List<OrderRow> DeliveredRows { get; set; }
    }

    public partial class BackDeliverRowParam
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string DeliverNote { get; set; }
        public bool BackDeliverOnMaterialRow { get; set; }
    }

    public partial class DeliverRowParam
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string WarehouseNo { get; set; }
        public string DeliverState { get; set; }
        public decimal AmountToDeliver { get; set; }
        public string DeliverNote { get; set; }
        public bool DeliverMaterialRow { get; set; }
        public bool HandleOperationRow { get; set; }
        public string DeliverDate { get; set; }
        public bool SetDeliverFlagFromPaymentTerms { get; set; }
        public bool? SetWarehouseOnOrderrow { get; set; }
        public string User { get; set; }

    }

    public partial class MaterialOrderListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string Filter { get; set; }
        public string DeliverDateFrom { get; set; }
        public string DeliverDateTo { get; set; }
        public bool WithRows { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public bool WithProduct { get; set; }
        public bool OnlyStarteble { get; set; }
        public CacheParam Cache { get; set; }
    }

    public class CacheParam
    {
        public string CacheId { get; set; }
        public bool UpdateTo { get; set; }
        public bool ReadFrom { get; set; }
        public int ExpirationInMinutes { get; set; }
        public bool UseInMemoryCache { get; set; }
        public bool CheckConsistency { get; set; }
    }

    public partial class OrderListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string OrderSerie { get; set; }
        public string Filter { get; set; }
        public bool WithRows { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public bool IncludeDeliveredRows { get; set; }
        public bool ReadByFile { get; set; }
        public CacheParam Cache { get; set; }
    }

    public partial class PickListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public int IndexCount { get; set; }
        public string OrderSerie { get; set; }
        public string Filter { get; set; }
        public string AdditionalFilter { get; set; }
        public string OGRFilter { get; set; }
        public bool WithRows { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public bool IncludeDeliveredRows { get; set; }
        public bool ExcludeNonWorkingDays { get; set; }
        public bool IncludeDeliverCustomer { get; set; }
        public bool IncludeInvoiceCustomer { get; set; }
        public bool ReadByFile { get; set; }
        public bool IncludeProduct { get; set; }
        public CacheParam Cache { get; set; }
    }

    public class FilterDTO
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Filter { get; set; }
        public bool UseCache { get; set; }
        public string CacheName { get; set; }
    }

    public partial class InventoryResult
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string PlaceNo { get; set; }
        public string PlaceName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Terminal { get; set; }
        public string Note { get; set; }
        public decimal OldAmount { get; set; }
        public decimal NewAmount { get; set; }
        public decimal Amount { get; set; }
        public string InternalMoveMessage { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class InventoryParam
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string PlaceNo { get; set; }
        public string PlaceName { get; set; }
        public string Date { get; set; }
        public string User { get; set; }
        public string Time { get; set; }
        // Rating == "Kuranskod"
        public string Rating { get; set; }
        public string Terminal { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
    }

    public partial class MoveParam
    {
        public string ProductNo { get; set; }
        public string MoveFrom { get; set; }
        public string MoveTo { get; set; }
        public decimal Amount { get; set; }
    }

    public partial class MoveResult
    {
        public string ProductNo { get; set; }
        public string MoveFrom { get; set; }
        public string MoveTo { get; set; }
        public decimal Amount { get; set; }
        public string InternalMoveMessage { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class SupplierListParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string Filter { get; set; }
        public string GetIbanFromField { get; set; }
        public string GetVatFromField { get; set; }
        public int MaxCount { get; set; }
    }

    public partial class PurchaseDeliverRowParam
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public decimal Amount { get; set; }
        public string WarehouseNo { get; set; }
        public string DeliverState { get; set; }
        public string DeliverNoteNo { get; set; }
        public string DeliverDate { get; set; }
    }

    public partial class InventoryFileParam
    {
        public string SaveToFile { get; set; }
        public bool ReturnInventoryFile { get; set; }
        public List<InventoryParam> Inventorys { get; set; }
    }

    public partial class InventoryFileResult
    {
        public string InventoryFile { get; set; }
    }

    public partial class ProductParam
    {
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public int UseIndex { get; set; }
    }

    public partial class UpdateResult
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string InternalMoveMessage { get; set; }
        public bool Succeeded { get; set; }
    }

    public class SingleResult<T>
    {
        public int Errorcode { get; set; }
        public string ErrorMessage { get; set; }
        public Exception ThrownException { get; set; }
        public Exception ServiceException { get; set; }
        public bool Succeeded { get; set; }
        public T Result { get; set; }
    }

    public class GetShipmentParam
    {
        public string DeliverNote { get; set; }
        public string GarpUser { get; set; }
        public string GarpTerminal { get; set; }
    }

    public class OrderResult<T>
    {
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        public bool Succeeded { get; set; }
        public T Result { get; set; }
        public List<OrderRowResult> RowResult { get; set; }
    }

    public class OrderRowResult
    {
        public string Message { get; set; }
        public OrderRow Row { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class MaterialPickListParam
    {
        public string IndexFilter { get; set; }
        public string Filter { get; set; }
        public string DeliverDateFrom { get; set; }
        public string DeliverDateTo { get; set; }
        public string ProductPlaceFrom { get; set; }
        public string ProductPlaceTo { get; set; }
        public int MaxCount { get; set; }
        public bool ReverseReading { get; set; }
        public CacheParam Cache { get; set; }
    }

}
