﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class ERPReport
    {
        public string DocGen { set; get; }
        public string Report { set; get; }
        public string Date { set; get; }
        public string IndexFrom { set; get; }
        public string IndexTo { set; get; }
        public string Medium { set; get; }
        public string Form { set; get; }
        public string FilePath { set; get; }
        public bool Wait { set; get; }
        public int WaitTimeoutSec { set; get; }
        public bool Collate { set; get; }
        public bool DirectPrint { set; get; }
        public int Copies { set; get; }
        public List<ERPReportDialog> Dialogs { get; set; }
    }

    public class ERPReportDialog
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}
