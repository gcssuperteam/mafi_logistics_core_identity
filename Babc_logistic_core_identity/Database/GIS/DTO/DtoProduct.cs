﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Babc_logistic_core.GIS.DTO
{
    public partial class Product
    {
        public string VariantCode { get; set; }
        public string HVL { get; set; }
        public string MVL { get; set; }
        public int Id { get; set; }                 //Id är ett tillagt värde och finns inte i GARP
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public string StockUpdateType { get; set; }
        public string ProductType { get; set; }
        public decimal? AmountPerUnit { get; set; }
        public decimal? PurchasePrice { get; set; }
        public decimal? Price { get; set; }
        public decimal? ExtraPrice1 { get; set; }
        public decimal? ExtraPrice2 { get; set; }
        public decimal? AverageCostPrice { get; set; }
        public string Season { get; set; }
        public string DeliveryCode { get; set; }
        public string OriginType { get; set; }
        public decimal? Stock { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Volyme { get; set; }
        public string WarehouseNo { get; set; }
        public string WarehouseType { get; set; }
        public int? AmountDecimalCount { get; set; }
        public int? PriceDecimalCount { get; set; }
        public string ProductGroup1 { get; set; }
        public string ProductGroup2 { get; set; }
        public string ProductGroup3 { get; set; }
        public string ProductGroup4 { get; set; }
        public string ProductGroup5 { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        public string Code4 { get; set; }
        public string Code5 { get; set; }
        public string Code6 { get; set; }
        public string Category { get; set; }
        public string PurchaseAccount { get; set; }
        public string SalesAccountId { get; set; }
        public string SalesAccount { get; set; }
        public string WOB { get; set; }
        public string Dimension { get; set; }
        public string Variant { get; set; }
        public string CountryOfOrigin { get; set; }
        public string VATCode { get; set; }
        public string PriceModelCode { get; set; }
        public string DiscountSalesCode { get; set; }
        public string DiscountPurchaseCode { get; set; }
        public string PurchaseCurrencyCode { get; set; }
        public string MaterialAccount { get; set; }
        public string SaleableCode { get; set; }
        public string OrderQuantity { get; set; }
        public string LeadTime { get; set; }
        public string OrderPoint { get; set; }
        public decimal? StandardPrice { get; set; }
        public string ProductRange1 { get; set; }
        public string ProductRange2 { get; set; }
        public string CompanyId { get; set; }
        public string SupplierNo { get; set; }
        public string SuppliersProductNo { get; set; }
        public string WarehouseAccountCode { get; set; }
        public string Trademark { get; set; }       //Nytt fält - Varumärke
        public List<Text> ProductText { get; set; }
        public List<WarehouseNumber> WarehouseList { get; set; }
        public string VariantDescription { get; set; }
        public string DimensionDescription { get; set; }
        public decimal? QuantityInOrder { get; set; }
        public decimal? QuantityInPurchase { get; set; }
        public decimal? QuantityInOrderReserved { get; set; }
        public decimal? QuantityInPurchaseReserved { get; set; }
        public decimal? DeliveredInThisYear { get; set; }
        public decimal? DeliveredInLastYear { get; set; }
        public decimal? DeliveredOutThisYear { get; set; }
        public decimal? DeliveredOutLastYear { get; set; }
        public string LastMovementDate { get; set; }
    }

    public partial class ProductModel
    {
        public string ModelNo { get; set; }
        public short ModelLenth { get; set; }
        public short VariantLength { get; set; }
        public short DimensionLength { get; set; }
        public List<ProductVariant> VariantList { get; set; }
        public Product ProductObj { get; set; }
    }


    public partial class ProductVariant
    {
        public string ModelNo { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public short ModelLenth { get; set; }
        public short VariantLength { get; set; }
        public short DimensionLength { get; set; }
        public List<Text> ProductText { get; set; }
        public List<ProductDimension> DimensionList { get; set; }
        public Product ProductObj { get; set; }
    }

    public partial class ProductDimension
    {
        public string ModelNo { get; set; }
        public string VariantCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public short ModelLenth { get; set; }
        public short VariantLength { get; set; }
        public short DimensionLength { get; set; }
        public Product ProductObj { get; set; }
        public bool Active { get; set; }
        public string VariantDescription { get; set; }
    }

    public partial class Text
    {
        public string Field { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }

    public class ProductLocationParam
    {
        public string IndexFilter { get; set; }
        public string GeneralFilter { get; set; }
        public bool StockOnly { get; set; }
    }

    public partial class WarehouseNumber
    {
        public string Id { get; set; }
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string OrderPoint { get; set; }
        public string VIP { get; set; }
        public decimal? OrderQuantity { get; set; }
        public decimal? QuantityInPurchase { get; set; }
        public decimal? QuantityInOrder { get; set; }
        public decimal Stock { get; set; }
        public string Company { get; set; }
        public string Account { get; set; }
        public string LastReduceDate { get; set; }
        public string WarehouseLocation { get; set; }
        public string CreationDate { get; set; }
        public decimal? QuantityInOrderReserved { get; set; }
        public decimal? QuantityInPurchaseReserved { get; set; }
        public string LeadTime { get; set; }
        public string BatchNo { get; set; }
        public string SekIdxFlg { get; set; }
        public string SaleableCode { get; set; }
        public int? PriceDecimalCount { get; set; }
        public int? AmountDecimalCount { get; set; }
        public ProductPlace Place { get; set; }
    }

    public partial class ProductLocation
    {
        public string ProductNo { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string SaleableCode { get; set; }
        public string LastReduceDate { get; set; }
        public string LastReadInDate { get; set; }
        public string LastMovmentDate { get; set; }
        public string LastInventoryDate { get; set; }
        public int? AmountDecimalCount { get; set; }
        public int? PriceDecimalCount { get; set; }
        public decimal? QuantityInOrderReserved { get; set; }
        public decimal? QuantityInPurchaseReserved { get; set; }
        public decimal? QuantityInOrder { get; set; }
        public decimal QuantityInPurchase { get; set; }
        public decimal Stock { get; set; }
        public decimal? DeliveredInThisYear { get; set; }
        public decimal? DeliveredInLastYear { get; set; }
        public decimal? DeliveredOutThisYear { get; set; }
        public decimal? DeliveredOutLastYear { get; set; }
        public decimal? QtyNotControlled { get; set; }
        public decimal? RecordQty { get; set; }
        public string WarehouseNoSingle { get; set; }
        public string BatchNoSingle { get; set; }
        public List<WarehouseNumber> WarehouseList { get; set; }
    }

    public partial class ProductPlace
    {
        public string Place { get; set; }
        public string Zone { get; set; }
    }


    public partial class ProductPlaceWithWarehouse : ProductPlace
    {

        public List<WarehouseNumber> WarehouseNumbers { get; set; }
        public bool? IsFree { get; set; }

    }
}
