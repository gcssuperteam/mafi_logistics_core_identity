﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Database
{
    public static class InventoryLib
    {

        public static InventorySet AddInventorySet(InventorySet set)
        {
            InventorySet result = null;

            try
			{
                using(var db = new InventorySetContext())
                {
                    db.InventorySets.Add(set);
                    db.SaveChanges();

                    result = set;
                }
			}
			catch (Exception e)
			{

			}

            return result;
        }

        public static InventorySet UpdateInventorySet(InventorySet set)
        {
            InventorySet result = null;

            try
            {
                using (var db = new InventorySetContext())
                {
                    db.Entry(set).State = EntityState.Modified;
                    db.SaveChanges();

                    result = set;
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static InventorySet DeleteInventorySet(InventorySet set)
        {
            InventorySet result = null;

            try
            {
                using (var db = new InventorySetContext())
                {
                    db.InventorySets.Remove(set);
                    db.SaveChanges();

                    result = set;
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static void AddInventoryBulk(List<Inventory> invList)
        {
            try
            {
                using (var db = new InventorySetContext())
                {
                    db.Inventorys.AddRange(invList);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

            }
        }

        public static InventorySet GetInventorySet(int id)
        {
            InventorySet result = null;
            try
            {
                using (var db = new InventorySetContext())
                {
                    result = db.InventorySets.Find(id);
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static List<InventorySet> GetAllInventorySet()
        {
            List<InventorySet> result = new List<InventorySet>();
            try
            {
                using (var db = new InventorySetContext())
                {
                    result = db.InventorySets.ToList();
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static Inventory GetInventory(int id)
        {
            Inventory result = null;
            try
            {
                using (var db = new InventorySetContext())
                {
                    result = db.Inventorys.Find(id);
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static List<Inventory> GetAllInventorOnSet(int set_id)
        {
            List<Inventory> result = null;

            try
            {
                using (var db = new InventorySetContext())
                {
                    result = db.Inventorys?.Where(s=>s.InventorySetId == set_id)?.ToList();
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static bool DeleteAllInventorOnSet(int set_id)
        {
            bool result = false;

            try
            {
                using (var db = new InventorySetContext())
                {
                    var remove = db.Inventorys?.Where(s => s.InventorySetId == set_id);
                    db.Inventorys.RemoveRange(remove);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (Exception e)
            {
                result = false;
            }

            return result;
        }

        public static Inventory DeleteInventory(Inventory set)
        {
            Inventory result = null;

            try
            {
                using (var db = new InventorySetContext())
                {
                    db.Inventorys.Remove(set);
                    db.SaveChanges();

                    result = set;
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }
    }
}
