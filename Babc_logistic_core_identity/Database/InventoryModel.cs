﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Database
{
    public class InventorySetContext : DbContext
    {
        public DbSet<Inventory> Inventorys { get; set; }
        public DbSet<InventorySet> InventorySets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=inventory.db");
    }

    public class InventorySet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        public string Description { get; set; }
        public string WarehouseNo { get; set; }
        public string LocationId { get; set; }
        public string LocationName { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<Inventory> InventoryList { get; set; }
    }

    public partial class Inventory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public string WarehouseNo { get; set; }
        public string Place { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public decimal OriginalAmount { get; set; }
        public DateTime AmountAtTime { get; set; }
        public decimal VIPAtTime { get; set; }
        public bool InventoryDone { get; set; }
        [ForeignKey("InventorySet")]
        public int InventorySetId { get; set; }
    }

}
