﻿using Babc_logistic_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Models
{
    public class PurchaseListVM
    {
        public List<PurchaseOrderHead> OrderHeads { get; set; }
        public string search_order { get; set; }
        public string search_supplierName { get; set; }
    }
}
