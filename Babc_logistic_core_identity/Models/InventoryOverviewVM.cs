﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Babc_logistic_core.Models
{
    public class InventoryOverviewVM
    {
        public string ProductNo { get; set; }
        public string Warehouse { get; set; }
        //public string Place { get; set; }
        //public string FirstMovmentDate { get; set; }
        public string Message { get; set; }
        public decimal Amount { get; set; }
        public DateTime InventoryDate { get; set; }
        public List<InventorySet> InventorySetList { get; set; }
        public InventorySet CurrentInventorySet { get; set; }
        public int CurrentInventorySetId { get; set; }
        public List<Inventory> InventoryList { get; set; }
        public List<ProductPlaceWithWarehouse> ProductPlaceList { get; set; }
    }

    public class InventorySumVM
    {
        public string ProductNo { get; set; }
        public string Warehouse { get; set; }
        public decimal Amount { get; set; }
        public string InventoryDate { get; set; }
        public List<InventoryDetail> ContainingInventories { get; set; }
    }

    public partial class InventoryDetail
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public decimal OriginalAmount { get; set; }
        public DateTime AmountAtTime { get; set; }
        public decimal VIPAtTime { get; set; }
        public bool InventoryDone { get; set; }
    }

    /// <summary>
    /// Parameter to initialze creation of an new InventoreySet
    /// </summary>
    public class CreateInventorySetVM
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string WarehousNo { get; set; }
        public string Message { get; set; }
        public InventorySet Result { get; set; }
    }
}
