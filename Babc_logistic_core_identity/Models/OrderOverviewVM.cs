﻿using Babc_logistic_core.GIS.DTO;
using System.Collections.Generic;

namespace Babc_logistic_core.Models
{
    public class OrderOverviewVM
    {
        public OrderOverviewVM()
        {
        }

        public Day Day_0 { get; set; }
        public Day Day_1 { get; set; }
        public Day Day_2 { get; set; }
        public Day Day_3 { get; set; }
        public Day Day_4 { get; set; }
        public Day Day_5 { get; set; }
    }

    public class Day
    {
        public Day()
        {
            Orders = new List<OrderHead>();
        }

        public List<OrderHead> Orders { get; set; }
        public string DayName { get; set; }
        public int DayNumber { get; set; }
        public string DayDate { get; set; }
    }
}
