﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Models
{
    public class InventoryFileVM
    {
        public string Filename { get; set; }
        public int SetID { get; set; }
        public string SetName { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
