﻿using Babc_logistic_core.GIS.DTO;
using BABC_Logistics_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BABC_Logistics_core.Models
{
    public class TransportVM
    {
        public Transport Transport { get; set; }
        public LogtradeShipment Shipment { get; set; }
        public string Season { get; set; }
        public List<BaseTable> DeliverWayList { get; set; }
        public TimedUnloading TimedUnloading { get; set; }
        public decimal NetWeight { get; set; }       
        public bool SendMail { get; set; } = false;
        public bool SaveShipment { get; set; } = false;
        public string UOM { get; set; }
        public bool AddPackageInformation { get; set; } = false;

    }
    public class SpecialFreight
    {
        public List<FreightExceptions> Customers { get; set; }
    }
    public class FreightExceptions
    {
        public string Customer { get; set; } = "";
        public string GisToken { get; set; } = "";
        public string OrderMark { get; set; } = "";
        public string PurchaseOrderLineMark { get; set; } = "";
    }
}


namespace Babc_logistic_core.GIS.DTO
{
    public partial class Transport
    {
        public decimal Qty1 { get; set; }
        public decimal NetWeight1 { get; set; }
        public decimal Length1 { get; set; }
        public decimal Width1 { get; set; }
        public decimal Height1 { get; set; }

        public decimal Qty2 { get; set; }
        public decimal NetWeight2 { get; set; }
        public decimal Length2 { get; set; }
        public decimal Width2 { get; set; }
        public decimal Height2 { get; set; }

        public decimal Qty3 { get; set; }
        public decimal NetWeight3 { get; set; }
        public decimal Length3 { get; set; }
        public decimal Width3 { get; set; }
        public decimal Height3 { get; set; }
    }

    public class TimedUnloading
    {
        public DateTime Date { get; set; }
        public DateTime TimeFrom { get; set; }
        public DateTime TimeTo { get; set; }

    }
}

