﻿using Babc_logistic_core.GIS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Models
{
    public class WarehouseMoveVM
    {
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string MaterialRow { get; set; }
        public string OperationNo { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNoFrom { get; set; }
        public string WarehouseNoTo { get; set; }
        public string Amount { get; set; }
        public List<WarehouseNumber> WarehouseList { get; set; }
    }
}
