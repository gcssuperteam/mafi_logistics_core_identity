﻿using Babc_logistic_core.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Models
{
    public class ProductsNotInSetVM
    {
        public InventorySet Set { get; set; }
        public List<MissingProduct> ProductList { get; set; }
    }

    public class MissingProduct
    {
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Place { get; set; }
        public string WarehouseNo { get; set; }
        public decimal Stock { get; set; }
    }
}
