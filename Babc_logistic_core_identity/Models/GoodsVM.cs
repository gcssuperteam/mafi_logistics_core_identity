﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GDN.Models
{
    public class GoodsViewModel
    {
        public string OrderNo { get; set; }
        public string DeliverNoteNo { get; set; }
        public string PackageCount { get; set; }
        public string TransferDate { get; set; }
    }
}