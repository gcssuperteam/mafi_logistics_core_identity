﻿using Babc_logistic_core.Controllers;
using Babc_logistic_core.GIS.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.Models
{
    public class OrderViewModel
    {
        public OrderHead Order;
        public List<CompactOrderRow> Rows;
        public List<Product> SelectableProducts;
        public bool LogtextAddedToday; 
        public List<DeliverBatchParam> DeliverBatchParams;
        public List<string> LogTexts { get; set; }
        //public List<string> additionalOrders;
        //public List<MaterialRow> materials;
    }

    public class PurchaseOrderViewModel
    {
        public PurchaseOrderHead Order;
        public List<Product> SelectableProducts;
        //public List<string> additionalOrders;
        //public List<MaterialRow> materials;
    }
    public class OrderViewVM
    {
        public OrderViewVM()
        {
            Orderrows = new List<OrderViewRow>();

        }

        public List<OrderViewRow> Orderrows { get; set; }
    }
    public class CompactOrderRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string ProductStockUpdateType { get; set; }
        public string WarehouseType { get; set; }
        public string MaterialPlanFlag { get; set; }
        public decimal Amount { get; set; }
        public decimal DeliveredAmount { get; set; }
        public decimal AmountToDeliver { get; set; }
        public string Unit { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string DeliverState { get; set; }
        public string WarehouseNo { get; set; }
        public string RowType { get; set; }
        public string MDF { get; set; }
        public List<Text> ProductText { get; set; }
        public List<CompactWarehouse> WarehouseList { get; set; }
        public CompactWarehouse SelectedWarehouse { get; set; }
        public List<OrderRowText> TextRows { get; set; }
        public bool ManuallyAdded { get; set; }
        public string Origin { get; set; }
        public string PrintLabel { get; set; }
        public string CustomerNo { get; set; }
        public string OhText2 { get; set; }
    }

    public class CompactWarehouse
    {
        public string Id { get; set; }
        public string Location { get; set; } // Place in Garp
        public string Zone { get; set; } // Place in Garp
        public string LastDate { get; set; }
        public DateTime LastDateAsDate { get; set; }
        public decimal Amount { get; set; }
        public bool IsSelected { get; set; }
        public string SaleableCode { get; set; }
    }
    public class OrderViewRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string CustomerNo { get; set; }

        public string CustomerName { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }

        public decimal Amount { get; set; }
        public string DeliverDate { get; set; }

    }
    public class OperationViewRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string DeliverDate { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public decimal RemainingAmount { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string SupplierNo { get; set; }
        public string PurchaserCode { get; set; }
        public string PurchaserName { get; set; }
        public string Ourreference { get; set; }
        public string MaterialFlag { get; set; }
        public string ProductionFlag { get; set; }
        public bool IsNonStockProduct { get; set; }
        public bool IsRemainingAmountInStock { get; set; }
        public string PickListFlag { get; set; }
        public string DeliverWayCode { get; set; }
        public string DeliverTerms { get; set; }
        public string DeliverTermsDescription { get; set; }
        public bool IsRowDone { get; set; }
    }
}
