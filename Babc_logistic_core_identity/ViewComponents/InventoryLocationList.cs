﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.ViewComponents
{
    public class InventoryLocationList : ViewComponent
    {

        IConfiguration _configuration;
        private UserManager<ApplicationUser> _userManager;
        public InventoryLocationList(IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            _configuration = config;
            _userManager = userManager;
        }
 
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = this.HttpContext.User;
            InventoryLocationListModel result = new InventoryLocationListModel();
            WarehouseModel mWM = new WarehouseModel(_configuration, _userManager.GetUserAsync(user)?.Result);
            try
            {

                result.ProductPlaceList = mWM.GetWarehouseLocationList("*", true);
            }
            catch(Exception e)
            {

            }
            return View(result);
        }
    }

    public class InventoryLocationListModel
    {
        public InventoryLocationListModel()
        {
              ProductPlaceList = new List<ProductPlaceWithWarehouse>();
 
        }
        public List<ProductPlaceWithWarehouse> ProductPlaceList { get; set; }   

    }
}
