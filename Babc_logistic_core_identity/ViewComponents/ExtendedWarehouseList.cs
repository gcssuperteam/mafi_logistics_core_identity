﻿using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Babc_logistic_core.ViewComponents
{
    public class ExtendedWarehouseList : ViewComponent
    {
        IConfiguration _configuration;
        private UserManager<ApplicationUser> _userManager;
        public ExtendedWarehouseList(IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            _configuration = config;
            _userManager = userManager;
        }
        public async Task<IViewComponentResult> InvokeAsync(string productno, string warehouseno, string id)
        {
            ExtendedWarehouseListModel result = new ExtendedWarehouseListModel();
            try
            {
                var user = this.HttpContext.User;
                Babc_logistic_core.GIS.Model.ProductModel mPRM = new Babc_logistic_core.GIS.Model.ProductModel(_configuration, _userManager.GetUserAsync(user)?.Result);
                result.Id = id;
                Product product = mPRM.GetProductById(productno);
                foreach (var wh in product.WarehouseList)
                {
                    if (!string.IsNullOrEmpty(wh.WarehouseLocation))
                    {
                        // Only places belonging to current location
                        if (wh.WarehouseLocation.StartsWith(warehouseno) && wh.Stock > 0)
                        {
                            CompactWarehouse cor = new CompactWarehouse();
                            result.WarehouseList.Add(new CompactWarehouse
                            {
                                Id = wh.Id,
                                Location = wh.WarehouseLocation,
                                Zone = wh.Place?.Zone,
                                Amount = wh.Stock,
                                LastDate = wh.Description2,
                                LastDateAsDate = GeneralFunctions.getDateFromStr(wh.Description2),
                                IsSelected = false,
                                SaleableCode = wh.SaleableCode

                            });
                         
                        }
                    }
                    else
                    {
                        if (product.WarehouseType == "1" && wh.Id == warehouseno)
                        {
                            CompactWarehouse cor = new CompactWarehouse();
                            result.WarehouseList.Add(new CompactWarehouse
                            {
                                Id = wh.Id,
                                Amount = wh.Stock,
                                IsSelected = false,
                                SaleableCode = wh.SaleableCode
                            });
                        }
                    }
                }
                if (result.WarehouseList.Count > 0)
                {
                    // If extended warehouse or ordinary
                    if (product.WarehouseType == "2")
                    {
                        result.WarehouseList.Where(s => s.Amount > 0).OrderBy(d => d.LastDateAsDate).First().IsSelected = true;
                        result.SelectedWarehouse = result.WarehouseList.Where(s => s.Amount > 0).OrderBy(d => d.LastDateAsDate).First();
                    }
                    else
                    {
                        result.WarehouseList.First().IsSelected = true;
                        result.SelectedWarehouse = result.WarehouseList.First();
                    }
                }
            }
            catch (Exception e)
            {
            }
            return View(result);
        }
    }
    public class ExtendedWarehouseListModel
    {
        public ExtendedWarehouseListModel()
        {
            this.WarehouseList = new List<CompactWarehouse>();
        }
        public CompactWarehouse SelectedWarehouse { get; set; }
        public string Id { get; set; }
        public List<CompactWarehouse> WarehouseList { get; set; }
    }
}