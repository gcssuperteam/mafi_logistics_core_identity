using Babc_logistic_core.Database;
using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.GIS.Model;
using Babc_logistic_core.Helpers;
using Babc_logistic_core.Models;
using BABC_Logistics_core.GIS.DTO;
using BABC_Logistics_core.GIS.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Babc_logistic_core.ViewComponents
{
    public class TAPackageSelector : ViewComponent
    {
        //private OrderModel mOM;
        //private BaseDataModel mBDM;
        private UserManager<ApplicationUser> _userManager;
        IConfiguration _configuration;

        private readonly IMemoryCache _memoryCache;


        public TAPackageSelector(IMemoryCache cache, IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            _configuration = config;
            _memoryCache = cache;
            _userManager = userManager;

        }
        private MemoryCacheEntryOptions _cacheExpiryOptions = null;
        public async Task<IViewComponentResult> InvokeAsync(PackageItem pkg, string delivernote, string controller, string add_action, string delete_action, string copy_action, string update_action)
        {
            PackageItem result = new PackageItem();

            try
            {
                ViewBag.Controller = controller;
                ViewBag.AddAction = add_action;
                ViewBag.DeleteAction = delete_action;
                ViewBag.CopyAction = copy_action;
                ViewBag.UpdateAction = update_action;

                ViewBag.PackageTypeList = GetPackageTypeList();

                result.DeliverNoteNo = delivernote;

               //_cacheExpiryOptions = new MemoryCacheEntryOptions
               //{
               //   AbsoluteExpiration = DateTime.Now.AddMinutes(60),
               //   Priority = CacheItemPriority.High
               //};

                UpdateTransport(delivernote, pkg);

                if (!string.IsNullOrEmpty(delivernote))
                {
                    var packages = await GetItemsAsync(delivernote);




                    if (packages.Count > 0)
                    {
                        //_memoryCache.Set(delivernote, packages, _cacheExpiryOptions);
            
                        ViewBag.AddedPackageList = packages;
                    }
                }

                //result.PackageType = DateTime.Now.ToString("HH:mm:ss");

            }
            catch (Exception e)
            {

            }

            return View(result);
        }

        private async Task<List<PackageItem>> GetItemsAsync(string delivernote)
        {
            List<PackageItem> result = new List<PackageItem>();

            try
            {
                /*
                var user = this.HttpContext.User;
                OrderModel mOM = new OrderModel(_configuration, _userManager.GetUserAsync(user)?.Result);

                 var delNote = mOM.GetTransportForDeliverNote(delivernote);
                if (delNote != null)
                {
                result = getPackageItemListFromTransport(delNote);
                }
                */
                result = getPackageItemListFromGarpTable(delivernote);
            }
            catch (Exception e)
            {

            }

            return result;
        }

        private List<PackageItem> getPackageItemListFromGarpTable(string delivernote)
        {

            List<PackageItem> result = new List<PackageItem>();
            List<LogtradeGoodsItem> result_LT = new List<LogtradeGoodsItem>();
            var user = this.HttpContext.User;
            LogtradeModel logtradeModel = new LogtradeModel(_configuration, _userManager.GetUserAsync(user)?.Result);


            try
            {

                var goodsItemListGarp = logtradeModel.GetGarpPackageList(delivernote);

                foreach (var gi in goodsItemListGarp)
                {
                    PackageItem temp = new PackageItem();

                    temp.ID = gi.GarpId;
                    temp.DeliverNoteNo = delivernote;
                    temp.PackageType = gi.PackageTypeCode;
                    temp.Quantity = gi.Amount;
                    temp.Weight = GeneralFunctions.getStrFromDecimal(gi.Weight);
                    if (gi.LoadingMeters > 0)
                    {
                        temp.GoodsMeasurementType = "F";
                    }
                    else
                    {
                        temp.GoodsMeasurementType = "V";
                    }

                    temp.LoadingMeters = GeneralFunctions.getStrFromDecimal(gi.LoadingMeters);

                    temp.Volume = GeneralFunctions.getStrFromDecimal(gi.Volyme);
                    if (gi.PackageItems.Count > 0)
                    {
                        temp.Length = GeneralFunctions.getStrFromDecimal(gi.PackageItems[0].Length);
                        temp.Width = GeneralFunctions.getStrFromDecimal(gi.PackageItems[0].Width);
                        temp.Height = GeneralFunctions.getStrFromDecimal(gi.PackageItems[0].Height);
                        temp.Pieces = gi.PackageItems[0].Pieces;
                    }

                    result.Add(temp);
                }


            }
             
            catch (Exception r)
            {

            }

            return result;
        }




            private List<PackageItem> getPackageItemListFromTransport(Transport transport)
            {
            List<PackageItem> result = new List<PackageItem>();
            //try
            //{
                      
            //   var goodsItemList = getTempDataGoodsItemList(transport.DeliverNo);
                
            //    if (transport.PackageAmount1 > 0)
            //    {
            //        PackageItem pkg1 = new PackageItem();

            //        pkg1.ID = "1";
            //        pkg1.DeliverNoteNo = transport.DeliverNo;
            //        pkg1.PackageType = transport.PackageType1;
            //        pkg1.Quantity = (int)transport.PackageAmount1;
            //        pkg1.Weight = GeneralFunctions.getStrFromDecimal(transport.Weight1.Value);
            //        pkg1.GoodsMeasurementType = transport.GoodsMeasurementType;
            //        if(transport.GoodsMeasurementType=="F" && transport.Height1 == 0 && transport.Width1 == 0 && transport.Length1 == 0)
            //        {
            //            pkg1.LoadingMeters = GeneralFunctions.getStrFromDecimal(transport.Volume1.Value);
            //        }
            //        else
            //        {
            //            pkg1.Volume = GeneralFunctions.getStrFromDecimal(transport.Volume1.Value);
            //        }

            //        if (goodsItemList?.Count >= 0)
            //        {
            //            pkg1.Length = goodsItemList[0].Length;
            //            pkg1.Width = goodsItemList[0].Width;
            //            pkg1.Height = goodsItemList[0].Height;
            //        }

            //        result.Add(pkg1);
            //    }

            //    if (transport.PackageAmount2 > 0)
            //    {
            //        PackageItem pkg2 = new PackageItem();

            //        pkg2.ID = "2";
            //        pkg2.DeliverNoteNo = transport.DeliverNo;
            //        pkg2.PackageType = transport.PackageType2;
            //        pkg2.Quantity = (int)transport.PackageAmount2;
            //        pkg2.Weight = GeneralFunctions.getStrFromDecimal(transport.Weight2.Value);

            //        pkg2.GoodsMeasurementType = transport.GoodsMeasurementType;
            //        if (transport.GoodsMeasurementType == "F" && transport.Height2 == 0 && transport.Width2 == 0 && transport.Length2 == 0)
            //        {
            //            pkg2.LoadingMeters = GeneralFunctions.getStrFromDecimal(transport.Volume2.Value);
            //        }
            //        else
            //        {
            //            pkg2.Volume = GeneralFunctions.getStrFromDecimal(transport.Volume2.Value);
            //        }
            //        if (goodsItemList?.Count >= 1)
            //        {
            //            pkg2.Length = goodsItemList[1].Length;
            //            pkg2.Width = goodsItemList[1].Width;
            //            pkg2.Height = goodsItemList[1].Height;
            //        }

            //        result.Add(pkg2);
            //    }

            //    if (transport.PackageAmount3 > 0)
            //    {
            //        PackageItem pkg3 = new PackageItem();

            //        pkg3.ID = "3";
            //        pkg3.DeliverNoteNo = transport.DeliverNo;
            //        pkg3.PackageType = transport.PackageType3;
            //        pkg3.Quantity = (int)transport.PackageAmount3;
            //        pkg3.Weight = GeneralFunctions.getStrFromDecimal(transport.Weight3.Value);
            //        pkg3.GoodsMeasurementType = transport.GoodsMeasurementType;
            //        if (transport.GoodsMeasurementType == "F" && transport.Height3 == 0 && transport.Width3 == 0 && transport.Length3 == 0)
            //        {
            //            pkg3.LoadingMeters = GeneralFunctions.getStrFromDecimal(transport.Volume3.Value);
            //        }
            //        else
            //        {
            //            pkg3.Volume = GeneralFunctions.getStrFromDecimal(transport.Volume3.Value);
            //        }
            //        if (goodsItemList?.Count >= 2)
            //        {
            //            pkg3.Length = goodsItemList[2].Length;
            //            pkg3.Width = goodsItemList[2].Width;
            //            pkg3.Height = goodsItemList[2].Height;
            //        }

            //        result.Add(pkg3);
            //    }
                
            //    var tempDataList = getTempDataGoodsItemList(transport.DeliverNo);

            //    // If we have some package saved in Garp we also have them in out tempdata, and then we remove them from tempdate and ads only non existing packeitems
            //   if (tempDataList.Count > 3)
            //   {
            //        for (int i = 3; i < tempDataList.Count; i++)
            //        {
            //            result.Add(tempDataList[i]);
            //        }
            //   }


            //    //if (ViewData.ContainsKey("packageitemlist"))
            //    //{
            //    //    List<PackageItem> list = JsonConvert.DeserializeObject<List<PackageItem>>((ViewData["packageitemlist"] as string));

            //    //    if (list.Count > 0)
            //    //    {
            //    //        for (int i = 0; i < list.Count; i++)
            //    //        {
            //    //            if (i < 2)
            //    //            {
            //    //                if (result.Count >= i)
            //    //                {
            //    //                    result[i].Width = list[i].Width;
            //    //                    result[i].Height = list[i].Height;
            //    //                    result[i].Length = list[i].Length;
            //    //                }
            //    //            }
            //    //            else
            //    //            {
            //    //                result.Add(list[i]);
            //    //            }
            //    //        }
            //    //    }
            //    //}
                
            //}
            //catch (Exception r)
            //{

            //}

            return result;
        }

        private Transport UpdateTransport(string delivernote, PackageItem pkg)
        {
            Transport result = null;

            try
            {
                if (!string.IsNullOrEmpty(delivernote) && pkg != null)
                {
                    var user = this.HttpContext.User;
                    OrderModel mOM = new OrderModel(_configuration, _userManager.GetUserAsync(user)?.Result);

                    result = mOM.GetTransportForDeliverNote(delivernote);

                    if (result.PackageAmount1 == 0)
                    {
                        result.PackageType1 = pkg.PackageType;
                        result.PackageAmount1 = pkg.Quantity;
                        result.Weight1 = GeneralFunctions.getDecimalFromStr(pkg.Weight);
                        result.Volume1 = GeneralFunctions.getDecimalFromStr(pkg.Volume) + GeneralFunctions.getDecimalFromStr(pkg.LoadingMeters);
                        result.Length1 = GeneralFunctions.getDecimalFromStr(pkg.Length);
                        result.Width1 = GeneralFunctions.getDecimalFromStr(pkg.Width);
                        result.Height1 = GeneralFunctions.getDecimalFromStr(pkg.Height);
                        result.GoodsMeasurementType = pkg.GoodsMeasurementType;
                    }
                    else if (result.PackageAmount2 == 0)
                    {
                        result.PackageType2 = pkg.PackageType;
                        result.PackageAmount2 = pkg.Quantity;
                        result.Weight2 = GeneralFunctions.getDecimalFromStr(pkg.Weight);
                        result.Volume2 = GeneralFunctions.getDecimalFromStr(pkg.Volume) + GeneralFunctions.getDecimalFromStr(pkg.LoadingMeters);
                        result.Length2 = GeneralFunctions.getDecimalFromStr(pkg.Length);
                        result.Width2 = GeneralFunctions.getDecimalFromStr(pkg.Width);
                        result.Height2 = GeneralFunctions.getDecimalFromStr(pkg.Height);
                        result.GoodsMeasurementType = pkg.GoodsMeasurementType;
                    }
                    else if (result.PackageAmount3 == 0)
                    {
                        result.PackageType3 = pkg.PackageType;
                        result.PackageAmount3 = pkg.Quantity;
                        result.Weight3 = GeneralFunctions.getDecimalFromStr(pkg.Weight);
                        result.Volume3 = GeneralFunctions.getDecimalFromStr(pkg.Volume) + GeneralFunctions.getDecimalFromStr(pkg.LoadingMeters);
                        result.Length3 = GeneralFunctions.getDecimalFromStr(pkg.Length);
                        result.Width3 = GeneralFunctions.getDecimalFromStr(pkg.Width);
                        result.Height3 = GeneralFunctions.getDecimalFromStr(pkg.Height);
                        result.GoodsMeasurementType = pkg.GoodsMeasurementType;
                    }

                    mOM.UpdateTransport(result);
                }
                




            }
            catch (Exception e)
            {

            }

            return result;
        }

        private List<ModeOfTransport> GetPackageTypeList()
        {
            List<ModeOfTransport> result = new List<ModeOfTransport>();

            try
            {
                var user = this.HttpContext.User;
                BaseDataModel mBDM = new BaseDataModel(_configuration, _userManager.GetUserAsync(user)?.Result);

                List<BaseTable> pkgList = mBDM.GetBaseTableList("1S");

                foreach (BaseTable bt in pkgList)
                {
                    result.Add(new ModeOfTransport { Id = bt.Id, Description = bt.Description });
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        private List<PackageItem> getTempDataGoodsItemList(string delivernote)
        {
            List<PackageItem> result = null;

            //try
            //{
            //    if (_memoryCache.TryGetValue(delivernote, out List<PackageItem> packageList))
            //    {
            //        result = packageList;
            //    }
            //}
            //catch (Exception)
            //{
            //}

            return result;
        }
    }


    public class PackageVM
    {
        public PackageVM()
        {
            AddedPackageList = new List<PackageItem>();
        }

        public string Controller { get; set; }
        public string Action { get; set; }
        public List<PackageItem> PackageTypesList { get; set; }

        public List<PackageItem> AddedPackageList { get; set; }
    }

    public class PackageItem
    {
        public string ID { get; set; }
        public string DeliverNoteNo { get; set; }
        public string PackageType { get; set; }
        public int Quantity { get; set; }
        public string Weight { get; set; }
        public string Volume { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public int? Pieces { get; set; } 
        public string? LoadingMeters { get; set; }
        public string? GoodsMeasurementType { get; set; }
    }
}
