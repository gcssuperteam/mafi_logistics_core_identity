﻿using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.ViewComponents
{
    public class MaterialOrderRowSlice : ViewComponent
    {
        public MaterialOrderRowSlice()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync(MaterialRow or, decimal amount, string warehouse)
        {
            MaterialOrderRowSliceModel result = new MaterialOrderRowSliceModel();
            
            try
            {
                result.OrderNo = or.OrderNo;
                result.RowNo = or.RowNo;
                result.MaterialRowNo = or.MaterialRowNo;
                result.ProductNo = or.ProductNo;
                result.Description = or.ProductDescription;
                result.Description2 = or.ProductDescription2;
                result.AmountRemaining = or.Amount.Value - or.DeliveredAmount.Value;
                result.AmountDelivered = or.DeliveredAmount.Value;
                result.Amount = or.Amount.Value;
                result.ManufacturedAmount = amount;
                result.DeliverState = or.DeliverState;
                result.DeliverDate = or.PreferedDeliverDate;
                result.WarehouseNo = string.IsNullOrEmpty(or.WarehouseNo) ? warehouse : or.WarehouseNo; // If no warehouse is set on materialrow, get it from OGR
                result.ShowBackButton = or.DeliverState.CompareTo("0") == 0 ? false : true;
                result.ShowDeliverButton = or.DeliverState.CompareTo("5") == 0 ? false : true;
                result.CheckStock = or.Product.StockUpdateType ?.CompareTo("4") <= 0 ? true : false;

            }
            catch (Exception e)
            {

            }
            return View(result);
        }

    }

    public class MaterialOrderRowSliceModel
    {
        public MaterialOrderRowSliceModel()
        {
            TextRows = new List<OrderRowText>();
        }

        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public int MaterialRowNo { get; set; }
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string DeliverState { get; set; }
        public string DeliverDate { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountRemaining { get; set; }
        public decimal AmountDelivered { get; set; }
        public decimal ManufacturedAmount { get; set; }
        public string WarehouseNo { get; set; }
        public string MDF { get; set; }
        public List<OrderRowText> TextRows { get; set; }
        public bool ShowDeliverButton { get; set; }
        public bool ShowBackButton { get; set; }
        public bool hasFrozenWH { get; set; }
        public bool CheckStock { get; set; }
    }
}
