﻿using Babc_logistic_core.GIS.DTO;
using Babc_logistic_core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Babc_logistic_core.ViewComponents
{
    public class OrderRowSlice : ViewComponent
    {
        public OrderRowSlice()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync(CompactOrderRow or)
        {
            OrderRowSliceModel result = new OrderRowSliceModel();

            try
            {
                
                result.OrderNo = or.OrderNo;
                result.RowNo = or.RowNo;
                result.ProductNo = or.ProductNo;
                result.Description = or.ProductDescription;
                result.Description2 = or.ProductDescription2;
                result.DeliverState = or.DeliverState;
                result.AmountRemaining = or.Amount - or.DeliveredAmount;
                result.WarehouseNo = or.WarehouseNo;
                result.MDF = or.MDF;
                result.ShowBackButton = or.DeliverState.CompareTo("0") == 0 ? false : true;
                result.ShowDeliverButton = or.DeliverState.CompareTo("5") == 0 ? false : true;
                result.Origin = or.Origin;
                result.hasFrozenWH = or.WarehouseList.Any(x => x.SaleableCode == "z");
                result.ProductText = or.ProductText?.FindAll(x=>x.Code=="P");
                result.PrintLabel = or.PrintLabel;
                result.OhText2 = or.OhText2;
                result.CustomerNo = or.CustomerNo;
                result.isBatchPickStarted = HttpContext.Request.Cookies.ContainsKey(or.OrderNo + or.RowNo + "-JSON");
                if (or.WarehouseType == "1" && (or.MaterialPlanFlag == "0" || or.MaterialPlanFlag == "5"))
                {
                    result.CheckStock = false;
                }
                else if (or.ProductStockUpdateType?.CompareTo("4") <= 0)
                {
                    result.CheckStock = true;
                }
                else
                {
                    result.CheckStock = false;
                }
                
                result.ManuallyAdded = or.ManuallyAdded;
                result.DeliverDate = or.PreferedDeliverDate;

                if(or.TextRows != null)
                {
                    result.TextRows = or.TextRows;
                }
            }
            catch(Exception e)
            {

            }
            return View(result);
        }
    }

    public class OrderRowSliceModel
    {
        public OrderRowSliceModel()
        {
            TextRows = new List<OrderRowText>();
        }

        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string DeliverState { get; set; }
        public string DeliverDate { get; set; }
        public decimal AmountRemaining { get; set; }
        public decimal AmountDelivered { get; set; }
        public string WarehouseNo { get; set; }
        public string MDF { get; set; }
        public List<OrderRowText> TextRows { get; set; }
        public List<Text> ProductText { get; set; }
        public bool ShowDeliverButton { get; set; }
        public bool ShowBackButton { get; set; }
        public bool CheckStock { get; set; }
        public bool ManuallyAdded { get; set; }
        public string DeliverLogText { get; set; }
        public string Origin { get; set; }
        public bool hasFrozenWH { get; set; }
        public string PrintLabel { get; set;}
        public string CustomerNo { get; set; }
        public string OhText2 { get; set; }
        public bool? isBatchPickStarted { get; set; }

    }
}
